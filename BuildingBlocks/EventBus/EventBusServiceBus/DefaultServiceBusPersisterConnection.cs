﻿using System;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;

namespace EventBusServiceBus
{
    public class DefaultServiceBusPersisterConnection :IServiceBusPersisterConnection
    {
        private readonly ILogger<DefaultServiceBusPersisterConnection> _logger;
        private readonly ServiceBusConnectionStringBuilder _serviceBusConnectionStringBuilder;
        private ITopicClient _topicClient;

        bool _disposed;

        public DefaultServiceBusPersisterConnection(ServiceBusConnectionStringBuilder serviceBusConnectionStringBuilder,
            ILogger<DefaultServiceBusPersisterConnection> logger,string topicName="")
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            
            _serviceBusConnectionStringBuilder = serviceBusConnectionStringBuilder ?? throw new ArgumentNullException(nameof(serviceBusConnectionStringBuilder));

            if (!string.IsNullOrWhiteSpace(topicName))
            {
                _topicClient = new TopicClient(_serviceBusConnectionStringBuilder.ToString(), topicName,  RetryPolicy.Default);
            }
            else
            {
                _topicClient = new TopicClient(_serviceBusConnectionStringBuilder,RetryPolicy.Default);
            }

            
        }

        public ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder => _serviceBusConnectionStringBuilder;

        public ITopicClient CreateModel()
        {
            if(_topicClient.IsClosedOrClosing)
            {
                _topicClient = new TopicClient(_serviceBusConnectionStringBuilder, RetryPolicy.Default);
            }

            return _topicClient;
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
        }
    }
}
