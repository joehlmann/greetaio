﻿using EventBusAbstract.Events;
using MorseCode.ITask;

namespace EventBusMsgBrokerSvc
{
    public interface IRPCReplyService <out T>
        where T : IntEvent
    {
        ITask<T> GetMessageAsync(long sessionId);
    }
}
