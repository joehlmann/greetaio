﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using EventBusMsgBrokerSvc.Settings;
using Microsoft.Azure.ServiceBus.Management;
using Microsoft.Extensions.Options;

namespace EventBusMsgBrokerSvc
{
    public class MsgBrokerManager : IMsgBrokerManager
    {
        private readonly AzureEventBusConfig _config;

        private readonly ManagementClient _managementClient;

        public MsgBrokerManager(IOptions<AzureEventBusConfig> config)
        {
            _config = config.Value;
            _managementClient = new ManagementClient(_config.AzureEventBusConnection);
        }

        // Queue
        public async Task<bool> QueueCheckOrCreateAsync(string queueName)
        {
            if (await _managementClient.QueueExistsAsync(queueName))
                await _managementClient.DeleteQueueAsync(queueName);

            await _managementClient.CreateQueueAsync(queueName);

            return await _managementClient.QueueExistsAsync(queueName);
        }

        

        // Topic
        public async Task<bool> TopicCheckOrCreateAsync(string topicName)
        {
            if (await _managementClient.TopicExistsAsync(topicName))
                await _managementClient.DeleteTopicAsync(topicName);
            await _managementClient.CreateTopicAsync(topicName);

            return await _managementClient.TopicExistsAsync(topicName);
        }

        // Multi Queues
        public async Task<bool> QueuesCheckOrCreateAsync(IEnumerable<string> queueNames)
        {
            var result = new List<bool>();

            foreach (var queueName in queueNames) 
                result.Add(await QueueCheckOrCreateAsync(queueName));

            return result.All(x => x);
        }

        // Multi Topics
        public async Task<bool> TopicsCheckOrCreateAsync(IEnumerable<string> topicNames)
        {
            var result = new List<bool>();

            try
            {
                foreach (var topic in topicNames) 
                    result.Add(await TopicCheckOrCreateAsync(topic));


                return result.All(x => x);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }


            return false;
        }

        
        //Subs & Topic
        public async Task<bool> SubscriptionCheckOrCreateForTopicAsync(string topicName, string subsName)
        {

            var aTopic =  topicName ?? throw new ArgumentNullException(nameof(topicName));
            var aSubs =  subsName ?? throw new ArgumentNullException(nameof(subsName));
            var result = new List<bool>();

            try
            {
                result.Add(await TopicCheckOrCreateAsync(topicName));

                if (await _managementClient.SubscriptionExistsAsync(topicName,subsName))
                    await _managementClient.DeleteSubscriptionAsync(topicName,subsName);

                await _managementClient.CreateSubscriptionAsync(topicName,subsName);

                result.Add(await _managementClient.SubscriptionExistsAsync(topicName,subsName));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                
            }

           

            return result.All(x => x);

        }
    }
}