﻿using System.Text;
using EventBusAbstract.Events;
using EventBusMsgBrokerSvc.Settings;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Options;
using MorseCode.ITask;
using Newtonsoft.Json;

namespace EventBusMsgBrokerSvc
{
    public class RpcReplyService<TEvent> : IRPCReplyService<TEvent>
        where TEvent : IntEvent
    {
        private readonly AzureEventBusConfig _config;
        


        public RpcReplyService(IOptions<AzureEventBusConfig> config)
        {
            _config = config.Value;
        }


        public async ITask<TEvent> GetMessageAsync(long sessionId)
        {
            var sessionClient = new SessionClient(_config.AzureEventBusConnection,_config.AzureReplyTopicName);

            var messageSession = await sessionClient.AcceptMessageSessionAsync(sessionId.ToString());

            var responseMessage = await messageSession.ReceiveAsync();

            await sessionClient.CloseAsync();

            var responseJson = Encoding.UTF8.GetString(responseMessage.Body);

            var responseEventDeserialized =
                JsonConvert.DeserializeObject<TEvent>(responseJson);

            return responseEventDeserialized;

        }
    }
}