﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventBusMsgBrokerSvc
{
    public interface IMsgBrokerManager
    {
        Task<bool> QueuesCheckOrCreateAsync(IEnumerable<string> queueNames);


        Task<bool> QueueCheckOrCreateAsync(string queueName);

        Task<bool> TopicsCheckOrCreateAsync(IEnumerable<string> topicNames);


        Task<bool> TopicCheckOrCreateAsync(string topicName);

        Task<bool> SubscriptionCheckOrCreateForTopicAsync(string topicName,string subsName);
        


        
    }
}
