﻿namespace EventBusMsgBrokerSvc.Settings
{
    public class AzureEventBusConfig
    {
        public bool AzureServiceBusEnabled { get; set; }

        public string AzureEventBusConnection { get; set; }

        public string AzureEventBusConnectionWithTopic { get; set; }

        public string AzureReplyTopicName { get; set; }

        public string AzureTopicName { get; set; }

        public string AzureSubscriptionClientName { get; set; }
    }
}
