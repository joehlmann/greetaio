﻿using Newtonsoft.Json;
using System;
using GreetaIO.Utility.Infrastructure;

namespace EventBusAbstract.Events
{
    public class IntEvent
    {
        public IntEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        [JsonConstructor]
        public IntEvent(Guid id, DateTime createDate)
        {
            Id = id;
            CreationDate = createDate;
        }

        [JsonConstructor]
        public IntEvent(Guid id, DateTime createDate,string correlationId,string clientName)
        {
            Id = id;
            CreationDate = createDate;
            CorrelationId = correlationId;
            SessionId = correlationId;
            ClientName = clientName;
            CorrelationName = EventCorrelation.Name;
        }


        [JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
        public DateTime CreationDate { get; private set; }

        [JsonProperty]
        public string ClientName { get; private set; }
        [JsonProperty]
        public string CorrelationId { get; private set; }
        [JsonProperty]
        public string SessionId { get; private set; }


        /// <summary>
        /// Correlation Info
        /// </summary>
        /// <param name="correlationId"></param>
        /// <param name="clientName"></param>
        public void SetCorrelationDetails(string correlationId, string clientName)
        {
            ClientName = clientName;
            CorrelationId = correlationId;
            SessionId = correlationId;
            CorrelationName = EventCorrelation.Name;

        }

        public void SetAuthToken(string authToken)
        {
            AuthToken = authToken;
        }

       

        public string CorrelationName { get; private set; }

        public string AuthToken { get; private set; }
    }

    
}
