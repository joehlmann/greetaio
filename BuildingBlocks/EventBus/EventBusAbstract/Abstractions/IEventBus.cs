﻿using EventBusAbstract.Events;

namespace EventBusAbstract.Abstractions
{
    public interface IEventBus
    {
        void Publish(IntEvent @event);

        void Subscribe<T, TH>()
            where T : IntEvent
            where TH : IIntegrationEventHandler<T>;

        void SubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        void UnsubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        void Unsubscribe<T, TH>()
            where TH : IIntegrationEventHandler<T>
            where T : IntEvent;
    }
}
