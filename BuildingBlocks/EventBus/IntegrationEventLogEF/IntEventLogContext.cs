﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace IntegrationEventLogEF
{
    public class IntEventLogContext : DbContext
    {
        public IntEventLogContext(DbContextOptions<IntEventLogContext> options) : base(options)
        {
        }

        public DbSet<IntEventLogEntry> IntegrationEventLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<IntEventLogEntry>(ConfigureIntegrationEventLogEntry);
        }

        void ConfigureIntegrationEventLogEntry(EntityTypeBuilder<IntEventLogEntry> builder)
        {
            builder.ToTable("IntegrationEventLog");

            builder.HasKey(e => e.EventId);

            builder.Property(e => e.EventId)
                .IsRequired();

            builder.Property(e => e.Content)
                .IsRequired();

            builder.Property(e => e.CreationTime)
                .IsRequired();

            
            builder.Property(e => e.State)
                .HasConversion(
                    v => v.ToString(),
                    v => (EventStateEnum)Enum.Parse(typeof(EventStateEnum), v));

            builder.Property(e => e.TimesSent)
                .IsRequired();

            builder.Property(e => e.EventTypeName)
                .IsRequired();

        }
    }

    public class IntEventLogContextDesignFactory : IDesignTimeDbContextFactory<IntEventLogContext>
    {
        public IntEventLogContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IntEventLogContext>()
                .UseSqlServer(
                    "Server=robin,1433;Initial Catalog=ApiDb;User Id=greeta;Password=hst;",b => b.MigrationsAssembly("GreetaIO.Infrastructure"));

            return new IntEventLogContext(optionsBuilder.Options);
        }
    }
}
