﻿using System;
using System.Threading.Tasks;
using Akavache;
using GreetaIO.Mobile.Core.Constants;
using GreetaIO.Mobile.Core.Contracts.Repository;
using GreetaIO.Mobile.Core.Contracts.Services.Data;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Services.Data
{
    public class ContactDataService : BaseService, IContactDataService
    {
        private readonly IGenericRepository _genericRepository;

        public ContactDataService(IGenericRepository genericRepository, IBlobCache cache = null) : base(cache)
        {
            _genericRepository = genericRepository;
        }

        public async Task<ContactInfo> AddContactInfo(ContactInfo contactInfo)
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.AddContactInfoEndpoint
            };

            var result = await _genericRepository.PostAsync(builder.ToString(), contactInfo);

            return result;
        }
    }
}
