﻿using System;
using System.Threading.Tasks;
using GreetaIO.Mobile.Core.Constants;
using GreetaIO.Mobile.Core.Contracts.Repository;
using GreetaIO.Mobile.Core.Contracts.Services.Data;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Services.Data
{
    public class OrderDataService : IOrderDataService
    {
        private readonly IGenericRepository _genericRepository;

        public OrderDataService(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public async Task<Order> PlaceOrder(Order order)
        {
            UriBuilder builder = new UriBuilder(ApiConstants.BaseApiUrl)
            {
                Path = ApiConstants.PlaceOrderEndpoint
            };

            var result =
                await _genericRepository.PostAsync<Order>(builder.ToString(), order);

            return order;
        }
    }
}
