﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreetaIO.Mobile.Core.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainView : MasterDetailPage
	{
		public MainView ()
		{
			InitializeComponent ();
		}
	}
}