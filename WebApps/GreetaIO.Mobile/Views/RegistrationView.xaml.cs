﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreetaIO.Mobile.Core.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationView : ContentPage
	{
		public RegistrationView ()
		{
			InitializeComponent ();
		}
	}
}