﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreetaIO.Mobile.Core.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactView : ContentPage
	{
		public ContactView ()
		{
			InitializeComponent ();
		}
	}
}