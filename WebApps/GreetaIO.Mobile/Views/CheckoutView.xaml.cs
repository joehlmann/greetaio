﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreetaIO.Mobile.Core.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CheckoutView : ContentPage
	{
		public CheckoutView ()
		{
			InitializeComponent ();
		}
	}
}