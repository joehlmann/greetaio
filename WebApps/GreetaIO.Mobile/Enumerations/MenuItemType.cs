﻿namespace GreetaIO.Mobile.Core.Enumerations
{
    public enum MenuItemType
    {
        Home,
        Pies,
        ShoppingCart,
        Contact,
        Logout
    }
}
