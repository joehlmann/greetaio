﻿using Plugin.Connectivity.Abstractions;

namespace GreetaIO.Mobile.Core.Contracts.Services.General
{
    public interface IConnectionService
    {
        bool IsConnected { get; }
        event ConnectivityChangedEventHandler ConnectivityChanged;
    }
}
