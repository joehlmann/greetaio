﻿using System.Threading.Tasks;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Contracts.Services.Data
{
    public interface IOrderDataService
    {
        Task<Order> PlaceOrder(Order order);
    }
}
