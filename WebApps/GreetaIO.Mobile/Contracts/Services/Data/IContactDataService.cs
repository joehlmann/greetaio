﻿using System.Threading.Tasks;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Contracts.Services.Data
{
    public interface IContactDataService
    {
        Task<ContactInfo> AddContactInfo(ContactInfo contactInfo);
    }
}
