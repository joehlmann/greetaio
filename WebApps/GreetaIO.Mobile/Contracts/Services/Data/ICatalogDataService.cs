﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Contracts.Services.Data
{
    public interface ICatalogDataService
    {
        Task<IEnumerable<Pie>> GetAllPiesAsync();
        Task<IEnumerable<Pie>> GetPiesOfTheWeekAsync();
    }
}
