﻿using System.Threading.Tasks;
using GreetaIO.Mobile.Core.Models;

namespace GreetaIO.Mobile.Core.Contracts.Services.Data
{
    public interface IShoppingCartDataService
    {
        Task<ShoppingCart> GetShoppingCart(string userId);
        Task<ShoppingCartItem> AddShoppingCartItem(ShoppingCartItem shoppingCartItem, string userId);
    }
}
