﻿using GreetaIO.Mobile.Core.Extensions;
using GreetaIO.Mobile.Core.Models;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace GreetaIO.Mobile.Core.Utility
{
    public static class AppSettings
    {
        private static ISettings Settings => CrossSettings.Current;

        public static User User
        {
            get => Settings.GetValueOrDefault(nameof(User), default(User));

            set => Settings.AddOrUpdateValue(nameof(User), value);
        }
    }
}
