﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreetaIO.Mobile.Core.Templates
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PieTemplate : ContentView
	{
		public PieTemplate ()
		{
			InitializeComponent ();
		}
	}
}