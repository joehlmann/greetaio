﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GreetaIO.Utility.Infrastructure;
using MediatR;

namespace GreetaIO.Utility
{
    public abstract class EntitySimple<TId> : IEquatable<Entity<TId>>
      //where TId : struct
    {
        protected TId _id;
        public TId Id { get => _id;
             set
        {
            // operator== EqualityComparer 
            if (!EqualityComparer<TId>.Default.Equals(_id, value))
            {
                _id = value;
            }
        } }

        //for db and user track
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public string CurrentUserId { get; set; }


        public DateTime CreateDate { get; set; }

        public DateTime? LastModified { get; set; }


        protected EntitySimple(TId id, string currentUserId)
        {
            CurrentUserId = currentUserId;
            Id = id;
        }

        protected EntitySimple()
        {
        }

        

        public bool IsTransient()
        {
            return default(TId).Equals(Id);
        }

        private List<ApiError> _errors;
        public IReadOnlyCollection<ApiError> Errors => _errors?.AsReadOnly();
        public bool HasErrors => 0 != (_errors?.Count ?? 0);

        public void AddError(ApiError error)
        {
            _errors = _errors ?? new List<ApiError>();
            _errors.Add(error);
        }

        public void RemoveError(ApiError error)
        {
            _errors?.Remove(error);
        }
        public void ClearErrors()
        {
            _errors?.Clear();
        }

        // Comparable 
        public override bool Equals(object otherObject)
        {

            

            if (otherObject is Entity<TId> entity)
            {
                return Equals(entity);
            }
            return base.Equals(otherObject);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(Entity<TId> other)
        {
            if (other == null)
            {
                return false;
            }

            return EqualityComparer<TId>.Default.Equals(Id, other.Id);

            
        }

       
    }
}
