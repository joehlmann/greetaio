﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreetaIO.Utility.Infrastructure
{
    public static class ClientNames
    {
        public static string BioClient => "BioClient";

        public static string ApiClient => "ApiClient";

        public static string VrClient => "VrClient";

    }
}
