﻿using System;

namespace GreetaIO.Utility.Infrastructure
{
    public class ApiError
    {

        public Guid Id { get; set; }

        public string Code { get; set; }
        public string Message { get; set; }
    }
}
