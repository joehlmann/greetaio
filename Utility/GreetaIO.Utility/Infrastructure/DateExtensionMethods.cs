﻿using System;

namespace GreetaIO.Utility.Infrastructure
{
    public static class DateExtensionMethods
    {
        public static DateTime? Parse( string text){
            return DateTime.TryParse(text, out var date) ? date : (DateTime?) null;
        }
        
    }
}
