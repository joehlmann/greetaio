﻿namespace GreetaIO.Utility.Infrastructure
{
    public static class ValidationMessages
    {
        public static readonly ApiError PickupReadyTimeIsMissing;
        public static readonly ApiError PickupReadyTimeIsInvalid;
        public static readonly ApiError PickupCloseTimeIsMissing;
        public static readonly ApiError PickupWindowNotLongEnough;
        public static readonly ApiError PickupSenderNameMissing;
        public static readonly ApiError PickupContactNameMissing;
        public static readonly ApiError PickupContactPhoneMissing;
        public static readonly ApiError PickupAddressMissing;
        public static readonly ApiError PickupSuburbMissing;
        public static readonly ApiError PickupPostcodeMissing;
        public static readonly ApiError PickupInvalidSuburbDetails;
        public static readonly ApiError PickupMissingSuburbDetails;
        public static readonly ApiError PickupCustomerProvidedDuplicatePickupNumber;
        public static readonly ApiError PickupDateIsToFarIntoTheFuture;
        public static readonly ApiError PickupDateIsToFarIntoThePast;
        public static readonly ApiError PickupBookingUserIsMissing;
        public static readonly ApiError PickupBookingUserIsInvalid;
        public static readonly ApiError PickupBookingDateTimeIsMissing;
        public static readonly ApiError PickupBranchIsMissing;
        public static readonly ApiError PickupBranchCutoffTimeIsMissing;
        public static readonly ApiError PickupBranchCutoffTimeIsInvalid;
        public static readonly ApiError PickupUnknownPickup;
        public static readonly ApiError PickupBranchIsUnknown;

        public static readonly ApiError BranchPreferredSendingBranchIsMissing;
        public static readonly ApiError BranchPreferredSendingBranchIsUnknown;
        public static readonly ApiError BranchPreferredReceivingBranchIsUnknown;
        public static readonly ApiError BranchPickupBranchSuburbIsUnknown;

        public static readonly ApiError PickupItemDGUnknown;
        public static readonly ApiError PickupItemUnknown;
        
        public static readonly ApiError RegionReceivingRegionIsUnknown;

        static ValidationMessages()
        {
            PickupReadyTimeIsMissing = new ApiError{Code = "1.1.1", Message = "Ready time must be provided"};
            PickupCloseTimeIsMissing = new ApiError{Code = "1.1.2", Message = "Close time must be provided"};
            PickupWindowNotLongEnough = new ApiError{Code = "1.1.3", Message = "A pickup windows of greater than {MinimumPickupWindowDuration} must be provided"};
            PickupSenderNameMissing = new ApiError{Code ="1.1.4", Message = "A sender name must be provided"};
            PickupContactNameMissing = new ApiError{Code ="1.1.5", Message = "A contact name must be provided"};
            PickupContactPhoneMissing = new ApiError{Code ="1.1.6", Message = "A contact phone number must be provided"};
            PickupAddressMissing = new ApiError{Code ="1.1.7", Message = "Address line details must be provided"};
            PickupSuburbMissing = new ApiError{Code ="1.1.8", Message = "Suburb details must be provided"};
            PickupPostcodeMissing = new ApiError{Code ="1.1.9", Message = "A postcode must be provided"};
            PickupInvalidSuburbDetails = new ApiError{Code ="1.1.10", Message = "The provided suburb details are invalid"};
            PickupMissingSuburbDetails = new ApiError{Code ="1.1.11", Message = "The provided suburb details are missing"};
            PickupCustomerProvidedDuplicatePickupNumber = new ApiError{Code = "1.1.12",Message = "The pickup number provided is already in use"};
            PickupDateIsToFarIntoTheFuture = new ApiError{Code = "1.1.13",Message = "You can not forward date a Pickup by more than {MaximumDaysPickupDateIsAllowedIntoTheFuture} days"};
            PickupDateIsToFarIntoThePast = new ApiError{Code = "1.1.14",Message = "You can not date a Pickup more than {MaximumDaysPickupDateIsAllowedIntoThePast} days into the past"};
            PickupBookingDateTimeIsMissing = new ApiError{Code = "1.1.15", Message = "A booking time must be provided"};
            PickupBookingUserIsInvalid = new ApiError{Code = "1.1.16", Message = "The provided booked by user is invalid"};
            PickupBookingUserIsMissing = new ApiError{Code="1.1.17", Message = "A booking user must be provided"};
            PickupReadyTimeIsInvalid = new ApiError{Code = "1.1.18", Message = "The Ready time must be a valid time"};
            PickupBranchIsMissing = new ApiError{Code = "1.1.19", Message = "A pickup branch must be provided"};
            PickupBranchCutoffTimeIsMissing = new ApiError{Code = "1.1.20", Message = "The pickup branch must have a cut off time"};
            PickupBranchCutoffTimeIsInvalid = new ApiError{Code = "1.1.21", Message = "The pickup branch cut off time must be a valid time"};
            PickupUnknownPickup = new ApiError{Code = "1.1.22", Message = "The pickup is unknown"};
            PickupBranchIsUnknown = new ApiError{Code = "1.1.23", Message = "The pickup branch is unknown"};
            
            BranchPreferredSendingBranchIsMissing = new ApiError{Code = "1.2.1", Message = "The preferred sending branch has not been set"};
            BranchPreferredSendingBranchIsUnknown = new ApiError{Code = "1.2.2", Message = "The preferred sending branch is unknown"};
            BranchPickupBranchSuburbIsUnknown = new ApiError{Code = "1.2.3", Message = "The pickup branch suburb is unknown"};
            BranchPreferredReceivingBranchIsUnknown = new ApiError{Code = "1.2.4", Message = "The preferred receiving branch is unknown"};

            PickupItemDGUnknown = new ApiError {Code = "1.3.1", Message = "The provided dangerous good details are invalid"};
            PickupItemUnknown = new ApiError {Code = "1.3.2", Message = "The provided item is unknown"};

            RegionReceivingRegionIsUnknown = new ApiError{Code = "1.4.1", Message = "The receiving region is unknown"};
        }

        
    }
}
