﻿using System;

namespace GreetaIO.Utility.Helper
{
    public static class GuidExtenstion
    {
        public static Guid SeedId(this Guid @this, int num)
        {
            
            var id = (Guid.Empty.ToString()).Remove(0, num.ToString().Length).Insert(0, num.ToString());

            return Guid.Parse(id);
        }

    }
}
