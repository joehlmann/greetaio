﻿using CSharpFunctionalExtensions;
using System;
using System.Text;

namespace GreetaIO.Utility.Helper
{
    public static class StringHelper
    {

        
        
            public static string NullIfEmpty(this string s)
            {
                return string.IsNullOrEmpty(s) ? null : s;
            }
            public static string NullIfSpace(this string s)
            {
                return string.IsNullOrWhiteSpace(s) ? null : s;
            }
        

        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);

                }
            }
            return sb.ToString();
        }

        public static byte[] ToByte(this string str)
        {
            var result = Convert.FromBase64String(str);
            return result;
        }


        public static string ToByteString(this byte[] bytes)
        {
            var result = Convert.ToBase64String(bytes);
            return result;
        }

        /// <summary>
        /// Converts int String To Guid String
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsGuid(this string str)
        {
            return Guid.TryParse(str, out _);
            
        }


        public static bool IsByteArray(this string str)
        {
           
                Span<byte> buffer = new Span<byte>(new byte[str.Length]);
                return Convert.TryFromBase64String(str, buffer, out int bytesParsed);

        }


        public static bool IsInt(this string str)
        {
            int i = 0;
            bool result = int.TryParse(str, out i);
            return result;
        }

        public static Result<bool> IsIntResult(this string str)
        {
            int i = 0;
            bool result = int.TryParse(str, out i);

            return result ? Result.Success(true) : Result.Failure<bool>($"String:{str} is not int");

            
        }

        public static int ToInt(this string str)
        {
            int id;

            string result;
            if (!int.TryParse(str, out id))
            {
                    return default;
             
            }

            return id;
        }


        public static Guid ToGuid(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return default;
                }

                return (Guid.Empty).SeedId(idInt);
            }

            return Guid.Parse(str);
        }

        public static string ToGuidStr(this string str)
        {
            Guid id;

            string result;
            if (!Guid.TryParse(str, out id))
            {
                int idInt;
                if (!int.TryParse(str, out idInt))
                {
                    return default;
                }

                return ((Guid.Empty).SeedId(idInt)).ToString();
            }

            return (Guid.Parse(str)).ToString();
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
