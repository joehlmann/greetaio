﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Autofac;
using GreetaIO.BioClient.Interfaces;
using GreetaIO.BioClient.Utilities;



namespace GreetaIO.BioClient.DI
{
    public  class BioClientModule : Module
    {
        public string BioEndPoint { get; }

        private ContainerBuilder _container;



        public BioClientModule(ContainerBuilder container,string bioEndPoint)
        {
            BioEndPoint = bioEndPoint;
            _container = container;
        }


        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FVDBScanPortTypeClient>()
                .As<IFVDBScanPortTypeClient>()
                .WithParameter(new TypedParameter(typeof(Binding),
                    new WebClientBinding()))
                .WithParameter(new TypedParameter(typeof(EndpointAddress),
                    new EndpointAddress(BioEndPoint))).InstancePerLifetimeScope();

            builder.RegisterType<BioClient>().As<IBioClient>().InstancePerLifetimeScope();

        }


        
    }
    public class WebClientBinding : BasicHttpBinding
    {
        public WebClientBinding()
        {
            Name = "default";
            MaxBufferSize = 2147483647;
            MaxReceivedMessageSize = 2147483647;

        }
    }
}
