﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient.Utilities
{
    public class StringUtils : BaseUtil<StringUtils>
    {

        /// <summary>
        /// Sanitises facebook urls to the correct format
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string SanitiseFacebookURLS(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url))
                {
                    return url;
                }
                url = StringUtils.CleanHTMLFromString(url);
                url = url.ToLower();
                url = url.Replace("http://", "");
                url = url.Replace("https://", "");
                url = url.Replace("www.facebook.com/", "");
                url = url.Replace("facebook.com/", "");
                url = url.Replace("www.facebook.com", "");
                url = url.Replace("facebook.com", "");
                url = "https://www.facebook.com/" + url;
                return url;
            }
            catch (Exception e)
            {
                Log.LogError(e, "Failed to Sanitise Facebook URLS [" + url + "]");
            }

            return string.Empty;

        }

        /// <summary>
        /// Sanitises twitter urls to the correct format
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string SanitiseTwitterURLS(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url))
                {
                    return url;
                }
                url = StringUtils.CleanHTMLFromString(url);
                url = url.ToLower();
                url = url.Replace("@", "");
                url = url.Replace("http://", "");
                url = url.Replace("https://", "");
                url = url.Replace("www.twitter.com/", "");
                url = url.Replace("twitter.com/", "");
                url = url.Replace("www.twitter.com", "");
                url = url.Replace("twitter.com", "");
                url = "https://twitter.com/" + url;
                return url;
            }
            catch (Exception e)
            {
                Log.LogError( e, "Failed to Sanitise Twitter URLS [" + url + "]");
            }

            return string.Empty;
          
        }
        
         /// <summary>
        /// Returns a pretty string removing double spaces 
        /// </summary>
        /// <param name="astring"></param>
        /// <returns></returns>
        public static string RemoveDoubleSpace(string astring)
        {
            
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(astring, @" ");
            
        }

        /// <summary>
        /// Returns a pretty string containing the distance in meters/kms
        /// </summary>
        /// <param name="meters"></param>
        /// <returns></returns>
        public static string GetDistanceText(int? meters)
        {

            if (meters == null)
            {
                return null;
            }

            int metersInt = (int)meters;

            if (metersInt < 1000)
            {
                return metersInt.ToString("N0") + " meters";
            }
            else
            {
                return (metersInt / 1000).ToString("N0") + " km";
            }
        }


        /// <summary>
        /// Converts a string into a memory stream
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Applies Title Case to a string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetTitleCase(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                TextInfo myTI = CultureInfo.CurrentCulture.TextInfo;
                value = myTI.ToTitleCase(value.ToLower());
            }

            return value;
        }


        /// <summary>
        /// Returns a random string using A-Z, 23456789
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetRandomString(int length)
        {
            var chars = "ABCDEFGHJQLMNPQRSTUVWXYZ23456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());

            return result;
        }



        /// <summary>
        /// Returns a formatted currency rounded 3 digits and apply K=thousand, M= millions, B=billions
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetCurrency(decimal ? value)
        {

            if (value == null)
            {
                return "";
            }

            var decimalValue = (decimal)value;

            if (decimalValue < 1000000) // Under a million
            {
                var returnVal = decimalValue / 1000;
                return Math.Round(returnVal, 0).ToString("C0") + "K";
            }
            else if (decimalValue < 1000000000) // Under a billion
            {
                var returnVal = decimalValue / 1000000;
                return Math.Round(returnVal, 2).ToString("C2") + "M";
            }
            else
            {
                var returnVal = decimalValue / 1000000000;
                return Math.Round(returnVal, 2).ToString("C2") + "B";
            }
        }


        /// <summary>
        /// Returns a formatted string for a percentage
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetPercentage(decimal value)
        {
            if (value >= 100)
            {
                return Math.Round(value, 0).ToString("N0") + "%";
            }
            else if (value >= 10)
            {
                return Math.Round(value, 1).ToString("N1") + "%";
            }
            else
            {
                return Math.Round(value, 2).ToString("N2") + "%";
            }
        }


        /// <summary>
        /// Removes all HTML tags from a string and returns the text
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanHTMLFromString(string text)
        {
            string output = "";

            try
            {
                if (string.IsNullOrEmpty(text))
                {
                    return output;
                }


                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(text);
                if (doc != null)
                {
                    foreach (var node in doc.DocumentNode.ChildNodes)
                    {
                        output += node.InnerText;
                    }
                }

                output = output.Replace("  ", " "); //replace double spaces
                output = output.Trim();

            }
            catch(Exception e)
            {
                Log.LogError(e, "Failed to Clean HTML String [" + text + "]");
            }

            return output;

        }

        /// <summary>
        /// Returns a valid URL for example www.google would return http://www.google.com
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetValidURL(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                {
                    url = "http://" + url;
                }
            }

            Uri result;
            if (!Uri.TryCreate(url, UriKind.Absolute, out result))
            {
                return null;
            }

            return url;

        }


        /// <summary>
        /// Returns an int or a null string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int? GetInt(string value)
        {
            try
            {
                return Int32.Parse(value);
            }
            catch (Exception)
            {

                return null;
            }
        }

        /// <summary>
        /// Returns a decimal or a null string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal? GetDecimal(string value)
        {
            try
            {
                return Decimal.Parse(value);
            }
            catch (Exception)
            {

                return null;
            }


        }

        /// <summary>
        /// Returns a Long or a null string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long? GetLong(string value)
        {
            try
            {
                return long.Parse(value);
            }
            catch (Exception)
            {

                return null;
            }


        }

        /// <summary>
        /// Returns a DateTime or a null string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static DateTime? GetDateTime(string value, string pattern = null)
        {
            try
            {

                if (string.IsNullOrEmpty(value))
                {
                    return null;
                }

                if (string.IsNullOrEmpty(pattern))
                {
                    string[] formats = { 
                    // Basic formats
                    "yyyy-MM-dd",
                    "yyyy-MM-dd-HH:mm",
                    "yyyy-MM-dd-HH:mm:ss",
                    "yyyy-MM-ddTHH:mm",
                    "yyyy-MM-ddTHH:mm:ss",
                    "yyyyMMdd",
                    "yyyyMMdd-HHmm",
                    "yyyyMMdd-HHmmss",
                    "yyyyMMddTHHmm",
                    "yyyyMMddTHHmmss",
                    "dd/MM/yy",
                    "dd/MM/yyyy",
                    };

                    return DateTime.ParseExact(value, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                else
                {
                    return DateTime.ParseExact(value, pattern, CultureInfo.InvariantCulture);
                }                
            }
            catch (Exception e)
            {
                Log.LogError(e, "Failed to parse date [" + value + "] with format [" + pattern + "]");
                return null;
            }
        }
    }

}
