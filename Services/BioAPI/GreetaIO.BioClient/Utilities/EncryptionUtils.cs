﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GreetaIO.BioClient.Utilities
{
    public class EncryptionUtils : BaseUtil<EncryptionUtils>
    {
        
        /// <summary>
        /// Returns a MD5 hash of the input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Guid GetGuidHash(string input)
        {
            return GetGuidHash(Encoding.Default.GetBytes(input));
        }

        /// <summary>
        /// Returns a MD5 hash of the input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Guid GetGuidHash(byte[] input)
        {
            // Encode URL
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] hashData = md5Hasher.ComputeHash(input);
            Guid hash = new Guid(hashData);
            hashData = null;
            return hash;
        }


        public static string GetMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

    }
}
