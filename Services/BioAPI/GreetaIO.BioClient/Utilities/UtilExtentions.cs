﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using GreetaIO.BioClient.Interfaces;
using Newtonsoft.Json;

namespace GreetaIO.BioClient.Utilities
{
    public static class UtilExtentions
    {
        public static Stream ToStream(this System.Drawing.Image image, ImageFormat formaw)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, formaw);
            stream.Position = 0;
            return stream;
        }
        public static string ToStringList(this IEnumerable<string> list)
        {
            string temp = string.Join(", ", list.Where(x => !string.IsNullOrEmpty(x)));
            return String.Format("[{0}]", temp);
        }

        public static TTarget Convert<TSource, TTarget>(this TSource src)
        {
            string json = JsonConvert.SerializeObject(src);
            return JsonConvert.DeserializeObject<TTarget>(json);
        }

        public static byte[] ImageToByteJpg(this System.Drawing.Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public static  System.Drawing.Image ByteToImage(this byte[] byteImg)
        {
            using (var ms = new MemoryStream(byteImg))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public static AnnotatedImage ImageToAnontatedImage(this System.Drawing.Image img)
        {
            var image = new Image
            {
                binaryImg = img.ImageToByteJpg()
            };


            var imgAnnon = new AnnotatedImage();

            imgAnnon.img = image;
            imgAnnon.annotation = new Eyes();

                
            return imgAnnon;
        }
        public static Image ImageToSvcImage(this System.Drawing.Image img)
        {
            var image = new Image
            {
                binaryImg = img.ImageToByteJpg()
            };
            return image;
        }
        public static byte[] ImageToBytePng(this System.Drawing.Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }
    }
}
