﻿using System;

namespace GreetaIO.BioClient.Utilities
{
   public static  class CC
    {
        public static void Green(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }
        public static void Gray(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.DarkGray;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }
        public static void White(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }

        public static void Magenta(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Magenta;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }

        public static void Red(string s, string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }
        public static void Cyan(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Cyan;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }

        public static void Yellow(string message)
        {
            var beforeColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine(message);

            Console.ForegroundColor = beforeColor;
        }
    }
}
