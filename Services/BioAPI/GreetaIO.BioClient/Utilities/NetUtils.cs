﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient.Utilities
{
    public class NetUtils : BaseUtil<NetUtils>
    {

        /// <summary>
        /// Returns the local IP Address of the machine running the code or null if none exists
        /// </summary>
        /// <returns></returns>
        public static string LocalIPAddress()
        {
            try
            {
                if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                {
                    return null;
                }

                IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

                return host
                    .AddressList
                    .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();
            }
            catch
            {
            }
            return null;
        }

        /// <summary>
        /// Downloads an image from a URL
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="UserAgent"></param>
        /// <returns></returns>
        public static byte[] DownloadImage(string URL, string UserAgent)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.UserAgent = UserAgent;
            byte[] returnData = null;

            using (var response = (HttpWebResponse)request.GetResponse())
            {

                // Check that the remote file was found. The ContentType
                // check is performed since a request for a non-existent
                // image file might be redirected to a 404-page, which would
                // yield the StatusCode "OK", even though the image was not
                // found.
                if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect) &&
                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                {

                    // if the remote file was found, download oit
                    using (Stream inputStream = response.GetResponseStream())
                    {
                        byte[] buffer = new byte[16 * 1024];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            int read;
                            while ((read = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, read);
                            }
                            returnData = ms.ToArray();
                            //ms.Close();
                            //ms.Dispose();
                            buffer = null;
                        }
                        //inputStream.Close();
                        //inputStream.Dispose();
                    }
                }

            }
            return returnData;
        }


        /// <summary>
        /// Returns the data from a URL
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        public static string GetWebResponse(string url, string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36")
        {
            string responseFromServer = null;
            try
            {

                // Create a request for the URL. 
                WebRequest request = WebRequest.Create(url);
                ((HttpWebRequest)request).UserAgent = userAgent;
                // Get the response.
                using (var response = request.GetResponse())
                {
                    // Get the stream containing content returned by the server.
                    using (Stream dataStream = response.GetResponseStream())
                    {

                        // Open the stream using a StreamReader for easy access.
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            // Read the content.
                            responseFromServer = reader.ReadToEnd();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                var err = e.Message;
                Log.LogError("Failed to Get Web Response using URL[" + url + "] and UserAgent[" + userAgent + @"] with Error {0}",err);            }
            return responseFromServer;
        }

        public static async Task<string> GetWebResponseAsync(string url, string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36")
        {
            string responseFromServer = null;

            try
            {

                using (var client = new HttpClient())
                {
                    Uri uri = new Uri(url);
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", userAgent);
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                    var response = await client.GetAsync(uri).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content.Headers.ContentEncoding.FirstOrDefault() == "gzip") {
                            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                            using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                            using (var streamReader = new StreamReader(decompressedStream))
                            {
                                responseFromServer = streamReader.ReadToEnd();
                            }
                        }
                        else
                        {
                            responseFromServer = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        Log.LogError("Received error [" + response.StatusCode + "] from url [" + url + "]");
                    }
                }
            }
            catch (Exception e)
            {
                Log.LogError(e, "Failed to Get Web Response using URL[" + url + "] and UserAgent[" + userAgent + "]");
            }
            return responseFromServer;
        }


        /// <summary>
        /// Returns a list of MailAddress's based on an input string that is sperated by commas
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static List<MailAddress> GetEmailList(string address)
        {
            try
            {
                List<MailAddress> recipients = new List<MailAddress>();

                var list = address.Split(',');
                foreach (var email in list)
                {
                    recipients.Add(new MailAddress(email));
                }
                return recipients;
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// A very basic method to send an email using the default SMTP settings. This will be replaced soon by the Emailer. 
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isBodyHtml"></param>
        /// <param name="attachment"></param>
        /// <param name="from"></param>
        /// <returns></returns>
        public static bool SendEmail(List<MailAddress> to, String subject, String body, MailAddress from, bool isBodyHtml = false)
        {
            try
            {
                if (to == null || to.Count == 0)
                    throw new InvalidOperationException("To field for email can not be empty");

                if (string.IsNullOrEmpty(subject))
                    throw new InvalidOperationException("Subject field for email can not be empty");

                if (string.IsNullOrEmpty(body))
                    throw new InvalidOperationException("Body field for email can not be empty");

                MailMessage msg = new MailMessage();

                msg.IsBodyHtml = isBodyHtml;
                to.ForEach(x => msg.To.Add(x));
                msg.From = from;
                msg.Subject = subject;
                msg.Body = body;

                // We dont specify the SMTP client as it should come out of the Web.Config
                SmtpClient client = new SmtpClient();
                client.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                Log.LogError(e, "Failed to Send EMail");
                return false;
            }

        }

    }
}
