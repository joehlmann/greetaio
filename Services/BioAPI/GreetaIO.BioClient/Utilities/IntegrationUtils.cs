﻿using System;
using System.Text.RegularExpressions;

namespace GreetaIO.BioClient.Utilities
{
    public class IntegrationUtils
    {
        /// <summary>
        /// This method removes invalid no ASCI characters from a string. This is mainly used when cleaning invalida XML files
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanNonASCIChars(string text)
        {
            const string re = "[\x00-\x08\x0B\x0C\x0E-\x1F]";
            return Regex.Replace(text, re, "", RegexOptions.Compiled);
        }

        ///// <summary>
        ///// This medthod removes any HTML encoding from a string, removes double spaces and also trims and leading/trailing spaces.
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //public static string CleanHTMLString(string input)
        //{
        //    if (string.IsNullOrEmpty(input))
        //    {
        //        return input;
        //    }

        //    string returnValue = System.Web.HttpUtility.HtmlDecode(input);
        //    returnValue = returnValue.Replace("  ", " ");
        //    returnValue = returnValue.Trim();

        //    return returnValue;
        //}

        /// <summary>
        /// This method removes any line breaks (\r\n and \n) from a string and cleans up any leading/trailing spaces 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CleanLineBreaks(string text)
        {
            if (String.IsNullOrEmpty(text)) return String.Empty;

            string result = text.Replace("\r\n", ", ").TrimEnd(',', ' ');
            result = text.Replace("\n", ", ").TrimEnd(',', ' ');

            return result;
        }


        /// <summary>
        /// This method cleans an australian mobile number. It will convert the following... +61 410 334 000, (04) 10 334 000 into 0410 334 000.
        /// </summary>
        /// <param name="mobileNumber"></param>
        /// <returns></returns>
        public static string CleanMobileNumber(string mobileNumber)
        {
            // Remove Spaces
            string result = mobileNumber.Replace(" ", "");

            // Remove Brackets ()
            result = result.Replace("(", "").Replace(")", "");

            // Remove incorrect format
            result = result.Replace("+6104", "+614");

            // Remove +61
            result = result.Replace("+61", "0");

            return result;
        }


        /// <summary>
        /// This method removes and $ or commans in a text
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CleanCash(string input)
        {

            input = input.Replace("$", "");
            input = input.Replace(",", "");

            return input;
        }


    }
}
