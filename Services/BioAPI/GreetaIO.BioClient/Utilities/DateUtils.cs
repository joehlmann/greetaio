﻿using System;

namespace GreetaIO.BioClient.Utilities
{
    public class DateUtils
    {

        /// <summary>
        /// This will convert a DateTime object into a DateTimeOffset using the Australian Eastern Standard Time Offset (+10:00)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTimeOffset? GetTimeOffset(DateTime? dateTime)
        {

            if (dateTime == null)
            {
                return null;
            }

            try
            {
                var est = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                return TimeZoneInfo.ConvertTime((DateTime)dateTime, est);
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// This method will return the difference between two dates as a string.... e.g. 4 Days, 1 hour, 10 mins...
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static string TimeDifference(DateTime startTime, DateTime endTime)
        {
            TimeSpan span = endTime.Subtract(startTime);

            if (span.TotalDays > 365)
            {

                var years = (int)(span.TotalDays / 365);

                if (years == 1)
                {
                    return years + " year";
                }
                else
                {
                    return years + " years";
                }
            }
            if (span.TotalDays >= 1)
            {
                if (Math.Floor(span.TotalDays) == 1)
                {
                    return Math.Floor(span.TotalDays) + " day";
                }
                else
                {
                    return Math.Floor(span.TotalDays) + " days";
                }
            }
            else if (span.TotalHours >= 1)
            {
                if (Math.Floor(span.TotalHours) == 1)
                {
                    return Math.Floor(span.TotalHours) + " hour";
                }
                else
                {
                    return Math.Floor(span.TotalHours) + " hours";
                }
            }
            else if (span.TotalMinutes >= 1)
            {
                if (Math.Floor(span.TotalMinutes) == 1)
                {
                    return Math.Floor(span.TotalMinutes) + " min";
                }
                else
                {
                    return Math.Floor(span.TotalMinutes) + " mins";
                }
            }
            else if (span.TotalSeconds == 0)
            {
                return "now";
            }
            else            
            {
                return Math.Floor(span.TotalSeconds) + " secs";
            }
        }

        /// <summary>
        /// This will return the difference between the supplied datetime and the current time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
       [Obsolete("This will be replaced by a DateTimeOffset method as we no longer want to use DateTime in the system")]
        public static string TimeFromNow(DateTime dateTime)
        {
            return TimeDifference(dateTime, CurrentTimeInAustralia());

        }


        [Obsolete("This will be replaced by a DateTimeOffset method as we no longer want to use DateTime in the system")]
        public static DateTime CurrentTimeInAustralia()
        {
            try
            {
                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "AUS Eastern Standard Time");
            }
            catch { }
            Console.WriteLine("Could not find AUS Eastern Standard Time");
            return DateTime.UtcNow.AddHours(10);

        }

        /// <summary>
        /// This returns a DateTimeOffset using the AUS Eastern Standard Time as the offset
        /// </summary>
        /// <returns></returns>
        public static DateTimeOffset CurrentTimeOffsetInAustralia()
        {
            try
            {
                var est = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                return TimeZoneInfo.ConvertTime(DateTimeOffset.UtcNow, est);
            }
            catch { }
            Console.WriteLine("Could not find AUS Eastern Standard Time");
            return DateTimeOffset.UtcNow;
        }

        
        /// <summary>
        /// This will convert a DateTime object into a DateTimeOffset using the Aus Eastern Standard Time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTimeOffset DateTimeOffsetInAustralia(DateTime dateTime)
        {
            var est = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, est);
        }
    }
}
