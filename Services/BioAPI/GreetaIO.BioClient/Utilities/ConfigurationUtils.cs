﻿using System;
using System.Configuration;

using System.Configuration;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient.Utilities
{
    /// <summary>
    /// Helper class for App and Web configuration management
    /// </summary>
    public class ConfigurationUtils : BaseUtil<ConfigurationUtils>
    {
        /// <summary>
        /// Helper method to:
        /// Get the appsetting key from web.config, and app.config 
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="mandatory"></param>
        /// <returns></returns>
        public static string GetConfigItem(string key, bool mandatory = false)
        {
            var item = ConfigurationManager.AppSettings[key];

            if (string.IsNullOrEmpty(item)) 
            {
               item = ConfigurationManager.AppSettings[key];
            }

            if (mandatory && string.IsNullOrEmpty(item))
            {
                string msg = "Mandatory configuration item [" + key + "] was not found";
                Log.LogError(msg);
                throw new ApplicationException(msg);
            }

            return item;
        }


        /// <summary>
        /// Returns an appsetting key from web.config and app.config 
        /// as an Integer
        /// </summary>
        /// <param name="key"></param>
        /// <param name="mandatory"></param>
        /// <returns></returns>
        public static int GetConfigItemInt(string key, bool mandatory = false)
        {
            var item = GetConfigItem(key, mandatory);

            int returnVal;

            if (!Int32.TryParse(item, out returnVal))
            {
                string msg = "Configuration item [" + key + "] was not an int as required";
                Log.LogError(msg);
                throw new ApplicationException(msg);
            }
            return returnVal;
        }
    }
}
