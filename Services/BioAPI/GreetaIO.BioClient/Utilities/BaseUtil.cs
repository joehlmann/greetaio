﻿//Needed to init the Log4net config.
//[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config", Watch = true)]




//using Utilities.IO.Logging.Interfaces;
using Microsoft.Extensions.Logging;
using Serilog;

namespace GreetaIO.BioClient.Utilities
{
    public abstract class BaseUtil<T>
    {
        private static ILogger<T> _log;

        protected static ILogger<T> Log { get => _log; set => _log = value; }



        //protected static log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}
