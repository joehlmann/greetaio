﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient.Utilities
{
    /// <summary>
    /// Helper class for App and Web configuration management
    /// </summary>
    public class FileUtils : BaseUtil<FileUtils>
    {
        public static bool IsJpgImage(Stream stream)
        {
            try
            {
                System.Drawing.Image img = System.Drawing.Image.FromStream(stream);

                // Two image formats can be compared using the Equals method
                // See http://msdn.microsoft.com/en-us/library/system.drawing.imaging.imageformat.aspx
                //
                return img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg)
                    || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp)
                    || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif)
                    || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png);
            }
            catch (Exception)
            {
                Log.LogError("Image is not a valid JPEG");
                // Image.FromFile throws an OutOfMemoryException 
                // if the file does not have a valid image format or
                // GDI+ does not support the pixel format of the file.
                //
                return false;
            }

        }



        public static List<string> AddFileNamesDir(string directory)
        {
            List<string> myList = new List<string>();
            try
            {
                myList = Directory.GetFiles(directory).ToList();
            }
            catch (Exception e)
            {

                Log.LogError(e.Message);
            }
            return myList;
        }
    }
}
