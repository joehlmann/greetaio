﻿// ***********************************************************************
// Assembly         : Greeta.BioClient
// Author           : justin
// Created          : 08-17-2017
//
// Last Modified By : justin
// Last Modified On : 08-21-2017
// ***********************************************************************
// <copyright file="ImgUtils.cs" company="">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using GreetaIO.BioClient.Interfaces;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient.Utilities
{
    /// <summary>
    /// Class ImgUtils.
    /// </summary>
    /// <seealso cref="BaseUtil" />
    public class ImgUtils : BaseUtil<ImgUtils>
    {
        /// <summary>
        /// The c colors
        /// </summary>
        static int[] cColors = { 0x000000, 0x000080, 0x008000, 0x008080, 0x800000, 0x800080, 0x808000, 0xC0C0C0, 0x808080, 0x0000FF, 0x00FF00, 0x00FFFF, 0xFF0000, 0xFF00FF, 0xFFFF00, 0xFFFFFF };

        /// <summary>
        /// Consoles the write pixel.
        /// </summary>
        /// <param name="cValue">The c value.</param>
        public static void ConsoleWritePixel(Color cValue)
        {
            Color[] cTable = cColors.Select(x => Color.FromArgb(x)).ToArray();
            char[] rList = new char[] { (char)9617, (char)9618, (char)9619, (char)9608 }; // 1/4, 2/4, 3/4, 4/4
            int[] bestHit = new int[] { 0, 0, 4, int.MaxValue }; //ForeColor, BackColor, Symbol, Score

            for (int rChar = rList.Length; rChar > 0; rChar--)
            {
                for (int cFore = 0; cFore < cTable.Length; cFore++)
                {
                    for (int cBack = 0; cBack < cTable.Length; cBack++)
                    {
                        int R = (cTable[cFore].R * rChar + cTable[cBack].R * (rList.Length - rChar)) / rList.Length;
                        int G = (cTable[cFore].G * rChar + cTable[cBack].G * (rList.Length - rChar)) / rList.Length;
                        int B = (cTable[cFore].B * rChar + cTable[cBack].B * (rList.Length - rChar)) / rList.Length;
                        int iScore = (cValue.R - R) * (cValue.R - R) + (cValue.G - G) * (cValue.G - G) + (cValue.B - B) * (cValue.B - B);
                        if (!(rChar > 1 && rChar < 4 && iScore > 50000)) // rule out too weird combinations
                        {
                            if (iScore < bestHit[3])
                            {
                                bestHit[3] = iScore; //Score
                                bestHit[0] = cFore;  //ForeColor
                                bestHit[1] = cBack;  //BackColor
                                bestHit[2] = rChar;  //Symbol
                            }
                        }
                    }
                }
            }
            Console.ForegroundColor = (ConsoleColor)bestHit[0];
            Console.BackgroundColor = (ConsoleColor)bestHit[1];
            Console.Write(rList[bestHit[2] - 1]);
        }
        /// <summary>
        /// Consoles the write image.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void ConsoleWriteImage(Bitmap source)
        {
            int sMax = 19;
            decimal percent = Math.Min(decimal.Divide(sMax, source.Width), decimal.Divide(sMax, source.Height));
            Size dSize = new Size((int)(source.Width * percent), (int)(source.Height * percent));
            Bitmap bmpMax = new Bitmap(source, dSize.Width * 2, dSize.Height);
            for (int i = 0; i < dSize.Height; i++)
            {
                for (int j = 0; j < dSize.Width; j++)
                {
                    ConsoleWritePixel(bmpMax.GetPixel(j * 2, i));
                    ConsoleWritePixel(bmpMax.GetPixel(j * 2 + 1, i));
                }
                System.Console.WriteLine();
            }
            Console.ResetColor();
        }

        /// <summary>
        /// Displays the annotated image.
        /// </summary>
        /// <param name="aimg">The aimg.</param>
        public static void DisplayAnnotatedImage(AnnotatedImage aimg)
        {
            var eyes = aimg.annotation;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Eye Confidence{0}", aimg.annotation.confidence);
            Console.WriteLine("Left Eye X:{0} Y:{1} Right Eye X:{2} Y:{3}", eyes.left.x, eyes.left.y, eyes.right.x, eyes.right.y);
            Console.WriteLine("------------------------------------------");
            var imgbmp = ByteToBitmap(aimg.img.binaryImg);
            ConsoleWriteImage(imgbmp);

        }


        /// <summary>
        /// Bytes to image.
        /// </summary>
        /// <param name="blob">The BLOB.</param>
        /// <returns>Bitmap.</returns>
        public static Bitmap ByteToBitmap(byte[] blob)
        {
            using (MemoryStream mStream = new MemoryStream())
            {
                mStream.Write(blob, 0, blob.Length);
                mStream.Seek(0, SeekOrigin.Begin);

                Bitmap bm = new Bitmap(mStream);
                return bm;
            }
        }

        public static System.Drawing.Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }


        /// <summary>
        /// Images to byte2.
        /// </summary>
        /// <param name="img">The img.</param>
        /// <returns>System.Byte[].</returns>
        public static byte[] ImageToByte2(System.Drawing.Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        /// <summary>
        /// Gets the image list.
        /// </summary>
        /// <param name="imgNames">The img names.</param>
        /// <returns>IEnumerable&lt;System.Drawing.Image&gt;.</returns>
        public static IEnumerable<System.Drawing.Image> GetImageList(IEnumerable<string> imgNames)
        {

            var result = new List<System.Drawing.Image>();

            imgNames.ToList().ForEach(x =>
            {
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        //var img = new Image();
                        var image = System.Drawing.Image.FromFile(x);
                        image.Save(ms, image.RawFormat);
                        result.Add(image);
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }

            });

            return result;

        }

        /// <summary>
        /// A quick lookup for getting image encoders
        /// </summary>
        private static Dictionary<string, ImageCodecInfo> _encoders = null;

        /// <summary>
        /// Gets the image thumb nail.
        /// </summary>
        /// <param name="imageStream">The image stream.</param>
        /// <returns>Stream.</returns>
        /// <exception cref="Exception">Image is not valid format</exception>
        /// <exception cref="ApplicationException">Failed to find codec for file type ["+imgformat.ToString()+"]</exception>
        public static  Stream GetImageThumbNail(Stream imageStream)
        {
            try
            {
                var image = System.Drawing.Image.FromStream(imageStream);

                ImageFormat imgformat = null;

                if (ImageFormat.Jpeg.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Jpeg;
                }
                else if (ImageFormat.Gif.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Gif;
                }
                else if (ImageFormat.Png.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Png;
                }
                else if (ImageFormat.Bmp.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Bmp;
                }
                else if (ImageFormat.Tiff.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Tiff;
                }
                else if (ImageFormat.Emf.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Emf;
                }
                else if (ImageFormat.Icon.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Icon;
                }
                else if (ImageFormat.Wmf.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Wmf;
                }
                else if (ImageFormat.Exif.Equals(image.RawFormat))
                {
                    imgformat = ImageFormat.Exif;
                }

                if (imgformat == null)
                {
                    Log.LogError("Image is not valid format");
                    throw new Exception("Image is not valid format");
                }

                Rectangle rect = new Rectangle();

                var width = image.Width;
                var height = image.Height;

                int newheight = 0;
                int newwidth = 0;

                if (width > height)
                {
                    newwidth = height;
                    newheight = height;

                    var xval = ((width/2) - (height/2));
                    rect = new Rectangle(xval, 0, newwidth, newheight);
                }
                else if (height > width)
                {
                    newwidth = width;
                    newheight = width;
                    rect = new Rectangle(0, 0, newwidth, newheight);
                }
                else if (width == height)
                {
                    newwidth = width;
                    newheight = height;
                    rect = new Rectangle(0, 0, newwidth, newheight);
                }

                if (newwidth > 224 && newheight > 224 && newwidth == newheight)
                {
                    newwidth = 244;
                    newheight = 244;
                }
                               
                Bitmap newBitmap = new Bitmap(newwidth, newheight);
                using (Graphics g = Graphics.FromImage(newBitmap))
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.DrawImage(image, new Rectangle(0, 0, newwidth, newheight), rect, GraphicsUnit.Pixel);
                }

                //create an encoder parameter for the image quality
                EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                //get the codec
                ImageCodecInfo codec = GetEncoderInfo("image/" + imgformat.ToString());

                if (codec == null)
                {
                    throw new ApplicationException("Failed to find codec for file type ["+imgformat.ToString()+"]");
                }

                //create a collection of all parameters that we will pass to the encoder
                EncoderParameters encoderParams = new EncoderParameters(1);
                //set the quality parameter for the codec
                encoderParams.Param[0] = qualityParam;
                
                var stream = new MemoryStream();
                newBitmap.Save(stream, codec, encoderParams);

                return stream;
            }
            catch (Exception e)
            {
                Log.LogError( e, $"Failed to Crop Image File ");
                return null;
            }
        }


        /// <summary>
        /// Returns the image codec with the given mime type
        /// </summary>
        /// <param name="mimeType">Type of the MIME.</param>
        /// <returns>ImageCodecInfo.</returns>
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            //if the quick lookup isn't initialised, initialise it
            if (_encoders == null)
            {
                _encoders = new Dictionary<string, ImageCodecInfo>();
            }

            //if there are no codecs, try loading them
            if (_encoders.Count == 0)
            {
                //get all the codecs
                foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                {
                    //add each codec to the quick lookup
                    _encoders.Add(codec.MimeType.ToLower(), codec);
                }
            }

            //do a case insensitive search for the mime type
            string lookupKey = mimeType.ToLower();

            //the codec to return, default to null
            ImageCodecInfo foundCodec = null;

            //if we have the encoder, get it to return
            if (_encoders.ContainsKey(lookupKey))
            {
                //pull the codec from the lookup
                foundCodec = _encoders[lookupKey];
            }

            return foundCodec;
        } 





    }
}
