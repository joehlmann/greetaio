﻿using System;
using System.IO;
using System.Threading.Tasks;

//namespace VRAPI.Common.Utilities
//{

    /// <summary>
    /// Available Redis Databases. Defaults to the General Database
    /// </summary>
        /// <summary>
        /// Amazon Web Service S3 Bucket Service
        /// </summary>
//    public class S3Utils : BaseUtil
//    {

//        private static string ACCESSKEY;
//        private static string SECRETKEY;
//        private static string BUCKET;
//        private static string SERVICE_URL;
        

//        private static IAmazonS3 S3Client = null;

//        static S3Utils()
//        {
//            ACCESSKEY = ConfigurationUtils.GetConfigItem("rma.S3.AccessKey", true);
//            SECRETKEY = ConfigurationUtils.GetConfigItem("rma.S3.SecretKey", true);
//            BUCKET = ConfigurationUtils.GetConfigItem("rma.S3.BucketName", true);
//            SERVICE_URL = ConfigurationUtils.GetConfigItem("rma.S3.ServiceURL", true);

//            AmazonS3Config config = new AmazonS3Config
//            {
//                ServiceURL = SERVICE_URL
//            };

//            S3Client = AWSClientFactory.CreateAmazonS3Client(ACCESSKEY, SECRETKEY, config);

//        }

//        public static bool DeleteImageFromS3(Guid imageId)
//        {
//            try
//            {
//                var deleteObjectRequest = new DeleteObjectRequest
//                {
//                    BucketName = BUCKET,
//                    Key = imageId.ToString()
//                };

//                var result = S3Client.DeleteObject(deleteObjectRequest);

//                if (result.HttpStatusCode == System.Net.HttpStatusCode.NoContent)
//                {
//                    return true;
//                }
//                else
//                {
//                    _log.Error("Failed to delete ["+imageId+"] from S3, go status code ["+result.HttpStatusCode+"]");
//                    return false;
//                }


                
//            }
//            catch (Exception e)
//            {
//                _log.Error("Failed to delete image ["+imageId+"] from S3", e);
//                return false;
//            }
//        }

        

//        public static async Task<Stream> DownloadFileFromS3Async(string filename)
//        {
//            try
//            {
//                GetObjectRequest request = new GetObjectRequest
//                {
//                    BucketName = BUCKET,
//                    Key = filename
//                };

//                var result = await S3Client.GetObjectAsync(request);

//                return result.ResponseStream;

//            }
//            catch (Exception e)
//            {
//                _log.Error("Failed to Download File from S3", e);
//                return null;
//            }
//        }

//        public static bool UploadImageToS3(string filename, Stream stream)
//        {
//            try
//            {

//                if (stream == null)
//                {
//                    _log.Error("File Stream is null, can not upload image to s3");
//                    return false;
//                }

//                string KEY = filename;

//                // Check If object exists
//                try
//                {

//                    var putObjectRequest = new PutObjectRequest
//                    {
//                        BucketName = BUCKET,
//                        Key = KEY,
//                        InputStream = stream
//                    };

//                    S3Client.PutObject(putObjectRequest);

//                    return true;

//                }
//                catch (Exception e)
//                {
//                    // Does not exist so lets add it...
//                    _log.Error("Failed to upload image with id [" + filename + "]", e);
//                    return false;
//                }

//            }
//            catch (Exception e)
//            {
//                _log.Error("Failed to upload image to s3", e);
//                return false;
//            }
//        }



//        public static bool UploadImageToS3(Guid imageId, Stream stream)
//        {

//             return UploadImageToS3(imageId.ToString(), stream);


//        }
//    }
//}

