﻿using System.IO;

namespace GreetaIO.BioClient.Models
{
    public class VerifyDto
    {
        public string TargetId { get; private set; }

        public System.Drawing.Image Image { get; private set; }

        public string AuthName { get; private set; }

        public byte[] ImgStream { get; private set; }

        public VerifyDto(string targetId,string authName, System.Drawing.Image image,byte[] imgStream )
        {
            TargetId = targetId;
            AuthName = authName;
            Image = image;
            ImgStream = imgStream;

        }
        public VerifyDto(string targetId, string authName, System.Drawing.Image image) : this (targetId,authName,image,imgStream: new byte[0]){
        }

        public VerifyDto(string targetId, string authName, byte[] imgStream) : this(targetId, authName, System.Drawing.Image.FromStream(Stream.Null) , imgStream){
        }

    }
}
