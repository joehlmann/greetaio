﻿using System.Collections.Generic;

namespace GreetaIO.BioClient.Models
{
    public class FirDto :BaseModel
    {
        public string CaseId { get; private set; }

        public string LibraryId { get; private set; }

        public IEnumerable<System.Drawing.Image> ImgSet { get; private set; }

        public IEnumerable<byte[]> ByteSet { get; private set; }

        public string AuthName { get; private set; }

        private FirDto(string caseId, string libraryId, IEnumerable<byte[]> byteSet,IEnumerable<System.Drawing.Image> imgSet )
        {
            CaseId = caseId;
            LibraryId = libraryId;
            AuthName= libraryId;
            ByteSet = byteSet;
            ImgSet = imgSet;
        }

        public FirDto(string caseId, string libraryId,IEnumerable<byte[]> byteSet ) : this (caseId,libraryId,byteSet,new List<System.Drawing.Image>())   {
        }

        public FirDto(string caseId, string libraryId, IEnumerable<System.Drawing.Image> imgSet) : this(caseId, libraryId,new List<byte[]>(),imgSet){
        }
    }
}
