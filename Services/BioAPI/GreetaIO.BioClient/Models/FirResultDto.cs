﻿using System;
using System.Collections.Generic;
using GreetaIO.BioClient.Interfaces;

namespace GreetaIO.BioClient.Models
{
    public class FirResultDto
    {
        public string TargetId { get; private set; }

        public string TransactionId { get; private set; }

        public IEnumerable<ImageProcessingInfo> ImgSetResult { get; private set; }

        public bool Success { get; private set; }

        public string AuthName { get; private set; }

        public FirResultDto(string transactionId, string targetId, string authName, IEnumerable<ImageProcessingInfo> imgSet, bool success)
        {
            Success = success;
            TransactionId = transactionId;
            TargetId = targetId;
            AuthName = authName;
            ImgSetResult = imgSet;
        }

        public FirResultDto()
        {
            throw new NotImplementedException();
        }
    }
}
