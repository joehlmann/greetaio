﻿using System;
using System.Collections.Generic;

namespace GreetaIO.BioClient.Models
{
    public abstract class BaseModel
    {
        public virtual bool TryValidate()
        {
            return true;
        }

        public List<String> Errors { get; set; }

        public bool IsValid { get; set; }

        public bool HasBeenValidated { get; set; }
    }
}
