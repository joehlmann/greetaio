﻿using GreetaIO.BioClient.Interfaces;

namespace GreetaIO.BioClient.Models
{
    public class VerifyResultDto
    {
        public string TransactionId { get; private set; }

        public ImageProcessingInfo ImageProcessingInfo { get; private set; }

        public string AuthName { get; private set; }

        public bool NotEnrolled { get; private set; }

        public bool Verified { get; private set; }

        public float VerifyScore { get; private set; }
        public string TargetId { get; private set; }

        public VerifyResultDto(string targetId,string transactionId,string authName,ImageProcessingInfo imageProcessingInfo,bool notEnrolled,bool verified,float verifyScore)
        {
            TransactionId = transactionId;
            TargetId = targetId;
            AuthName = authName;
            ImageProcessingInfo = imageProcessingInfo;
            NotEnrolled = notEnrolled;
            Verified = verified;
            VerifyScore = verifyScore;


        }
        public VerifyResultDto(string targetId, string authName) : this (targetId,"000000",authName,new ImageProcessingInfo(), true,false,0){
        }

        

    }
}
