﻿using GreetaIO.BioClient.Interfaces;

namespace GreetaIO.BioClient.Models
{
    public class MatchResultDto
    {
        public string TransactionId { get; private set; }

        public ImageProcessingInfo ProcessingInfo { get; private set; }

        public Match[] Matches { get; private set; }

        public MatchResultDto(string transactionId,ImageProcessingInfo processingInfo,Match[] matches)
        {
            TransactionId = transactionId;
            Matches = matches;
            ProcessingInfo = processingInfo;
        }

    }
}
