﻿using System.Collections.Generic;

namespace GreetaIO.BioClient.Models
{
    public class TargetProfileDto :BaseModel
    {
        public Dictionary<string,string> Properties { get; private set; }

        public TargetProfileDto(Dictionary<string,string> properties)
        {
            Properties = properties;

        }

    }
}
