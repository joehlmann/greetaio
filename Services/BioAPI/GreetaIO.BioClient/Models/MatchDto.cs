﻿namespace GreetaIO.BioClient.Models


{
    public class MatchDto :BaseModel 
    {
        public System.Drawing.Image Image { get; private set; }

        public float MatchThreshold { get; private set; }

        public int MaxMatches { get; private set; }

        public string AuthName { get; private set; }

        public string LibraryId { get;private set; }

        public string EventId { get;private set; }

        

        
        public MatchDto(string eventId,  string libraryId , System.Drawing.Image image, string authName, float matchThreshold = 0.5f, int maxMatches = 1)
        {
            EventId = eventId;
            LibraryId = libraryId;
            AuthName = authName;
            Image = image;
            MatchThreshold = matchThreshold;
            MaxMatches = maxMatches;
        }
    }
}
