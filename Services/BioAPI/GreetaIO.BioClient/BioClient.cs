﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using GreetaIO.BioClient.DI;
using GreetaIO.BioClient.Interfaces;
using GreetaIO.BioClient.Models;
using GreetaIO.BioClient.Utilities;
using Serilog.Core;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioClient
{
    public class BioClient : IBioClient
    {
        private string _endpoint;
        private ILogger<BioClient> _log;

        private List<string> _imgList = new List<string>();
        private string _authname = System.Security.Principal.WindowsIdentity.GetCurrent().Name;


        private int _refImgSize = 100;

        private ImageFormat _imgType = ImageFormat.Jpeg;


        private IFVDBScanPortTypeClient _proxy;

        public BioClient(ILogger<BioClient> log, string endpoint)
        {
            _proxy = new FVDBScanPortTypeClient(new WebClientBinding(),new EndpointAddress(endpoint));
            _log = log;
            _endpoint = endpoint;
        }

        public async Task<bool> CheckIsAliveAsync()
        {
            _log.LogInformation($"{this.ToString()} {MethodBase.GetCurrentMethod().Name}");
            var result = await _proxy.aliveAsync(new aliveRequest()).ConfigureAwait(false);
            return result != null;
        }

        public async Task<bool> TargetIdExistsAsync(string targetId)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");
            var result = await _proxy.fvdbcaseExistsAsync(new fvdbcaseExistsRequest(targetId)).ConfigureAwait(false);

            return result.exists;
        }

        public async Task TargetIdCreateAsync(string targetId, Dictionary<string, string> properties)
        {
            _log.LogInformation($"{this.ToString()} {MethodBase.GetCurrentMethod().Name}");

            var resDic = properties.Select(x => new CaseProperty
            {
                name = x.Key,
                value = x.Value
            });

            await _proxy.fvdbaddCaseAsync(new fvdbaddCaseRequest(targetId, resDic.ToArray()))
                .ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} TargetId:{targetId} suceeded");

        }

        public async Task TargetDeleteAsync(string targetId)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} Start");

            await _proxy.fvdbdeleteCaseAsync(new fvdbdeleteCaseRequest(targetId)).ConfigureAwait(false);;

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} TargetId:{targetId} suceeded");

        }

        public async Task FirDeleteAsync(string targetId)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");

            await  _proxy.fvdbdeleteFIRAsync(new fvdbdeleteFIRRequest(targetId)).ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} for  TargetId:{targetId} Fir Delete Succeeded");

        }

        public async Task<FirResultDto> FirEnrollAsync(FirDto firDto)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");

            Image[] binImgSet;

            if (firDto.ByteSet != null)
            {
                binImgSet = firDto.ByteSet.Select(img => new Image
                    {
                        binaryImg = img
                    })
                    .ToArray();
            }
            else
            {
                binImgSet = firDto.ImgSet.Select(img => new Image
                    {
                        binaryImg = img.ImageToByteJpg()
                    })
                    .ToArray();
            }

            var response = await _proxy.enrollmentsetAsync( new enrollmentsetRequest(firDto.CaseId, binImgSet, firDto.AuthName)).ConfigureAwait(false);

            var result = new FirResultDto(
                response.val.transactionId,
                firDto.CaseId,
                firDto.AuthName,
                response.val.processedImages,
                response.val.successfullEnrolled
            );

            return result;
        }

        public async Task<MatchResultDto> MatchImgAsync(MatchDto matchDto)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");


            const float matchthreshold = 0.4f;


            var img = new Image
            {
                binaryImg = matchDto.Image.ImageToByteJpg()
            };
            var result = await _proxy.identificationAsync(new identificationRequest(matchDto.Image.ImageToSvcImage(),
                matchDto.AuthName,
                matchDto.MaxMatches,
                matchthreshold,
                0
            )).ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}  Succeeded  . Matches:{result.val.matches.Length}");

            return new MatchResultDto(result.val.transactionId, result.val.processedImage, result.val.matches);

        }

        public async Task<VerifyResultDto> VerifyImgAsync(VerifyDto verifyDto)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");


            if (!TargetIdExistsAsync(verifyDto.TargetId).Result)
            {
                return new VerifyResultDto(verifyDto.TargetId, verifyDto.AuthName);

            }


            var result = await _proxy.verificationAsync(new verificationRequest(verifyDto.TargetId,
                verifyDto.Image.ImageToSvcImage(), verifyDto.AuthName)).ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} TargetID {verifyDto.TargetId} Processed");

            return new VerifyResultDto(verifyDto.TargetId,
                result.val.transactionId,
                verifyDto.AuthName,
                result.val.processedImage,
                result.val.notEnrolled,
                result.val.successfulVerified,
                result.val.score
            );
        }

        public async Task<TargetProfileDto> GetTargetProfileAsync(string targetId)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");

            var result = await _proxy.fvdbgetCasePropertiesAsync(new fvdbgetCasePropertiesRequest(targetId)).ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} TargetID {targetId} profile found");

            return new TargetProfileDto(result.caseProperties.ToDictionary(
                property => property.name,
                property => property.value,
                StringComparer.OrdinalIgnoreCase
            ));
        }

        public async Task<byte[]> GetReferenceImageAsync(string targetId)
        {
            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name}");

            var res = await _proxy.fvdbgetReferenceImageAsync(new fvdbgetReferenceImageRequest(targetId, DefaultValues.ThumbSize)).ConfigureAwait(false);

            _log.LogInformation($"{MethodBase.GetCurrentMethod().Name} TargetID {targetId} Image found");

            return res.face.img.binaryImg;
        }

       
    }

    static class DefaultValues
    {
        public static int ThumbSize = 30;

    }
}
