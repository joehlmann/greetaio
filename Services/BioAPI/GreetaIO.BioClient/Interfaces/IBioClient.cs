﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreetaIO.BioClient.Models;

namespace GreetaIO.BioClient.Interfaces
{
    public interface IBioClient
    {
        /// <summary>
        /// Heartbeat System Up
        /// </summary>
        /// <returns></returns>
        Task<bool> CheckIsAliveAsync();

        /// <summary>
        /// Check id Exists
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns>bool</returns>
        Task<bool> TargetIdExistsAsync(string targetId);


        /// <summary>
        /// Create Target
        /// </summary>
        /// Add Case
        /// <param name="targetId"></param>
        /// <param name="properties"></param>
        Task TargetIdCreateAsync(string targetId,Dictionary<string,string> properties );

        /// <summary>
        /// Delete Target
        /// </summary>
        /// Add Case
        /// <param name="targetId"></param>
        Task TargetDeleteAsync(string targetId);

        /// <summary>
        /// Delete Fir
        /// </summary>
        /// Add Case
        /// <param name="targetId"></param>
        Task FirDeleteAsync(string targetId);

        /// <summary>
        /// Create FIR Record From Images
        /// </summary>
        /// <param name="firDto"></param>
        /// <returns></returns>
        Task<FirResultDto> FirEnrollAsync(FirDto firDto);

        /// <summary>
        /// Match Image 
        /// </summary>
        /// <param name="matchDto"></param>
        /// <returns></returns>
        Task<MatchResultDto> MatchImgAsync(MatchDto matchDto);

        /// <summary>
        /// Verify Image 
        /// </summary>
        /// <param name="verifyDto"></param>
        /// <returns></returns>
        Task<VerifyResultDto> VerifyImgAsync(VerifyDto verifyDto);

        /// <summary>
        /// Verify Image 
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></returns>
        Task<TargetProfileDto> GetTargetProfileAsync(string targetId);

        /// <summary>
        /// Get Reference Image
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></returns>
        Task<byte[]> GetReferenceImageAsync(string targetId);
        

    }
}