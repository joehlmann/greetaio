﻿namespace GreetaIO.BioAPI.Settings
{
    public class VrSettingsConfig 
    {
        public string AuthName { get; set; }
        public int NumMatches { get; set; }
        public float MatchThreshold { get; set; }
    }
}
