﻿namespace GreetaIO.BioAPI.Settings
{
    public class CacheConfig
    {

        public string RedisConfigurationKey { get; set; }
        public string RedisConnection { get; set; }

        public string RedisPort { get; set; }

    }
}
