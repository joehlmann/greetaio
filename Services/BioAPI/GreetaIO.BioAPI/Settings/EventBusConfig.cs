﻿namespace GreetaIO.BioAPI.Settings
{
    public class EventBusConfig
    {

        public string EventBusUserName { get; set; }
        public string EventBusPassword { get; set; }

        public string EventBusConnection { get; set; }

        public string EventBusManagementPort { get; set; }
        public string EventBusEnvVarNodeName { get; set; }
        public string EventBusEnvVarNodePort { get; set; }
        
        public string SubscriptionClientName { get; set; }

        public string EventBusRetryCount { get; set; }
    }
}
