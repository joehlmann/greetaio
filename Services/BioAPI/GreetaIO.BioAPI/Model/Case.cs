﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreetaIO.Utility;

namespace GreetaIO.BioAPI.Model
{
    public class Case :EntitySimple<int>
    {
        public int Id { get; set; }

        public string CaseNumber { get; set; }

        public string Name { get; set; }

        

        public CaseType CaseType { get; set; }
    }

    public enum CaseType
    {
        Empty,
        Default

    }
}
