﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreetaIO.Utility;

namespace GreetaIO.BioAPI.Model
{
    public class Image :EntitySimple<int>
    {
        public int ImageNumber { get; set; }

        public string Name { get; set; }



        public ImageType ImageType { get; set; }
    }

    public enum ImageType
    {
        Empty,
        Default

    }
}
