﻿using System.Collections.Generic;

namespace GreetaIO.BioAPI.Dtos
{
    public class LibraryDTO
    {
        public int LibraryId { get; set; }

        public int LibraryNumber { get; set; }

        public string Name { get; set; }


        public string Description { get; set; }

        public string LibraryType { get; set; }

        public int LocationId { get; set; }

        public IEnumerable<UserDTO> LibraryUsers {get;set;}
       

        public static LibraryDTO Empty()
        {
            return new LibraryDTO()
            {
                LibraryType = DtoState.Empty()
            };


        }
    }

   
}
