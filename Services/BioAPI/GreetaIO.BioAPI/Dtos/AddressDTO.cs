﻿namespace GreetaIO.BioAPI.Dtos
{
    public class AddressDTO
    {
        public string Address1 { get;  set; }
        public string Address2 { get;  set; }
        public string PostCode { get;  set; }
        public string State { get;  set; }
        public string Suburb { get;  set; }

    }
}
