﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GreetaIO.BioAPI.Application.IntegrationEvents.Events;
using GreetaIO.BioClient.Models;

namespace GreetaIO.BioAPI.ServicesApi
{
    public interface IVrApiClient
    {
        Task<FindResponseDto> FindUserFromUserImageAsync(UserImageAtLocationIntEvent @event);

        Task<VerifyResultDto> VerifyImageAsync(VerifyDto dto);

        Task<FirResultDto> EnrollAImageAsync(ImageAddedToUserIntEvent @event);

        Task<FirResultDto> EnrollImagesAsync(IEnumerable<ImageAddedToUserIntEvent> events);

        Task CreateCase(string userId, int libraryId, int locationId);
    }

    public class FindResponseDto
    {
        public string EventId { get; set; }

        public IEnumerable<FindResultDto> FindResultDtos { get; set; }

        public bool IsEmpty { get; set; }


    }

    public enum FindResponseType
    {
        Empty,
        Match

    }

    public class FindResultDto
    {

        public int RankField { get; set; }

        public int UserId { get; set; }

        public float ScoreField { get; set; }
        
    }
}
