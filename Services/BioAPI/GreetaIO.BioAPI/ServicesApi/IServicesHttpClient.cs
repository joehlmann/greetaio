﻿using System.Net.Http;
using System.Threading.Tasks;

namespace GreetaIO.BioAPI.ServicesApi
{
    public interface IServicesHttpClient
    {
        
        HttpRequestMessage CreateRequest(string uri, HttpMethod method);
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
}