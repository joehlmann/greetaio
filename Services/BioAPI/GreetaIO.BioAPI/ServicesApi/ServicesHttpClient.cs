﻿using System.Net.Http;
using System.Threading.Tasks;
using GreetaIO.BioAPI.Infrastructure.TokenContext;
using GreetaIO.Utility.Infrastructure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace GreetaIO.BioAPI.ServicesApi
{
    public class ServicesHttpClient : IServicesHttpClient
    {
        
        private readonly ITokenContextService _tokenService;
        private HttpClient _httpClient;
        //private readonly ILogger<OrderApiClient> _logger;
        //private readonly UrlsConfig _urls;

        public ServicesHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public ServicesHttpClient(HttpClient client, ITokenContextService tokenService)
        {
            _httpClient = client;
            _tokenService = tokenService;
        }

        

        public HttpRequestMessage CreateRequest(string uri, HttpMethod method)
        {
            var request = new HttpRequestMessage(method, uri);
            var accessToken = _tokenService.GetAccessToken();
            if (accessToken.IsPresent())
            {
                request.Properties.Add("access_token", accessToken);
            }

            return request;
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            var result = await _httpClient.SendAsync(request);

            return result;
        }

        //public async Task<OrderData> GetOrderDraftFromBasketAsync(BasketData basket)
        //{
        //    var uri = _urls.Orders + UrlsConfig.OrdersOperations.GetOrderDraft();
        //    var content = new StringContent(JsonConvert.SerializeObject(basket), System.Text.Encoding.UTF8, "application/json");
        //    var response = await _apiClient.PostAsync(uri, content);

        //    response.EnsureSuccessStatusCode();

        //    var ordersDraftResponse = await response.Content.ReadAsStringAsync();

        //    return JsonConvert.DeserializeObject<OrderData>(ordersDraftResponse);
        //}
    }
}
