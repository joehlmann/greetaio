﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreetaIO.BioAPI.ServicesApi
{
    public class UrlsConfig
    {
        public class ApiOperations
        {
            public static string GetUserById(int id) => $"/api/v1/user/{id}";
            public static string GetCustomersById(IEnumerable<int> ids) => $"/api/v1/catalog/items?ids={string.Join(',', ids)}";

            public static string GetUserImagesByUserId(int id) => $"/api/v1/user/{id}/userimages";
        }

        public class BioApiOperations
        {
            public static string GetUserById(string id) => $"/api/v1/basket/{id}";
            public static string FindUserImage() => "/api/v1/basket";
        }
        public class VrApiOperations
        {
            public static string GetUserById(string id) => $"/api/v1/basket/{id}";
            public static string MatchUserImage() => "/api/v1/basket";
        }


        public string Api { get; set; }
        public string BioApi { get; set; }
        public string VrApi { get; set; }

        public string GrpcApi { get; set; }
        public string GrpcBioApi { get; set; }
        
    }
}
