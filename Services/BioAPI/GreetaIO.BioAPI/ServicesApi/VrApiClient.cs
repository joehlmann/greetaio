﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GreetaIO.BioAPI.Application.IntegrationEvents.Events;
using GreetaIO.BioAPI.Dtos;
using GreetaIO.BioClient.Interfaces;
using GreetaIO.BioClient.Models;
using GreetaIO.BioClient.Utilities;
using GreetaIO.Utility.Helper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace GreetaIO.BioAPI.ServicesApi
{
    public class VrApiClient : IVrApiClient
    {
        private readonly HttpClient _apiClient;
        private readonly ILogger<VrApiClient> _logger;
        private readonly UrlsConfig _urls;
        private readonly IBioClient _bioClient;

        public VrApiClient(IBioClient bioClient,  HttpClient httpClient, ILogger<VrApiClient> logger, IOptions<UrlsConfig> config)
        {
            _bioClient = bioClient;
            _apiClient = httpClient;
            _logger = logger;
            _urls = config.Value;
        }

        public async Task<UserData> FindUserFromUserImage(UserImageDTO basket)
        {
            var uri = _urls.VrApi + UrlsConfig.VrApiOperations.MatchUserImage();
            var content = new StringContent(JsonConvert.SerializeObject(basket), System.Text.Encoding.UTF8, "application/json");
            var response = await _apiClient.PostAsync(uri, content);

            response.EnsureSuccessStatusCode();

            var ordersDraftResponse = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<UserData>(ordersDraftResponse);
        }


        public async Task<FindResponseDto> FindUserFromUserImageAsync(UserImageAtLocationIntEvent @event)
        {
            
            var matchRequest = new MatchDto(@event.Id.ToString(),@event.LibraryId.ToString(), @event.ImageData.ToByte().ByteToImage(),@event.ClientName);

            var result = await  _bioClient.MatchImgAsync(matchRequest);

            var response = new FindResponseDto()
            {
                EventId = @event.Id.ToString(),
                FindResultDtos = result.Matches.Select(m=> new FindResultDto()
                {
                    RankField = m.rank,
                    ScoreField = m.score,
                    UserId = m.caseID.ToInt()
                }),
                IsEmpty = !result.Matches.Any()
            };

            return response;
        }

        public async Task<VerifyResultDto> VerifyImageAsync(VerifyDto dto)
        {
            throw new NotImplementedException();
        }

        public async Task<FirResultDto> EnrollAImageAsync(ImageAddedToUserIntEvent @event)
        {
            var enrollRequest = new FirDto(@event.UserId.ToString(),@event.LibraryId.ToString(),new List<byte[]>{ @event.ImageData.ToByte() });

            var result = await  _bioClient.FirEnrollAsync(enrollRequest);

            return result;

        }

        public Task<FirResultDto> EnrollImagesAsync(IEnumerable<ImageAddedToUserIntEvent> events)
        {
            throw new NotImplementedException();
        }

        public async Task CreateCase(string targetId, int libraryId, int locationId)
        {
            throw new NotImplementedException();
        }
    }

    public class UserData
    {
    }

    
}
