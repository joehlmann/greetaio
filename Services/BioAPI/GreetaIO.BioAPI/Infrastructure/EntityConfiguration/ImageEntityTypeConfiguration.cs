﻿using System;
using GreetaIO.BioAPI.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.BioAPI.Infrastructure.EntityConfiguration
{
    public class ImageEntityTypeConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
           
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            

            builder.ToTable("Images");

            builder.HasKey(t => t.Id);

            builder.Property(c => c.Id).HasColumnName("ImageId").ValueGeneratedOnAdd().UseHiLo("ImageHiLo");

           
            builder.Property(c => c.ImageNumber)
                .HasColumnType("int")
                .IsRequired();

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");
        }
    }
}
