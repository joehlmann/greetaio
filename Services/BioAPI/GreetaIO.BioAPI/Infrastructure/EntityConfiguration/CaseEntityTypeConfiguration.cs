﻿using System;
using GreetaIO.BioAPI.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.BioAPI.Infrastructure.EntityConfiguration
{
    public class CaseEntityTypeConfiguration : IEntityTypeConfiguration<Case>
    {
        public void Configure(EntityTypeBuilder<Case> builder)
        {
            
            
            


            builder.ToTable("Cases");

            builder.HasKey(l => l.Id);

            builder.Property(u => u.Id).HasColumnName("CaseId").ValueGeneratedOnAdd().UseHiLo("CaseHiLo");

            builder.Property(c => c.Id)
                .HasColumnType("int");

            

            // Type

            builder.Property(c => c.CaseType)
                .HasConversion(
                    v => v.ToString(),
                    v => (CaseType)Enum.Parse(typeof(CaseType), v));



            //Address value Type
            

            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())"); 

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");




        }
    }
}
