﻿using GreetaIO.BioAPI.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.BioAPI.Infrastructure.EntityConfiguration
{
    public class ClientRequestEntityTypeConfiguration : IEntityTypeConfiguration<ClientRequest>
    {
        public void Configure(EntityTypeBuilder<ClientRequest> builder)
        {
            builder.ToTable("Requests");
            builder.HasKey(cr => cr.Id);
            builder.Property(u => u.Id).HasColumnName("RequestId");
            builder.Property(cr => cr.Id).IsRequired();
            builder.Property(cr => cr.RequestType).IsRequired();
        }
    }
}
