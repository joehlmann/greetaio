﻿namespace GreetaIO.BioAPI.Infrastructure.DelegateHandlersCorrelation
{
    public interface ICorrelationContextService
    {
        void SetCorrelationId(string id);
        void SetCorrelationHeader(string headerName);
        string GetCorrelationId();
        string GetCorrelationHeader();
    }
}