﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GreetaIO.BioAPI.Model;
using Microsoft.EntityFrameworkCore.Internal;

namespace GreetaIO.BioAPI.Infrastructure
{
    public  class BioContextSeed
    {
        public static async Task SeedAsync(BioContext context)
        {


            using (context)
            {

                //context.Database.EnsureDeleted();
                //context.SaveChanges();

                //context.Database.Migrate();
                //context.Database.EnsureCreated();



                

                //if (!context.Customers.Any())

                //{
                   
                //        context.Customers.AddRange(GetPreconfiguredCustomers());

                //        context.SaveChanges();


                //}

                //if (!context.Users.Any())
                //{

                    
                //    context.Users.Add(new User(
                //        "fred",
                //        Contact.Create("fred","12345678","1@1.com"),
                //        (int) UserType.New, 0));

                    
                //    context.Users.Add(new User(
                //        "mia",
                //        Contact.Create("miapia", "8910111213", "mia@greeta.com"),
                //        (int)UserType.New, 0));



                //    await context.SaveChangesAsync();
                //}

                //if (!EnumerableExtensions.Any(context.Locations))
                //{
                //    var customer = await context.Customers.FirstAsync();
                //    await context.Locations.AddRangeAsync(GetPreconfiguredLocations(customer));

                //    await context.SaveChangesAsync();
                //}

                //if (!context.Libraries.Any())
                //{
                //    var location = await context.Locations.FirstAsync();
                //    var user = await context.Users.FirstAsync();
                //    await context.Libraries.AddRangeAsync(GetPreconfiguredLibrary(user,location));

                //    await context.SaveChangesAsync();
                //}

                //if (!context.Items.Any())
                //{

                //    var item = Item.Create(EntityTypeRange.Item.ToInt(), "Patronage", "A shop Visit", ItemType.Visit,
                //        RewardPoints.Create(RewardsType.None, 0, 0), false);



                //    context.Items.Attach(item);

                //    await context.SaveChangesAsync();
                //}


               


                

                //if (!context.UserImages.Any())
                //{
                //    var user1 = context.Users.FirstOrDefault(u => u.Id == 1000);
                //    var user2 = context.Users.FirstOrDefault(u => u.Id != 1000);

                //    var userImage1 = UserImage.Create( File.ReadAllBytes("Resources/cat1.png"), "Resources/cat1.png");

                //    var userImage2 = UserImage.Create(File.ReadAllBytes("Resources/cat2.png"), "Resources/cat2.png");

                //    user1.AddUserImage(userImage1);

                //    user2.AddUserImage(userImage2);

                 

                //    await context.SaveChangesAsync();
                //}

                //if (!context.Transactions.Any())
                //{
                //    var location = await context.Locations.FirstAsync();
                //    var user = await context.Users.FirstAsync();
                //    var item = await context.Items.FirstOrDefaultAsync();
                //    await context.Transactions.AddRangeAsync(GetPreconfiguredTransaction(location, user, item));

                //    await context.SaveChangesAsync();
                //}

            }
        }

        private static IEnumerable<Case> GetPreconfiguredCase()
        {


            //var result = new List<Transaction>
            //{
            //    new Transaction()
            //    {
            //        Id = EntityTypeRange.Transaction.ToInt(),
            //        Item = item,
            //        TransactionType = TransactionType.Unknown,
            //        User = user,
            //        Location = location,
            //        PointsReward = item.GetPoints(),
            //        CreateDate = DateTime.Parse("30Nov2019"),
            //        LastModified = DateTime.Parse("30Nov2019"),
            //        CurrentUserId = "Me101"

            //    }
            //};
            
            return new Case[0];
        }


        

        private static IEnumerable<Image> GetPreconfiguredImageLibrary()
        {


            //var result =  new List<Library>
            //{
            //    Library.Create(EntityTypeRange.Library.ToInt(),
            //        "My Library","MyLib desc",LibraryType.Default,location)
                
            //};

            //result[0].AddLibUser(

            //    new LibraryUser
            //    {
            //        Library = result[0],
            //        User = user
            //    });
            
            return new Image[0];
        }

        private static IEnumerable<ClientRequest> GetPreconfiguredUser()
        {
            //var result = new Customer(
            //    EntityTypeRange.Customer.ToInt(),
            //    "MyCompany",
            //    "Vic", "Brighton", "508 hawthorn Rd", "", "3162",
            //    "Bill Bob", "0416544517", "bob@bobs.com.au");

            //result.LastModified = DateTime.Parse("30Nov2019");
            //result.CreateDate = DateTime.Parse("30Nov2019");
            //result.CurrentUserId = "Me101";
            //result.CustomerType = CustomerType.Default;
            return new List<ClientRequest>();

        }


       

    }
}
