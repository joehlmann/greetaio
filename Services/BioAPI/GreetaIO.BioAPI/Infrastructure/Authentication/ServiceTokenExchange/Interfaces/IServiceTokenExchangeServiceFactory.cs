﻿namespace GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface IServiceTokenExchangeServiceFactory
    {
        ITokenExchangeService GetInstance(bool enableCaching);
    }
}