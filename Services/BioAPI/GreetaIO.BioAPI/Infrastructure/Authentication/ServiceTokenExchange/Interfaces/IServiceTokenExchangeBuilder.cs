﻿using Microsoft.Extensions.DependencyInjection;

namespace GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface IServiceTokenExchangeBuilder
    {
        IServiceCollection Services { get; }
    }
}