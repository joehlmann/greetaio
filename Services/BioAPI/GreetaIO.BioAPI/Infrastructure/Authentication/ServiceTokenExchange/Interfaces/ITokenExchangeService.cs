﻿using System.Threading.Tasks;
using GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeService
    {
        Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options);
    }
}