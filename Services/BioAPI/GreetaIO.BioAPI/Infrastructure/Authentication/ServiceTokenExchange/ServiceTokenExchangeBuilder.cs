﻿using System;
using GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace GreetaIO.BioAPI.Infrastructure.Authentication.ServiceTokenExchange
{
    public class ServiceTokenExchangeBuilder : IServiceTokenExchangeBuilder
    {
        public ServiceTokenExchangeBuilder(IServiceCollection services)
        {
            Services = services ?? throw new ArgumentNullException(nameof(services));
        }
        
        public IServiceCollection Services { get; }
    }
}