﻿using System.Reflection;
using Autofac;
using EventBusAbstract.Abstractions;
using GreetaIO.BioAPI.Application.IntegrationEvents.EventHandlers;

namespace GreetaIO.BioAPI.Infrastructure.Modules
{

    public class ApplicationModule
        :Autofac.Module
    {

        public string QueriesConnectionString { get; }

        private ContainerBuilder _container;

        public ApplicationModule(ContainerBuilder container,string queriesConnectionString)
        {
            QueriesConnectionString = queriesConnectionString;
            _container = container;
        }

        protected override void Load(ContainerBuilder builder)
        {
            

            //builder.Register(c => new GreetaQueries(QueriesConnectionString))
            //    .As<IGreetaQueries>()
            //    .InstancePerLifetimeScope();

            //builder.RegisterType<CustomerRepository>()
            //    .As<ICrudRepository<Customer,int>>()
            //    .InstancePerLifetimeScope();

           

            builder.RegisterAssemblyTypes(typeof(UserImageAtLocationIntEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IIntegrationEventHandler<>));

          
        }
    }
}
