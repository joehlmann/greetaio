﻿using Autofac;
using GreetaIO.BioAPI.Application.IntegrationEvents.EventHandlers;
using Microsoft.Data.SqlClient;

namespace GreetaIO.BioAPI.Infrastructure.Modules
{
    public class ServicesModule : Module
    {

        protected string _connectionStringEventLog;



        public ServicesModule(string connectionStringEventLog)
        {
            _connectionStringEventLog = connectionStringEventLog;

            
        }

        protected override void Load(ContainerBuilder builder)
        {

            //builder.RegisterType<UserImageAtLocationIntEventHandler>()
            //    .As<UserImageAtLocationIntEventHandler>()
            //    .InstancePerLifetimeScope();




     

            // builder.RegisterType<EntityIdValidService>()
            //     .As<IEntityIdValidService>()
            //     .InstancePerLifetimeScope();

            // builder.RegisterType<CorrelationContextService>()
            //     .As<ICorrelationContextService>()
            //     .InstancePerLifetimeScope();

            // builder.RegisterType<ContactValidService>()
            //     .As<IContactValidService>()
            //     .InstancePerLifetimeScope();



            // builder.RegisterType<AddressValidService>()
            //     .As<IAddressValidService>()
            //     .InstancePerLifetimeScope();


            // builder.RegisterType<IntegrationEventLogService>()
            //     .WithParameter("dbConnection", new SqlConnection(_connectionStringEventLog))
            //     .As<IIntegrationEventLogService>()
            //     .InstancePerLifetimeScope();


            //// Add&Save Integration Service
            //builder.RegisterType<GreetaIntegrationEventSvc>()
            //     .As<IGreetaIntegrationEventSvc>()
            //     .InstancePerLifetimeScope();




            //Singleton
            //builder.RegisterType<DomainEventManagerInMemory>()
            //    .As<IDomainEventManagerInMemory>()
            //    .SingleInstance();

            //builder.RegisterType<RequestManager>()
            //    .As<IRequestManager>()
            //    .SingleInstance();




        }
    }
}
