﻿using GreetaIO.BioAPI.Infrastructure.TokenContext;

namespace GreetaIO.BioAPI.Infrastructure.Authorization
{
    public class TokenContextService : ITokenContextService
    {
        
        private string AccessToken { get; set; }
        public void SetAccessToken(string token)
        {
            AccessToken = token;
        }

        public string GetAccessToken()
        {
            return AccessToken;
        }

        
    }
}
