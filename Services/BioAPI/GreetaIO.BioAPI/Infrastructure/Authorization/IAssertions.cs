﻿using Microsoft.AspNetCore.Authorization;

namespace GreetaIO.BioAPI.Infrastructure.Authorization
{
    public interface IAssertions
    {
        bool CanReadPickups(AuthorizationHandlerContext context);
        bool CanWritePickups(AuthorizationHandlerContext context);
        bool CanDeletePickups(AuthorizationHandlerContext context);
    }
}