﻿using GreetaIO.BioAPI.Infrastructure.Authorization.Models;

namespace GreetaIO.BioAPI.Infrastructure.Authorization
{
    //TODO: Load these values from DB as they are reused across applications
    public static class GreetaIdentity
    {
        //TODO: This should come from a config as it will need to change with environments
        private static string _issuer = "https://borderexpress.idp";
        public static class Scopes
        {
            public static Scope ReadPickups = new Scope{Name = "pickup|read", Issuer = _issuer};
            public static Scope DeletePickups = new Scope{Name = "pickup|delete", Issuer = _issuer};
            public static Scope WritePickups = new Scope{Name = "pickup|write", Issuer = _issuer};
        }

        public static class Claims
        {
            public static Claim Scope = new Claim {Issuer = _issuer, Type = "scope", Value = ""};
        }
    }
}
