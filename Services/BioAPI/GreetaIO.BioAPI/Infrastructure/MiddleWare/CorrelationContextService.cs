﻿using GreetaIO.BioAPI.Infrastructure.DelegateHandlersCorrelation;

namespace GreetaIO.BioAPI.Infrastructure.MiddleWare
{
    public class CorrelationContextService : ICorrelationContextService
    {
        private string correlationId { get; set; }
        private string correlationHeader { get; set; }
        public string GetCorrelationHeader()
        {
            return correlationHeader;
        }

        public string GetCorrelationId()
        {
            return correlationId;
        }

        public void SetCorrelationHeader(string headerName)
        {
            correlationHeader = headerName;
        }

        public void SetCorrelationId(string id)
        {
            correlationId = id;
        }
    }
}
