﻿using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace GreetaIO.BioAPI.Infrastructure.Extensions
{
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Check the exception for any duplicate constraint error types
        /// </summary>
        /// <param name="e">Database exception</param>
        /// <returns>True - exception is of a duplicate constraint type</returns>
        public static bool IsUniqueSqlConstraintError(this DbUpdateException e)
        {
            return (e.InnerException?.InnerException is SqlException sqlIEx &&
                    (sqlIEx?.Number == 2601 || sqlIEx.Number == 2627)) ||
                   (e.InnerException is SqlException sqlEx && (sqlEx?.Number == 2601 || sqlEx.Number == 2627));
            // - 2601 Duplicated key row error, - 2627 Unique constraint error
        }
    }
}
