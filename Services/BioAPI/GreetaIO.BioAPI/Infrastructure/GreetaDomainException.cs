﻿using System;

namespace GreetaIO.BioAPI.Infrastructure
{
    public class GreetaDomainException : Exception
    {
        public GreetaDomainException()
        { }

        public GreetaDomainException(string message)
            : base(message)
        { }

        public GreetaDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
