﻿using System;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GreetaIO.BioAPI.Infrastructure.Filters
{
    public class AuthorizeCheckOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            // Check for authorize attribute
            //var hasAuthorize = context.MethodInfo.DeclaringType.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any() ||
            //                   context.MethodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any();

            //if (!hasAuthorize) return;

            //operation.Responses.TryAdd("401", new Response { Description = "Unauthorized" });
            //operation.Responses.TryAdd("403", new Response { Description = "Forbidden" });

            //operation.Security = new List<IDictionary<string, IEnumerable<string>>>
            //{
            //    new Dictionary<string, IEnumerable<string>>
            //    {
            //        { "oauth2", new [] { "orderingapi" } }
            //    }
            //};
        }

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            throw new NotImplementedException();
        }
    }
}
