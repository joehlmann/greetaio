﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Events;
using GreetaIO.BioAPI.Infrastructure;
using IntegrationEventLogEF;
using IntegrationEventLogEF.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioAPI.Application.IntegrationEvents
{
    public class BioIntegrationEventSvc : IIntegrationEventSvc
    {

        private readonly Func<DbConnection, IIntegrationEventLogService> _integrationEventLogServiceFactory;
        private readonly IEventBus _eventBus;
        private readonly BioContext _greetaBioContext;
        private readonly IntEventLogContext _eventLogContext;
        private readonly IIntegrationEventLogService _eventLogService;
        private readonly ILogger<BioIntegrationEventSvc> _logger;
        private readonly ICorrelationContextAccessor _correlationContext;

        public BioIntegrationEventSvc(IEventBus eventBus,
            BioContext bioContext,
            IntEventLogContext eventLogContext,
            Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory,
            ILogger<BioIntegrationEventSvc> logger, ICorrelationContextAccessor correlationContext)
        {
            _greetaBioContext = bioContext ?? throw new ArgumentNullException(nameof(bioContext));
            _eventLogContext = eventLogContext ?? throw new ArgumentNullException(nameof(eventLogContext));
            _integrationEventLogServiceFactory = integrationEventLogServiceFactory ?? throw new ArgumentNullException(nameof(integrationEventLogServiceFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _eventLogService = _integrationEventLogServiceFactory(_greetaBioContext.Database.GetDbConnection());
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _correlationContext = correlationContext;
        }

        public async Task PublishEventsThroughEventBusAsync(Guid transactionId)
        {
            var pendingLogEvents = await _eventLogService.RetrieveEventLogsPendingToPublishAsync(transactionId);

            foreach (var logEvt in pendingLogEvents)
            {
                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", logEvt.EventId, Program.AppName, logEvt.IntegrationEvent);

                try
                {
                    await _eventLogService.MarkEventAsInProgressAsync(logEvt.EventId);
                    _eventBus.Publish(logEvt.IntegrationEvent);
                    await _eventLogService.MarkEventAsPublishedAsync(logEvt.EventId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "ERROR publishing integration event: {IntegrationEventId} from {AppName}", logEvt.EventId, Program.AppName);

                    await _eventLogService.MarkEventAsFailedAsync(logEvt.EventId);
                }
            }
        }

        public async Task AddAndSaveEventAsync(IntEvent evt)
        {
            _logger.LogInformation("----- Enqueuing integration event {IntegrationEventId} to repository ({@IntegrationEvent})", evt.Id, evt);

            await _eventLogService.SaveEventAsync(evt, _greetaBioContext.GetCurrentTransaction());
        }

        private void SetCorrelationDetails(IntEvent evt)
        {
            
            var correlationId = _correlationContext?.CorrelationContext?.CorrelationId;
            var correlationHeader = _correlationContext?.CorrelationContext?.Header;


            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlationHeader = string.IsNullOrEmpty(correlationHeader) ? "RequestId" : correlationHeader;
            evt.SetCorrelationDetails(correlationId, correlationHeader);
        }


    }
}
