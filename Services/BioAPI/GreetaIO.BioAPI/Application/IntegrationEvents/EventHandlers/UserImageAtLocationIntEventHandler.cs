﻿using System;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using GreetaIO.BioAPI.Application.IntegrationEvents.Events;
using GreetaIO.BioAPI.Infrastructure.Extensions;
using GreetaIO.BioAPI.Infrastructure.TokenContext;
using MediatR;
using Microsoft.Extensions.Logging;
using Serilog.Context;

namespace GreetaIO.BioAPI.Application.IntegrationEvents.EventHandlers
{
    public class UserImageAtLocationIntEventHandler : IIntegrationEventHandler<UserImageAtLocationIntEvent>
    {
        //public ITokenContextService TokenContextService { get; }
        //public ICorrelationContextAccessor CorrelationContextAccessor { get; }
        //private readonly IMediator _mediator;
        private readonly ILogger<UserImageAtLocationIntEventHandler> _logger;
        //private readonly ICorrelationContextAccessor _correlationContext;
        //private readonly ITokenContextService _tokenContext;

        public UserImageAtLocationIntEventHandler(
            //IMediator mediator,
            ILogger<UserImageAtLocationIntEventHandler> logger
            //ITokenContextService tokenContextService,
            //ICorrelationContextAccessor correlationContextAccessor
            )
        {
            
            _logger = logger;
        }


        public Task Handle(UserImageAtLocationIntEvent @event)
        {
            using (LogContext.PushProperty("BioIntegrationEventContext", $"{@event.GenerateLoggingMetaData()["request-id"]}-{Program.AppName}"))
            {
                _logger.LogInformation("-----BIO Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                
                return Task.CompletedTask;
            }
        }

       
    }
}
