﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventBusAbstract.Events;
using Newtonsoft.Json;

namespace GreetaIO.BioAPI.Application.IntegrationEvents.Events
{
    public class UserImageSearchCompletedIntEvent : IntEvent
    {

        public int UserId { get; set; }

        public int TimeId { get; set; }


        [JsonConstructor]
        public UserImageSearchCompletedIntEvent(int userId, int timeId, string correlationId, string clientName, string authToken)
        {
            UserId = userId;
            TimeId = timeId;
            SetCorrelationDetails(correlationId,clientName);
            //SetAuthToken(authToken);
        }
    }
}
