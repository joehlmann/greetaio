﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventBusAbstract.Events;

namespace GreetaIO.BioAPI.Application.IntegrationEvents.Events
{
    
        public class ImageAddedToUserIntEvent : IntEvent
        {
        

            public int UserId { get; private set; }

            public string ImageData { get; private set; }

            public int LibraryId { get; private set; }

            public ImageAddedToUserIntEvent(int userId,int libraryId, string imageData)
            {

                UserId = userId;
                LibraryId = libraryId;
                ImageData = imageData;
            }

            public override string ToString()
            {
                return $"EventId:{Id} for ImageAdded to UserId:{UserId} @ LibraryId:{LibraryId} ";
            }


        
        }
}
