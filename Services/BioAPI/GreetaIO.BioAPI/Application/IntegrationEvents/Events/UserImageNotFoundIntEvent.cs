﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventBusAbstract.Events;
using Newtonsoft.Json;

namespace GreetaIO.BioAPI.Application.IntegrationEvents.Events
{
    public class UserImageNotFoundIntEvent : IntEvent
    {
        public int LibraryId { get; private set; }

        public int LocationId { get; private set; }

        public string ImageData { get; private set; }

        public override string ToString()
        {
            return $"EventId:{Id} for UnknownImageAtLocation LocationId:{LocationId} Library:{LibraryId} ";
        }



        #region Constructors
        public UserImageNotFoundIntEvent(int libraryId,int locationId, string imageData)
        {
            LibraryId = libraryId;
            LocationId = locationId;
            ImageData = imageData;
        }


        [JsonConstructor]
        public UserImageNotFoundIntEvent( int libraryId, int locationId, string imageData, string correlationId, string correlationName, string authToken)
        {
            LibraryId = libraryId;
            LocationId = locationId;
            ImageData = imageData;
            SetCorrelationDetails(correlationId, correlationName);
            SetAuthToken(authToken);
        }
        #endregion
    }
}
