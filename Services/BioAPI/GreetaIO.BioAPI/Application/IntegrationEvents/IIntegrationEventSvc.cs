﻿using System;
using System.Threading.Tasks;
using EventBusAbstract.Events;

namespace GreetaIO.BioAPI.Application.IntegrationEvents
{
    public interface IIntegrationEventSvc
    {
        Task PublishEventsThroughEventBusAsync(Guid transactionId);
        Task AddAndSaveEventAsync(IntEvent evt);
    }
}