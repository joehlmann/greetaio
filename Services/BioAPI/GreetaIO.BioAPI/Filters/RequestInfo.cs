﻿using Microsoft.AspNetCore.Http;

namespace GreetaIO.BioAPI.Filters
{
    public static class RequestInfo
    {
        public static string GetRequestErrorMessage(HttpRequest request)
        {
            return $"Error when requesting {request.Method} {request.Path}";
        }
    }
}
