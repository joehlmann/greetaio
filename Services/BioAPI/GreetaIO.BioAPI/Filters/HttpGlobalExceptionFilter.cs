﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation;
using GreetaIO.BioAPI.Infrastructure;
using GreetaIO.BioAPI.Infrastructure.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace GreetaIO.BioAPI.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        private readonly IHostingEnvironment _env;
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;


        public HttpGlobalExceptionFilter(IHostingEnvironment env, ILogger<HttpGlobalExceptionFilter> logger)
        {
            _logger = logger;
            _env = env;
        }



        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            //Todo Enumerate Errors

            var json = new JsonErrorResponse
            {
                Messages = new[] { "An error ocurred." }
            };

            if (_env.IsDevelopment())
            {
                var errors = new List<string>();
                var validationErrors = ((ValidationException) context.Exception.InnerException)?.Errors
                                       .Select(v => v.ErrorMessage).ToArray() ?? Array.Empty<string>();


                if (context.Exception is GreetaDomainException)
                {
                    errors.Add(((GreetaDomainException) context.Exception)?.Message);

                }

                json.DeveloperMessage = context.Exception;
                json.Messages = validationErrors.Concat(errors).ToArray();

            }

            context.Result = new InternalServerErrorResult(json);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            context.ExceptionHandled = true;
        }



        private class JsonErrorResponse
        {
            public string[] Messages { get; set; }

            public object DeveloperMessage { get; set; }
        }

    }
}
