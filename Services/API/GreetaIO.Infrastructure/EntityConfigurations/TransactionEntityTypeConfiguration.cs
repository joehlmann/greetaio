﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class TransactionEntityTypeConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            

            builder.ToTable("Transactions");

            builder.HasKey(t => t.Id);

            builder.Property(c => c.Id).HasColumnName("TransactionId").ValueGeneratedOnAdd().UseHiLo("TransactionHiLo");

            builder.HasOne(t => t.Item);

            builder.Property(c => c.TransactionType)
                .HasConversion(
                    v => v.ToString(),
                    v => (TransactionType)Enum.Parse(typeof(TransactionType), v));
                
                

            builder.HasOne(t => t.User);

            builder.HasOne(t => t.Location)
                .WithMany(l => l.Transactions);

            builder.Property(c => c.PointsReward)
                .HasColumnType("int")
                .IsRequired();

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");
        }
    }
}
