﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {

            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            builder.Ignore(e => e.UserImageCount);

            builder.ToTable("Users");

            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).HasColumnName("UserId").ValueGeneratedOnAdd().UseHiLo("UserHiLo");


            builder.Property(u => u.AliasName).HasColumnType("varchar(100)");

            builder.OwnsOne(l => l.Contact , c =>
            {
                c.Property<int>("UserId").UseHiLo("UserHiLo");
            });

            builder.Property(u => u.UserType)
                .HasConversion(
                v => v.ToString(),
                v => (UserType)Enum.Parse(typeof(UserType), v));


            //builder.Property<string>("TargetId").HasField("_id")
            //.UsePropertyAccessMode(PropertyAccessMode.Property)
            //.HasConversion(t =>  t.ToInt(),
            //    db =>  db.ToString());

            //builder.Property(u=> u.UserImages).HasField("_userImages");

            //builder.Metadata
            //    .FindNavigation(nameof(User.UserImages))
            //    .SetPropertyAccessMode(PropertyAccessMode.Field);


            builder.HasMany(u => u.UserImages)
                .WithOne()
                //.HasForeignKey(p => p.UserId)
                .OnDelete(DeleteBehavior.Cascade)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);


            //builder.Property(u=> u.UserImages).HasField("_userImages");

            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");




        }

        }
    }

