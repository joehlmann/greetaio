﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class LibraryEntityTypeConfiguration : IEntityTypeConfiguration<Library>
    {
        public void Configure(EntityTypeBuilder<Library> builder)
        {

            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            //builder.Ignore(e => e.Users);

            builder.ToTable("Libraries");

            builder.HasKey(l => l.Id);

            builder.Property(u => u.Id).HasColumnName("LibraryId").ValueGeneratedOnAdd().UseHiLo("LibraryHiLo");

            

            builder.Property(l => l.Name).HasColumnType("varchar(100)");

            builder.Property(l => l.Description).HasColumnType("varchar(250)");

            //Type
            builder.Property(l => l.LibraryType).HasConversion(
                v => v.ToString(),
                v => (LibraryType)Enum.Parse(typeof(LibraryType), v));


            // Users

            builder.HasMany(u => u.LibraryUsers)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);

            // UserImages

            builder.HasMany(u => u.UserImages)
                .WithOne()
                .OnDelete(DeleteBehavior.NoAction)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);

            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");

        }
    }
}
