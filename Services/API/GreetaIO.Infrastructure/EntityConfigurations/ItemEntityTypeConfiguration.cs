﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class ItemEntityTypeConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {

            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            
            

            builder.ToTable("Items");

            // Domain methods
            //builder.Ignore(e => e.GetPoints());

            //Start

            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).HasColumnName("ItemId").ValueGeneratedOnAdd().UseHiLo("ItemHiLo");

            //builder.Property(u => u.ItemNumber).ValueGeneratedOnAdd();

            builder.Property(u => u.ItemName).HasColumnType("varchar(100)");

            builder.Property(u => u.ItemDescription).HasColumnType("varchar(100)");


            // Type
            builder.Property(u => u.ItemType).HasConversion(
                v => v.ToString(),
                v => (ItemType)Enum.Parse(typeof(ItemType), v));

            builder.Property(u => u.IsProduct).HasColumnType("bit");


            //RewardsPoints
            builder.OwnsOne(i => i.RewardPoints, rp =>
            {
                rp.Property<int>("ItemId").UseHiLo("ItemHiLo");
            });

            //builder.Property(u => u.RewardPoints.RewardsType).HasConversion(
            //    v => v.ToString(),
            //    v => (RewardsType)Enum.Parse(typeof(RewardsType), v));


            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");

            

        }
    }

}