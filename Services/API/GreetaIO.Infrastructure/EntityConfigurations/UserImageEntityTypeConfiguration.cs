﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class UserImageEntityTypeConfiguration : IEntityTypeConfiguration<UserImage>
    {
        public void Configure(EntityTypeBuilder<UserImage> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);


            builder.ToTable("UserImages");

            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).HasColumnName("UserImageId").ValueGeneratedOnAdd().UseHiLo("UserImageHiLo");


            builder.Property(u => u.ImageData).HasColumnType("varbinary(max)");

            builder.Property(u => u.SessionId).HasColumnType("bigint");

            builder.Property(u => u.RequestId).HasColumnType("UNIQUEIDENTIFIER");

            builder.Property(e => e.UserImageType)
                .HasConversion(
                    v => v.ToString(),
                    v => (UserImageType)Enum.Parse(typeof(UserImageType), v));

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");


        }
    }
}
