﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class CustomerEntityTypeConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            
            


            builder.ToTable("Customers");

            builder.HasKey(l => l.Id);

            builder.Property(u => u.Id).HasColumnName("CustomerId").ValueGeneratedOnAdd().UseHiLo("CustomerHiLo");

            builder.Property(c => c.CustomerNumber)
                .HasColumnType("int");

            builder.HasMany(c => c.Locations)
                .WithOne(l => l.Customer);


            // Type

            builder.Property(c => c.CustomerType)
                .HasConversion(
                    v => v.ToString(),
                    v => (CustomerType)Enum.Parse(typeof(CustomerType), v));


            //Locations
            builder.HasMany(u => u.Locations)
                .WithOne(l => l.Customer)
                .OnDelete(DeleteBehavior.Cascade)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);


            

            //Contact Value Type
            builder.OwnsOne(c => c.Contact, co =>
            {
                co.Property<int>("CustomerId").UseHiLo("CustomerHiLo");
            });
                

            builder.OwnsOne(l => l.Contact)
                .Property(a => a.Email).HasColumnName("ContactEmail");

            builder.OwnsOne(l => l.Contact)
                .Property(a => a.Phone).HasColumnName("ContactPhone");


            //audit

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())"); 

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");




        }
    }
}
