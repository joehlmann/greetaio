﻿using System;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GreetaIO.Infrastructure.EntityConfigurations
{
    public class LocationEntityTypeConfiguration : IEntityTypeConfiguration<Location>
    {
        

        public void Configure(EntityTypeBuilder<Location> builder)
        {

            builder.Ignore(e => e.DomainEvents);
            builder.Ignore(e => e.Errors);
            builder.Ignore(e => e.HasErrors);
            

            builder.ToTable("Locations");
            

            builder.HasKey(l => l.Id);

            builder.Property(u => u.Id).HasColumnName("LocationId").ValueGeneratedOnAdd().UseHiLo("LocationHiLo");

            //builder.Property(l => l.LocationNumber).ValueGeneratedOnAdd();

            builder.HasOne(l => l.Customer)
                .WithMany(e => e.Locations);


            //builder.Property<int>("LibraryId");

            //Address value Type
            builder.OwnsOne(l => l.Address, a =>
            {
                a.Property<int>("LocationId").UseHiLo("LocationHiLo");
            });

            builder.OwnsOne(l => l.Address)
                .Property(a => a.Address1).HasColumnName("Address1");

            builder.OwnsOne(l => l.Address)
                .Property(a => a.Address2).HasColumnName("Address2");

            builder.OwnsOne(l => l.Address)
                .Property(a => a.PostCode).HasColumnName("PostCode").HasColumnType("varchar(50)");

            builder.OwnsOne(l => l.Address)
                .Property(a => a.State).HasColumnName("State");

            builder.OwnsOne(l => l.Address)
                .Property(a => a.Suburb).HasColumnName("Suburb");


            //Type

           builder.Property(l=>l.LocationType)
               .HasConversion(
                v => v.ToString(),
                v => (LocationType)Enum.Parse(typeof(LocationType), v));


            // contact
            builder.OwnsMany(l => l.Contacts, a =>
            {
                a.WithOwner().HasForeignKey("LocId");
                a.Property<int>("LocationContactId");
                a.HasKey("LocationContactId");
                a.ToTable("LocationContacts");
            });

            // transactions 

            builder.HasMany(u => u.Transactions)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);

            // items

            builder.HasMany(u => u.Items)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade)
                .Metadata.PrincipalToDependent.SetPropertyAccessMode(PropertyAccessMode.Field);

            

            builder.Property(e => e.CreateDate).HasColumnType("datetime").ValueGeneratedOnAdd()
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.LastModified).HasColumnType("datetime").ValueGeneratedOnUpdate()
                .HasDefaultValueSql("(getdate())");



        }
    }
}
