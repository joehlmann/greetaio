﻿namespace GreetaIO.Infrastructure.Infrastructure.DomainServices
{
    public interface IVrSettingsConfig
    {
        string AuthName { get; set; }

        int NumMatches { get; set; }

        float MatchThreshold { get; set; }
    }
}