﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;

namespace GreetaIO.Infrastructure.Infrastructure.DomainServices
{
    public interface IVrServices
    {
        Task<User> MatchImage(Library library, UserImage userImage);

        Task<User> EnrollUser(Library library, User user);
    }
}