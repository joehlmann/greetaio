﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GreetaIO.BioClient.Interfaces;
using GreetaIO.BioClient.Models;
using GreetaIO.BioClient.Utilities;
using GreetaIO.Infrastructure.HttpClients;
using GreetaIO.Infrastructure.TokenContext;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Infrastructure;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly.CircuitBreaker;

namespace GreetaIO.Infrastructure.Infrastructure.DomainServices
{
    public class VrDomainService : IVrServices
    {
        private readonly IBioClient _bioClient;
        private readonly IVrSettingsConfig _vrSettings;
        private readonly ILogger<VrDomainService> _logger;
        private readonly ICrudRepository<UserImage, Guid> _repository;
        private readonly IServicesHttpClient _httpClient;

        public VrDomainService(IBioClient bioClient, IVrSettingsConfig vrSettings,
            ICrudRepository<UserImage, Guid> repository, IServicesHttpClient httpClient)
        {
            _bioClient = bioClient;
            _vrSettings = vrSettings;
            _repository = repository;
            _httpClient = httpClient;

        }

        public async Task<User> MatchImage(Library library, UserImage userImage)
        {

            _logger.LogInformation(
                $"----- MatchImage Request - Library: {library.Id.ToString()} ImageLength:{userImage.ImageData.Length}");

            var result = await _bioClient.MatchImg(
                new MatchDto(library.GalleryId, userImage.ImageData.ByteToImage(),
                    _vrSettings.AuthName,
                    _vrSettings.MatchThreshold,
                    _vrSettings.NumMatches));



            var resultMatches = result.Matches
                .Select(m => new {m.caseID, m.score})
                .OrderBy(m => m.score)
                .DefaultIfEmpty()
                .First();

            var userResult = library.Users
                .Where(u => u.TargetId.ToString() == resultMatches.caseID)
                .DefaultIfEmpty(User.Empty())
                .First();

            _logger.LogInformation(
                $"----- MatchImage Result - Library: {library} UserImage: {userImage} Result {userResult}");
            return userResult;


        }

        public async Task<User> EnrollUser(Library library, User user)
        {
            var enrollDto = new FirDto(user.TargetId.ToString(), _vrSettings.AuthName, user.UserImages.Select(u => u.ImageData));


            await _bioClient.TargetIdCreate(user.TargetId.ToString(),
                new Dictionary<string, string> {{"Gallery", $"{library.GalleryId}"}});

            var result = await _bioClient.FirCreate(enrollDto);

            if (!result.Success)
            {
                user.AddError(VrServiceError.EnrollError);
            }

            return user;


        }

        public HttpRequestMessage CreateRequest(string uri, HttpMethod method, ITokenContextService tokenContextService)
        {
            var request = new HttpRequestMessage(method, uri);
            var accessToken = tokenContextService.GetAccessToken();
            if (accessToken.IsPresent())
            {
                request.Properties.Add("access_token", accessToken);
            }

            return request;
        }


        public async Task<User> EnrollUserAsync(FirDto enrollDto)
        {

            try
            {
                //var s = GenerateSuburbsJsonQueryString(name, postcode);
                var request = _httpClient.CreateRequest($"", HttpMethod.Post);
                var result = await _httpClient.SendAsync(request);

                if (result.IsSuccessStatusCode)
                {
                    var user =
                        JsonConvert.DeserializeObject<User>(
                            await result.Content.ReadAsStringAsync());

                    return user;
                }

                return User.Empty();
            }
            catch (BrokenCircuitException e)
            {
                HandleBrokenCircuitException(e);
                throw;
            }
        }


        private void HandleBrokenCircuitException(BrokenCircuitException e)
        {
            _logger.LogError(e, $"{typeof(ServicesHttpClient)} Circuit breaker tripped");
        }


        public static class VrServiceError
        {
            public static Error EnrollError { get; }

            static VrServiceError()
            {
                EnrollError = new Error()
                {
                    Code = "1.1.1",
                    Id = Guid.Empty,
                    Message = "Enrollment Error"
                };
            }
        }
    }
}

        
    
