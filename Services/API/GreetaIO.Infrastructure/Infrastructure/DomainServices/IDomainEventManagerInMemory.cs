﻿using System;

namespace GreetaIO.Infrastructure.Infrastructure.DomainServices
{
    public interface IDomainEventManagerInMemory
    {
        bool PublishDomainEvent(Guid domainEventId);

        void ClearDomainEvent();
    }
}