﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace GreetaIO.Infrastructure.Infrastructure.DomainServices
{
    public class DomainEventManagerInMemory : IDomainEventManagerInMemory
    {

        private readonly List<Guid> _domainEventIds;
        private readonly ILogger<DomainEventManagerInMemory> _logger;


        public DomainEventManagerInMemory(ILogger<DomainEventManagerInMemory> logger)
        {
            _logger = logger;
            _domainEventIds = new List<Guid>();
        }


        public bool PublishDomainEvent(Guid domainEventId)
        {
            if (EventHasBeenAlreadyPublished(domainEventId))
            {
                _logger.LogWarning($"Domain EventId:{domainEventId} has already Been Published");
                return false;
            } 
            
            _domainEventIds.Add(domainEventId);
            return true;
           
        }

        public void ClearDomainEvent()
        {
            _domainEventIds.Clear();
        }


        private bool EventHasBeenAlreadyPublished(Guid domainEventId)
        {
            return _domainEventIds.Contains(domainEventId);
        }

    }
}
