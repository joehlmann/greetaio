﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility.Helper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace GreetaIO.Infrastructure
{
    public  class ApiContextSeed
    {
        public static async Task SeedAsync(ApiContext context)
        {


            using (context)
            {

                //context.Database.EnsureDeleted();
                //context.SaveChanges();

                //context.Database.Migrate();
                //context.Database.EnsureCreated();



                

                if (!context.Customers.Any())

                {
                   
                        context.Customers.AddRange(GetPreconfiguredCustomers());

                        context.SaveChanges();


                }

                if (!context.Users.Any())
                {

                    
                    context.Users.Add(new User(
                        "fred",
                        Contact.Create("fred","12345678","1@1.com"),
                        (int) UserType.New, 0));

                    
                    context.Users.Add(new User(
                        "mia",
                        Contact.Create("miapia", "8910111213", "mia@greeta.com"),
                        (int)UserType.New, 0));



                    await context.SaveChangesAsync();
                }

                if (!context.Locations.Any())
                {
                    var customer = await context.Customers.FirstAsync();
                    await context.Locations.AddRangeAsync(GetPreconfiguredLocations(customer));

                    await context.SaveChangesAsync();
                }

                if (!context.Libraries.Any())
                {
                    var location = await context.Locations.FirstAsync();
                    var user = await context.Users.FirstAsync();
                    await context.Libraries.AddRangeAsync(GetPreconfiguredLibrary(user,location));

                    await context.SaveChangesAsync();
                }

                if (!context.Items.Any())
                {

                    var item = Item.Create(EntityTypeRange.Item.ToInt(), "Patronage", "A shop Visit", ItemType.Visit,
                        RewardPoints.Create(RewardsType.None, 0, 0), false);



                    context.Items.Attach(item);

                    await context.SaveChangesAsync();
                }


               


                

                if (!context.UserImages.Any())
                {
                    var user1 = context.Users.FirstOrDefault(u => u.Id == 1000);
                    var user2 = context.Users.FirstOrDefault(u => u.Id != 1000);

                    var userImage1 = UserImage.Create( File.ReadAllBytes("Application/Resources/cat1.png"),DateTime.Now.Ticks , "Application/Resources/cat1.png");

                    var userImage2 = UserImage.Create(File.ReadAllBytes("Application/Resources/cat2.png"), DateTime.Now.Ticks, "Application/Resources/cat2.png");

                    user1.AddUserImage(userImage1);

                    user2.AddUserImage(userImage2);

                 

                    await context.SaveChangesAsync();
                }

                if (!context.Transactions.Any())
                {
                    var location = await context.Locations.FirstAsync();
                    var user = await context.Users.FirstAsync();
                    var item = await context.Items.FirstOrDefaultAsync();
                    await context.Transactions.AddRangeAsync(GetPreconfiguredTransaction(location, user, item));

                    await context.SaveChangesAsync();
                }

            }
        }

        private static IEnumerable<Transaction> GetPreconfiguredTransaction(Location location, User user, Item item)
        {


            var result = new List<Transaction>
            {
                new Transaction()
                {
                    Id = EntityTypeRange.Transaction.ToInt(),
                    Item = item,
                    TransactionType = TransactionType.Unknown,
                    User = user,
                    Location = location,
                    PointsReward = item.GetPoints(),
                    CreateDate = DateTime.Parse("30Nov2019"),
                    LastModified = DateTime.Parse("30Nov2019"),
                    CurrentUserId = "Me101"

                }
            };
            
            return result;
        }


        

        private static IEnumerable<Library> GetPreconfiguredLibrary( User user,Location location)
        {


            var result =  new List<Library>
            {
                Library.Create(EntityTypeRange.Library.ToInt(),
                    "My Library","MyLib desc",LibraryType.Default,location)
                
            };

            result[0].AddLibUser(user);
                
            
            return result;
        }

        private static IEnumerable<Customer> GetPreconfiguredCustomers()
        {
            var result = new Customer(
                EntityTypeRange.Customer.ToInt(),
                "MyCompany",
                "Vic", "Brighton", "508 hawthorn Rd", "", "3162",
                "Bill Bob", "0416544517", "bob@bobs.com.au");

            result.LastModified = DateTime.Parse("30Nov2019");
            result.CreateDate = DateTime.Parse("30Nov2019");
            result.CurrentUserId = "Me101";
            result.CustomerType = CustomerType.Default;
            return new List<Customer>
            {
                result
            };
        }


        private static IEnumerable<Location> GetPreconfiguredLocations(Customer customer)
        {

            var result = Location.Create(EntityTypeRange.Location.ToInt(),
                "Axl Roasters",
                            LocationType.Default,
                            customer,
                            Address.Create("vic", "caulfield", "508 hawthorn Rd", "", "3162"),
                            Contact.Create("Mrs Bob", "0416544517", "mrs@bobs.com.au")
                            );

            result.LastModified = DateTime.Parse("30Nov2019");
            result.CreateDate = DateTime.Parse("30Nov2019");
            result.CurrentUserId = "Me101";
            result.LocationType = LocationType.Default;

            return new List<Location>
            {
                result
            };
        }


    }
}
