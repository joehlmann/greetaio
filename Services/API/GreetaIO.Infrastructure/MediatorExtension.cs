﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GreetaIO.Utility;
using MediatR;

namespace GreetaIO.Infrastructure
{
    public static class MediatorExtension
    {

        public static async Task DispatchDomainEventsAsync(this IMediator mediator, ApiContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<Entity<int>>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            
            try
            {
                foreach (var domainEvent in domainEvents)
                    await mediator.Publish(domainEvent);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

           
        }

    }
}
