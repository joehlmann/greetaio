﻿namespace GreetaIO.Infrastructure.TokenContext
{
    public interface ITokenContextService
    {

        void SetAccessToken(string token);
        string GetAccessToken();

    }
}
