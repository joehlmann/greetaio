﻿using System.Net.Http;
using System.Threading.Tasks;
using GreetaIO.Infrastructure.TokenContext;
using GreetaIO.Utility.Infrastructure;

namespace GreetaIO.Infrastructure.HttpClients
{
    public class ServicesHttpClient : IServicesHttpClient
    {
        
        private readonly ITokenContextService _tokenService;
        private HttpClient _httpClient;

        public ServicesHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public ServicesHttpClient(HttpClient client, ITokenContextService tokenService)
        {
            _httpClient = client;
            _tokenService = tokenService;
        }

        

        public HttpRequestMessage CreateRequest(string uri, HttpMethod method)
        {
            var request = new HttpRequestMessage(method, uri);
            var accessToken = _tokenService.GetAccessToken();
            if (accessToken.IsPresent())
            {
                request.Properties.Add("access_token", accessToken);
            }

            return request;
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            var result = await _httpClient.SendAsync(request);

            return result;
        }
    }
}
