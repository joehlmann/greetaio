﻿using System.Net.Http;
using System.Threading.Tasks;

namespace GreetaIO.Infrastructure.HttpClients
{
    public interface IServicesHttpClient
    {
        
        HttpRequestMessage CreateRequest(string uri, HttpMethod method);
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
}