﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EFSecondLevelCache.Core;
using EFSecondLevelCache.Core.Contracts;
using GreetaIO.Infrastructure.EntityConfigurations;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;

namespace GreetaIO.Infrastructure
{
    public class ApiContext : DbContext , IUnitOfWork
    {
        private int _greetaNumberBatchSize = 5;

        private IEFCacheServiceProvider _provider;


        public DbSet<Customer> Customers { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Library> Libraries { get; set; }

        //public DbSet<LibraryUser> LibraryUsers { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<UserImage> UserImages { get; set; }

        public DbSet<ApiError> Errors { get; set; }

        public DbSet<ClientRequest> ClientRequests { get; set; }


        //Transactions

        private readonly IMediator _mediator;
        private readonly IEFCacheServiceProvider _cacheServiceProvider;
        private IDbContextTransaction _currentTransaction;


        public IDbContextTransaction GetCurrentTransaction() => _currentTransaction;

        public bool HasActiveTransaction => _currentTransaction != null;


        public ApiContext(DbContextOptions<ApiContext> options, IMediator mediator, IEFCacheServiceProvider cacheServiceProvider) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _cacheServiceProvider = cacheServiceProvider;
            
        }

        

        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            if (_currentTransaction != null) return null;

            _currentTransaction = await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            return _currentTransaction;
        }

        public async Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            if (transaction != _currentTransaction)
                throw new InvalidOperationException($"Transaction {transaction.TransactionId} is not current");

            try
            {
                await SaveChangesAsync();
                transaction.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasSequence<int>("UserHiLo")
                .StartsAt(1000).IncrementsBy(_greetaNumberBatchSize);

            builder.HasSequence<int>("CustomerHiLo")
                .StartsAt(2000).IncrementsBy(_greetaNumberBatchSize);

            builder.HasSequence<int>("LibraryHiLo")
                .StartsAt(3000).IncrementsBy(_greetaNumberBatchSize);

            builder.HasSequence<int>("ItemHiLo")
                .StartsAt(4000).IncrementsBy(_greetaNumberBatchSize);

            builder.HasSequence<int>("LocationHiLo")
                .StartsAt(5000).IncrementsBy(_greetaNumberBatchSize);

            builder.HasSequence<int>("TransactionHiLo")
                .StartsAt(8000).IncrementsBy(_greetaNumberBatchSize);


            builder.HasSequence<int>("UserImageHiLo")
                .StartsAt(9000).IncrementsBy(_greetaNumberBatchSize);

            //builder.ForSqlServerUseSequenceHiLo("GreetaNumberSequenceHiLo");


            builder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            builder.ApplyConfiguration(new ItemEntityTypeConfiguration());
            builder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            builder.ApplyConfiguration(new LocationEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserImageEntityTypeConfiguration());
            builder.ApplyConfiguration(new LibraryEntityTypeConfiguration());
            //builder.ApplyConfiguration(new LibraryUserEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransactionEntityTypeConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // Dispatch Domain Events collection. 
            // Choices:
            // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
            // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime
            // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
            // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await SaveChangesAsync(cancellationToken);

            return true;
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var entities = (from entry in ChangeTracker.Entries()
                where entry.State == EntityState.Modified || entry.State == EntityState.Added
                select entry.Entity);

            
            //cache
            var changedEntityNames = this.GetChangedEntityNames();

            try
            {
                ChangeTracker.AutoDetectChangesEnabled = false;

                var result = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
                ChangeTracker.AutoDetectChangesEnabled = true;


                
                _cacheServiceProvider.InvalidateCacheDependencies(changedEntityNames);

                

                return result;
            }
            catch (DBConcurrencyException ex)
            {
                throw ex;
            }


            //var validationResults = new List<ValidationResult>();

            //foreach (var entity in entities)
            //{
            //    if (!Validator.TryValidateObject(entity, new ValidationContext(entity), validationResults))
            //    {
            //        throw new ValidationException();
            //    }
            //}
        }
    }

    public class ApiContextDesignFactory : IDesignTimeDbContextFactory<ApiContext>
    {
        public ApiContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApiContext>()
                .UseSqlServer(
                    "Server=robin,1433;Initial Catalog=ApiDb;User Id=justin;Password=hst;");

            return new ApiContext(optionsBuilder.Options, new NoMediator(),null);
        }

        class NoMediator : IMediator
        {
            public Task Publish<TNotification>(TNotification @event,
                CancellationToken cancellationToken = default(CancellationToken)) where TNotification : INotification
            {
                return Task.CompletedTask;
            }

            public Task Publish(object @event, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            public Task<TResponse> Send<TResponse>(IRequest<TResponse> request,
                CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.FromResult<TResponse>(default(TResponse));
            }

            public Task Send(IRequest request, CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.CompletedTask;
            }

            public Task<object> Send(object request, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }
        }
    }
}
    
