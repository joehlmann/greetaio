﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public  class ItemRepository : ICrudRepository<Item,int>
   {

        private readonly ApiContext _context;


        public ItemRepository(ApiContext context)
        {
            _context = context;
            
        }


        public IUnitOfWork UnitOfWork => _context;

        public Item Add(Item entity)
        {
            if (entity.IsTransient())
            {
                return _context.Items
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public Item Update(Item entity)
        {
            return _context.Items
                .Update(entity)
                .Entity;
        }
        

        public async Task<Item> GetAsync(int id)
        {
            var result = await _context.Items.FindAsync(id) ?? Item.Empty();
            if (!result.Equals(Item.Empty()))
            {
                return result;
            }

            return result;
        }

        
    }
}
