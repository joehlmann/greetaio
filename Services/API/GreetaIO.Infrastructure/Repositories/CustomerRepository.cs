﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public  class CustomerRepository : ICrudRepository<Customer,int>
   {

        private readonly ApiContext _context;


        public CustomerRepository(ApiContext context)
        {
            _context = context;
        }


        public IUnitOfWork UnitOfWork => _context;

        public Customer Add(Customer entity)
        {
            if (entity.IsTransient())
            {
                return _context.Customers
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public Customer Update(Customer entity)
        {
            return _context.Customers
                .Update(entity)
                .Entity;
        }
        

        public async Task<Customer> GetAsync(int id)
        {
            var result = await _context.Customers.FindAsync(id) ?? Customer.Empty() ;
            if (!result.Equals(Customer.Empty()))
            {
                await _context.Entry(result)
                    .Collection(u => u.Locations).LoadAsync();

            }

            return result;
        }

        
    }
}
