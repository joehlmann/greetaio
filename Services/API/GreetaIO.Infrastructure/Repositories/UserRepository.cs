﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public  class UserRepository : ICrudRepository<User,int>
   {

        private readonly ApiContext _context;


        public UserRepository(ApiContext context)
        {
            _context = context;
        }


        public IUnitOfWork UnitOfWork => _context;
            

        public User Add(User entity)
        {
            if (entity.IsTransient())
            {
                return _context.Users
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public User Update(User entity)
        {
            return _context.Users
                .Update(entity)
                .Entity;
        }
        

        public async Task<User> GetAsync(int id)
        {
            var result = await _context.Users.FindAsync(id) ?? User.Empty();
            if (!result.Equals(User.Empty()))
            {
                await _context.Entry(result)
                    .Collection(u => u.UserImages).LoadAsync();
            }                

            return result;
        }

        
    }
}
