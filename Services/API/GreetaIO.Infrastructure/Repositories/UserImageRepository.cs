﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public class UserImageRepository : ICrudRepository<UserImage,int>
    {
        private readonly ApiContext _context;
        public IUnitOfWork UnitOfWork => _context;



        public UserImageRepository(ApiContext context)
        {
            _context = context;
        }

        public UserImage Add(UserImage entity)
        {
            if (entity.IsTransient())
            {
                return _context.UserImages
                    .Add(entity)
                    .Entity;
            }

            return entity;
        }

        public UserImage Update(UserImage entity)
        {
            return _context.UserImages
                .Update(entity)
                .Entity;
        }

        public async Task<UserImage> GetAsync(int id)
        {
            var result = await _context.UserImages.FindAsync(id) ?? UserImage.Empty();
            
            return result;
        }
    }
}
