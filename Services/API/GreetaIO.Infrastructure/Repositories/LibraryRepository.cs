﻿using System;
using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public class LibraryRepository : ICrudRepository<Library,int>
    {
        private readonly ApiContext _context;


        public LibraryRepository(ApiContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public Library Add(Library entity)
        {
            if (entity.IsTransient())
            {
                return _context.Libraries
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public Library Update(Library entity)
        {
            return _context.Libraries
                .Update(entity)
                .Entity;
        }


        public async Task<Library> GetAsync(int id)
        {

            var result = await _context.Libraries.FindAsync(id) ?? Library.Empty();

            try
            {
                if (!result.Equals(Library.Empty()))
                {
                    await _context.Entry(result)
                        .Collection(u => u.LibraryUsers)
                        .LoadAsync();

                    //await _context.Entry(result)
                    //    .Collection(u => u.Users).LoadAsync();


                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
//                throw;
            }

  
            

            return result;
        }
    }
}
