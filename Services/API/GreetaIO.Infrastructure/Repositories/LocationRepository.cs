﻿using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public class LocationRepository : ICrudRepository<Location,int>
    {
        private readonly ApiContext _context;


        public LocationRepository(ApiContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public Location Add(Location entity)
        {
            if (entity.IsTransient())
            {
                return _context.Locations
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public Location Update(Location entity)
        {
            return _context.Locations
                .Update(entity)
                .Entity;
        }


        public async Task<Location> GetAsync(int id)
        {
            var result = await _context.Locations.FindAsync(id) ?? Location.Empty();


            //var result = await _context.Locations
            //                 .Include(l => l.Customer)
            //                 .Include(l => l.Contacts)
            //                 .Include(l => l.Items)
            //                 .Include(l => l.Transactions).FirstOrDefaultAsync() ?? Location.Empty();





            if (!result.Equals(Location.Empty()))
            {
                await _context.Entry(result)
                    .Reference(l => l.Customer).LoadAsync();

                await _context.Entry(result)
                    .Collection(u => u.Contacts).LoadAsync();

                await _context.Entry(result)
                    .Collection(u => u.Items).LoadAsync();

                await _context.Entry(result)
                    .Collection(u => u.Transactions).LoadAsync();


            }

            return result;
        }
    }
}
