﻿using System;
using System.Threading.Tasks;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.Infrastructure.Repositories
{
    public  class TransactionRepository : ICrudRepository<Transaction,int>
   {

        private readonly ApiContext _context;


        public TransactionRepository(ApiContext context)
        {
            _context = context;
        }


        public IUnitOfWork UnitOfWork => _context;

        public Transaction Add(Transaction entity)
        {
            if (entity.IsTransient())
            {
                return _context.Transactions
                    .Add(entity)
                    .Entity;
            }
            else
            {
                return entity;
            }
        }

        public Transaction Update(Transaction entity)
        {
            return _context.Transactions
                .Update(entity)
                .Entity;
        }
        

        public async Task<Transaction> GetAsync(int id)
        {
            try
            {
                var result = await _context.Transactions.FindAsync(id) ?? Transaction.Empty();


                if (!result.Equals(Transaction.Empty()))
                {
                    await _context.Entry(result)
                        .Reference(l => l.User).LoadAsync();


                    //await _context.Entry(result)
                    //    .Collection(u => u.Users).LoadAsync();


                }


                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            
            

            
        }

        
    }
}
