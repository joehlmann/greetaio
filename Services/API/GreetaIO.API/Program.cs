﻿using System;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace GreetaIO.API
{

    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;

        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);


        public static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += AppUnhandledException;

            // CreateWebHostBuilder(args).Build().Run();

            var configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);


            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = CreateHostBuilder(args)
                    .Build();

                //host.MigrateDbContext<CatalogContext>(context =>
                //{
                //    new CatalogContextSeed()
                //        .SeedAsync(context)
                //        .Wait();
                //});

                Log.Information("Applying migrations ({ApplicationContext})...", AppName);

                host.Run();
                return 0;

            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var res = Host.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.CaptureStartupErrors(false);
                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.UseUrls("https://localhost:5010", "http://localhost:5000");
                    webBuilder.UseStartup<Startup>();

                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .UseConsoleLifetime()
                .UseSerilog();


            return res;
        }






            private static IConfiguration GetConfiguration()
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddEnvironmentVariables();

                var config = builder.Build();

                return builder.Build();
            }
            private static ILogger CreateSerilogLogger(IConfiguration configuration)
            {

                var loggingConfig = configuration.GetSection("Logging");
                var levelSwitch = new LoggingLevelSwitch();
                var seqConnection = loggingConfig["SeqServerUrl"];
                var seqAPIKey = loggingConfig["SeqAPIKey"];
                var logFilePath = loggingConfig["LogFilePath"];
                var idOut = 0;
                Int32.TryParse(loggingConfig["SeqEventBodyLimitBytes"], out idOut);

                //Default event body size
                var eventBodyLimit = idOut > 0 ? idOut : 262144;

                var seqServerUrl = configuration["Logging:SeqServerUrl"];
                //var logstashUrl = configuration["Serilog:LogstashgUrl"];
                return new LoggerConfiguration()
                    .MinimumLevel.Verbose()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                    .MinimumLevel.Override("System", LogEventLevel.Information)
                    .MinimumLevel.Override("Ocelot", LogEventLevel.Information)
                    .Enrich.WithThreadId()
                    .Enrich.WithEnvironmentUserName()
                    .Enrich.WithProperty("ApplicationContext", AppName)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                    .WriteTo.File(logFilePath, rollingInterval: RollingInterval.Day)
                    .WriteTo.Seq(string.IsNullOrWhiteSpace(seqConnection) ? "http://seq:5341" : seqServerUrl,apiKey:seqAPIKey,controlLevelSwitch:levelSwitch,eventBodyLimitBytes:eventBodyLimit)
                    .WriteTo.Http(string.IsNullOrWhiteSpace(logstashUrl) ? "http://logstash:8080" : logstashUrl)
                    .ReadFrom.Configuration(configuration)
                    .CreateBootstrapLogger()
                    .CreateLogger();
            }

            private static void AppUnhandledException(object sender, UnhandledExceptionEventArgs e)
            {
                if (Log.Logger != null && e.ExceptionObject is Exception exception)
                {
                    UnhandledExceptions(exception);

                    // It's not necessary to flush if the application isn't terminating.
                    if (e.IsTerminating)
                    {
                        Log.CloseAndFlush();
                    }
                }
            }

            private static void UnhandledExceptions(Exception e)
            {
                Log.Logger?.Error(e, "Console application crashed");
            }

    }
}

