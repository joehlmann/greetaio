﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Items;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {

        private readonly IMediator _mediator;

        private readonly ILogger<ItemController> _logger;
        private readonly IGreetaQueries _greetaQueries;


        public ItemController(
            IMediator mediator,
            ILogger<ItemController> logger,
            IGreetaQueries greetaQueries
        )
        {
            _mediator = mediator;
            _logger = logger;
         
        }


        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CustomerDTO>>> GetItemsAsync()
        {

            var customers = await _greetaQueries.GetCustomersAsync();

            return Ok(customers);


        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<ItemDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> CreateItemForLocation([FromBody] ItemCreateForLocationCmd cmd)
        {
            _logger.LogInformation(
                "----- API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmd.GetGenericTypeName(),
                nameof(cmd.LocationId),
                cmd.LocationId,
                cmd);

            var result = await _mediator.Send(cmd);

            //if (result.Errors.Any())
            //{
            //    return BadRequest(result.Errors);
            //}


            return Ok(result);


        }

    }
}