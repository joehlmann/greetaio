﻿using System.Net;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Application.CmdQueries.UserImages;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds;
using GreetaIO.API.Cmds.UserImages;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UserImageController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<UserImageController> _logger;
        private readonly IGreetaQueries _greetaQueries;


        public UserImageController(
            IMediator mediator,
            ILogger<UserImageController> logger
        )
        {
            _mediator = mediator;
            _logger = logger;
            
        }


        [HttpGet("user/{userid}/images")]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> UserImagesByUserIdAsync([FromBody] UserImageByIdQuery query)
        {

            _logger.LogInformation(
                "----- Sending query: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                query.GetGenericTypeName(),
                nameof(query.Id),
                query.Id,
                query);

            var result = await _mediator.Send(query);

            if (result.UserImageType == UserImageType.Empty.GetEnumName())
            {
                return BadRequest(ErrorMsg.UserImageDoesNotExist);
            }

            return Ok(result);
        }

        [HttpGet()]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> UserImageByIdAsync([FromBody] UserImageByIdQuery query)
        {

            _logger.LogInformation(
                "----- Sending query: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                query.GetGenericTypeName(),
                nameof(query.Id),
                query.Id,
                query);

            var result = await _mediator.Send(query);

            if (result.UserImageType == UserImageType.Empty.GetEnumName())
            {
                return BadRequest(ErrorMsg.UserImageDoesNotExist);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> UserImageByIdAsync([FromRoute] string id, [FromBody] UserImageByIdQuery query)
        {

            var queryNew = new UserImageByIdQuery(id.ToInt());
            

            _logger.LogInformation(
                "----- Sending query: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                queryNew.GetGenericTypeName(),
                nameof(queryNew.Id),
                queryNew.Id,
                queryNew);

            var result = await _mediator.Send(query);


            if (result.UserImageType == UserImageType.Empty.GetEnumName())
            {
                return BadRequest(ErrorMsg.UserImageDoesNotExist);
            }

            return Ok(result);
        }



        [HttpPost("{userid}")]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserImageDTO>> CreateUserImageAsync([FromHeader(Name = "request-id")] string requestId,[FromRoute] string userid, [FromBody] UserImageCreateCmd cmd)
        {



            var cmdNew = new UserImageCreateCmd(userid, cmd.ImageData);

            _logger.LogInformation(
                "-----API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmdNew.GetGenericTypeName(),
                nameof(cmdNew.UserId),
                cmdNew.UserId,
                cmdNew);

            var requestCreateUserImage = new IdentifiedCommand<UserImageCreateCmd, UserImageDTO>(cmdNew, requestId.ToGuid());




            _logger.LogInformation(
                "----- Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                requestCreateUserImage.GetGenericTypeName(),
                nameof(requestCreateUserImage.Id),
                requestCreateUserImage.Id,
                requestCreateUserImage);


            var result = await _mediator.Send(requestCreateUserImage).ConfigureAwait(false);

            return CreatedAtAction(nameof(UserImageByIdAsync), new { id = result.UserImageId }, result);


        }
        [HttpPost()]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.Created)]
        [ProducesResponseType( (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> CreateUserImageAsync([FromBody] UserImageCreateCmd cmd)
        {


            //var headers = this.Request.Headers;

            var cmdNew = new UserImageCreateCmd(cmd.UserId.ToGuidStr(), cmd.ImageData);

            _logger.LogInformation(
                "----- Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmdNew.GetGenericTypeName(),
                nameof(cmdNew.UserId),
                cmdNew.UserId,
                cmdNew);


           var result = await _mediator.Send(cmdNew).ConfigureAwait(false);

            //if (result.Errors.Any())
            //{
            //    return BadRequest(result.Errors);
            //}

            return CreatedAtAction(nameof(UserImageByIdAsync), new { id = result.UserImageId }, result);
        }
    }
}