﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds;
using GreetaIO.API.Cmds.Libraries;
using GreetaIO.API.Cmds.Locations;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {


        private readonly IMediator _mediator;
        
        private readonly ILogger<LocationController> _logger;

        public LocationController(IMediator mediator, ILogger<LocationController> logger)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> CreateLocationAsync([FromBody] LocationCreateForCustomerCmd cmd)
        {
            _logger.LogInformation(
                "----- API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmd.GetGenericTypeName(),
                nameof(cmd.CustomerId),
                cmd.CustomerId,
                cmd);

            var result = await _mediator.Send(cmd);

            //if (result.Errors.Any())
            //{
            //    return BadRequest(result.Errors);
            //}


            return Ok(result);


        }

        [HttpPost("{locationId:int}/userimage")]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserImageDTO>> FindUserForFUserImageAtLocationAsync([FromHeader(Name = "request-id")] string requestId,[FromRoute] int locationId, [FromBody] LocationUserImageAtLocationCmd cmd )
        {
            UserDTO cmdResult= UserDTO.Empty();

            var newCmd = new LocationUserImageAtLocationCmd(cmd.ImageData,locationId,cmd.TimeId,requestId.ToGuid());

            if (requestId.IsGuid())
            {
                var request = new IdentifiedCommand<LocationUserImageAtLocationCmd, UserDTO>(newCmd, requestId.ToGuid());

                _logger.LogInformation(
                    "----- API Sending command: {CommandName} - {Id}: {TimeId} ({@Command})",
                    request.GetGenericTypeName(),
                    nameof(request.Id),
                    request.Command.TimeId,
                    request);

              cmdResult  =  await _mediator.Send(newCmd).ConfigureAwait(false);
            }

            if (cmdResult.Equals(UserDTO.Empty()))
            {
                return BadRequest();
            }

            return Ok(cmdResult);

            
            

        }


        [HttpPost("{customerId}")]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> CreateLocationAsync([FromRoute] string customerId , [FromBody] LocationCreateForCustomerCmd cmd)
        {

            var cmdNew = LocationCreateForCustomerCmd.CreateNew(customerId, cmd);

            _logger.LogInformation(
                "----- API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmd.GetGenericTypeName(),
                nameof(cmd.CustomerId),
                cmd.CustomerId,
                cmd);

            var result = await _mediator.Send(cmd);

            //if (result.Errors.Any())
            //{
            //    return BadRequest(result.Errors);
            //}


            return Ok(result);


        }

        



    }
}