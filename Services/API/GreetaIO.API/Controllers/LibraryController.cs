﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds;
using GreetaIO.API.Cmds.Libraries;
using GreetaIO.API.Cmds.Locations;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LibraryController : ControllerBase
    {

        private readonly IMediator _mediator;

        private readonly ILogger<LocationController> _logger;

        public LibraryController(IMediator mediator, ILogger<LocationController> logger)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost("{libraryid:int}/userimage")]
        [ProducesResponseType(typeof(UserImageDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<UserImageDTO>> FindUserForCreateUserImageAtLocationAsync([FromHeader(Name = "request-id")] string requestId, [FromRoute] int libraryid, [FromBody] LibraryFindUserForCreateImageCmd cmd)
        {
            UserImageDTO cmdResult = UserImageDTO.Empty();

            var newCmd = new LibraryFindUserForCreateImageCmd(cmd.ImageData, libraryid, cmd.TimeId);

            if (requestId.IsGuid())
            {
                var requestFindUser = new IdentifiedCommand<LibraryFindUserForCreateImageCmd, UserImageDTO>(cmd, requestId.ToGuid());

                _logger.LogInformation(
                    "----- API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                    requestFindUser.GetGenericTypeName(),
                    nameof(requestFindUser.Id),
                    requestFindUser.Command.TimeId,
                    requestFindUser);

                cmdResult = await _mediator.Send(cmd).ConfigureAwait(false);
            }

            if (cmdResult.Equals(UserImageDTO.Empty()))
            {
                return BadRequest();
            }

            return Ok();




        }

    }
}