﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Application.CmdQueries.Customers;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.TheDomain.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        private readonly ILogger<CustomerController> _logger;
        private readonly IGreetaQueries _greetaQueries;


        public CustomerController(
            IMediator mediator,
            ILogger<CustomerController> logger,
            IGreetaQueries greetaQueries
            )
        {
            _mediator = mediator;
            _logger = logger;
            _greetaQueries = greetaQueries;
        }


        // Get /api/customer
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int) HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CustomerDTO>>> GetCustomersAsync()
        {

            var customers = await _greetaQueries.GetCustomersAsync();

            return Ok(customers);


        }

        // Get /api/customer/id
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> GetCustomerByIdAsync([FromRoute] int customerId)
        {

            var result = await _mediator.Send(new CustomerByIdQuery(customerId));

           if (result.Equals(CustomerDTO.Empty()))
           {
                return BadRequest(result);
           }

                
            return Ok(result);


        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<CustomerDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> CreateCustomerAsync([FromBody] CustomerCreateCmd  cmd )
        {
            _logger.LogInformation(
                "-----API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmd.GetGenericTypeName(),
                nameof(cmd.CustomerNumber),
                cmd.CustomerNumber,
                cmd);

            var result = await _mediator.Send(cmd);

            if (result.CustomerType == DtoState.Empty())
            {
                return BadRequest(ErrorMsg.CustomerCreateFailed);
            }


            return Ok(result);


        }



    }
}