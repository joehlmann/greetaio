﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Application.CmdQueries.Users;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Users;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IMediator _mediator;

        private readonly ILogger<UserController> _logger;
        private readonly IGreetaQueries _greetaQueries;


        public UserController(
            IMediator mediator,
            ILogger<UserController> logger
        )
        {
            _mediator = mediator;
            _logger = logger;
        }


        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(UserDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDTO>> GetUserByIdAsync([FromRoute] int id)
        {

            var query = new UserByIdQuery(id);

            _logger.LogInformation(
                "----- API Sending query: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                query.GetGenericTypeName(),
                nameof(query.Id),
                query.Id,
                query);

            var result = await _mediator.Send(query);

           

            return Ok(result);


        }
        [HttpGet("{id:int}/images")]
        [ProducesResponseType(typeof(UserDTO), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserDTO>> GetUserAndImagesByIdAsync([FromRoute] int id)
        {

            var queryNew = new UserWithImagesByIdQuery(id);
            

            _logger.LogInformation(
                "----- API Sending query: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                queryNew.GetGenericTypeName(),
                nameof(queryNew.Id),
                queryNew.Id,
                queryNew);

            var result = await _mediator.Send(queryNew);


            return Ok(result);


        }
        

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<UserDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult<CustomerDTO>> CreateUserAsync([FromBody] UserCreateCmd cmd)
        {
            _logger.LogInformation(
                "----- API Sending command: {CommandName} - {IdProperty}: {CommandId} ({@Command})",
                cmd.GetGenericTypeName(),
                nameof(cmd),
                cmd.LocationId,
                cmd);

            var result = await _mediator.Send(cmd).ConfigureAwait(false);

            

            return Ok(result);


        }

    }
}