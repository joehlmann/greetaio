﻿using Autofac;
using EventBusMsgBrokerSvc;
using GreetaIO.API.Application.ApplicationServices;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Infrastructure.Authorization;
using GreetaIO.API.Infrastructure.MiddleWare;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.EventHandlers;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.Infrastructure.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.Infrastructure.TokenContext;
using IntegrationEventLogEF.Services;
using Microsoft.Data.SqlClient;

namespace GreetaIO.API.Infrastructure.Modules
{
    public class ServicesModule : Module
    {

        protected string _connectionStringEventLog;



        public ServicesModule(string connectionStringEventLog)
        {
            _connectionStringEventLog = connectionStringEventLog;

            
        }

        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();




            builder.RegisterType<CustomerAccountValidService>()
                .As<ICustomerAccountValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CorrelationContextService>()
                .As<ICorrelationContextService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ContactValidService>()
                .As<IContactValidService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<AddressValidService>()
                .As<IAddressValidService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionStringEventLog))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();


            //builder.RegisterType<UserImageAtLocationIntEventHandler>()
            //    .As<UserImageAtLocationIntEventHandler>()
            //    .InstancePerLifetimeScope();


            // RpcReply
            builder.RegisterGeneric(typeof(RpcReplyService<>))
                .As(typeof(IRPCReplyService<>))
                .InstancePerDependency();

            // Add&Save Integration Service
            builder.RegisterType<ApiIntegrationEventSvc>()
                .As<IIntegrationEventSvc>()
                .InstancePerLifetimeScope();

           builder.RegisterType<IdentityService>()
               .As<IIdentityService>()
               .InstancePerLifetimeScope();




            //Singleton
            builder.RegisterType<DomainEventManagerInMemory>()
                .As<IDomainEventManagerInMemory>()
                .SingleInstance();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .SingleInstance();




        }
    }
}
