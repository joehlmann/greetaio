﻿using Autofac;
using EventBusAbstract.Abstractions;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Cmds.Locations;
using GreetaIO.API.IntegrationEvents.EventHandlers;
using GreetaIO.Infrastructure.Repositories;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using System.Reflection;


namespace GreetaIO.API.Infrastructure.Modules
{

    public class ApplicationModule
        :Autofac.Module
    {

        public string QueriesConnectionString { get; }

        private ContainerBuilder _container;

        public ApplicationModule(ContainerBuilder container,string queriesConnectionString)
        {
            QueriesConnectionString = queriesConnectionString;
            _container = container;
        }

        protected override void Load(ContainerBuilder builder)
        {
            

            builder.Register(c => new GreetaQueries(QueriesConnectionString))
                .As<IGreetaQueries>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CustomerRepository>()
                .As<ICrudRepository<Customer,int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<LibraryRepository>()
                .As<ICrudRepository<Library,int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserImageRepository>()
                .As<ICrudRepository<UserImage, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<ICrudRepository<User, int>>()
                .InstancePerLifetimeScope();
            

            builder.RegisterType<LocationRepository>()
                .As<ICrudRepository<Location, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ItemRepository>()
                .As<ICrudRepository<Item, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TransactionRepository>()
                .As<ICrudRepository<Transaction, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(UserImageAtLocationIntEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IIntegrationEventHandler<>));

          
        }
    }
}
