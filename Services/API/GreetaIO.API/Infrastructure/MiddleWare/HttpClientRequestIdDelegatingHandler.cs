﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;

namespace GreetaIO.API.Infrastructure.MiddleWare
{
    public class HttpClientRequestIdDelegatingHandler
        : DelegatingHandler
    {
        private readonly ICorrelationContextAccessor _correlationContext;

        public HttpClientRequestIdDelegatingHandler(ICorrelationContextAccessor correlationContext)
        {
            _correlationContext = correlationContext;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            var correlationId = _correlationContext?.CorrelationContext?.CorrelationId;
            var correlationHeader = _correlationContext?.CorrelationContext?.Header;

            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            if (!string.IsNullOrEmpty(correlationHeader))
            {
                request.Headers.Add(correlationHeader, correlationId);
            }

            return await base.SendAsync(request, cancellationToken);


            //if (request.Method == HttpMethod.Post || request.Method == HttpMethod.Put)
            //{
            //    if (!request.Headers.Contains("x-requestid"))
            //    {
            //        request.Headers.Add("x-requestid", Guid.NewGuid().ToString());
            //    }
            //}

            //return await base.SendAsync(request, cancellationToken);
        }
    }
}
