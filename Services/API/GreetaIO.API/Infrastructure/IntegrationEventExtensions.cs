﻿using System;
using System.Collections.Generic;
using CorrelationId;
using EventBusAbstract.Events;
using GreetaIO.Utility.Infrastructure;


namespace GreetaIO.API.Infrastructure
{
    public static class IntegrationEventExtensions
    {
        public static Dictionary<string, object> GenerateLoggingMetaData(this IntEvent eventMsg)
        {
            var correlationId = eventMsg.CorrelationId;
            var clientName = eventMsg.ClientName;

            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            var correlationName = EventCorrelation.Name;

            var loggingMetaData = new Dictionary<string, object>()
            {
                {correlationName, correlationId}
            };

            // Set the correlation data
            new CorrelationContextFactory().Create(correlationId, correlationName);
            eventMsg.SetCorrelationDetails(correlationId, clientName);
            return loggingMetaData;
        }
    }
}
