﻿using System;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;

namespace GreetaIO.API.Infrastructure.AutoMapper
{
    public class ItemProfile : Profile
    {

        public ItemProfile()
        {
            CreateMap<Item, ItemDTO>()
                .ForMember(u => u.ItemId, opt => opt.MapFrom(src => src.Id.ToString()))
                .ForMember(u => u.ItemType,
                    opt => opt.MapFrom(src => Enum.GetName(typeof(ItemType), src.ItemType)))
                .ForMember(u => u.RewardsType,
                    opt => opt.MapFrom(src => Enum.GetName(typeof(RewardsType), src.RewardPoints.RewardsType)))
                .ForMember(u => u.PointsReward, opt => opt.MapFrom(src => src.RewardPoints.PointsReward.ToString()))
                .ForMember(u => u.PointsCost, opt => opt.MapFrom(src => src.RewardPoints.PointsCost))
                .ForMember(u => u.IsProduct, opt => opt.MapFrom(src => src.IsProduct));


        }

    }
}
