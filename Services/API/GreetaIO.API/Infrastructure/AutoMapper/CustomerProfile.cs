﻿using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility.Helper;

namespace GreetaIO.API.Infrastructure.AutoMapper
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {

            CreateMap<Location, LocationDTO>()
                .ForMember(u => u.LocationId, opt => opt.MapFrom(src => src.Id))
                .ForMember(u => u.LocationName, opt => opt.MapFrom(src => src.LocationName))
                .ForMember(u => u.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(u => u.Contacts, opt => opt.MapFrom(src => src.Contacts))
                .ForMember(u => u.CustomerId, opt => opt.MapFrom(src => src.Customer.Id))
                .ForMember(u => u.CustomerName, opt => opt.MapFrom(src => src.Customer.CompanyName))
                .ForMember(u => u.Transactions, opt => opt.MapFrom(src => src.Transactions))
                .ForMember(u => u.LocationType, opt => opt.MapFrom(src => src.LocationType.GetEnumName()));

            CreateMap<Transaction, TransactionDTO>()
                .ForMember(u => u.TransactionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(u => u.Item, opt => opt.MapFrom(src => src.Item))
                .ForMember(u => u.User, opt => opt.MapFrom(src => src.User.ToDto()))
                .ForMember(u => u.Location, opt => opt.MapFrom(src => src.Location.ToDto()))
                .ForMember(u => u.PointsReward, opt => opt.MapFrom(src => src.PointsReward))
                .ForMember(u => u.TransactionType, opt => opt.MapFrom(src => src.TransactionType.GetEnumName()));
                



            CreateMap<Address, AddressDTO>()
                .ForMember(u => u.Address1, opt => opt.MapFrom(src => src.Address1))
                .ForMember(u => u.Address2, opt => opt.MapFrom(src => src.Address2))
                .ForMember(u => u.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(u => u.State, opt => opt.MapFrom(src => src.State))
                .ForMember(u => u.Suburb, opt => opt.MapFrom(src => src.Suburb));

            CreateMap<Contact, ContactDTO>()
                .ForMember(u => u.ContactName, opt => opt.MapFrom(src => src.ContactName))
                .ForMember(u => u.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(u => u.Phone, opt => opt.MapFrom(src => src.Phone));


            CreateMap<Customer, CustomerDTO>()
                .ForMember(u => u.CustomerId, opt => opt.MapFrom(src => src.Id.ToString()))
                .ForMember(u => u.CustomerName, opt => opt.MapFrom(src => src.CompanyName))
                //.ForMember(u => u.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(u => u.Contact, opt => opt.MapFrom(src => src.Contact));


            

        }

    }
}
