﻿using System;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;

namespace GreetaIO.API.Infrastructure.AutoMapper
{
    public class UserImageProfile : Profile
    {

        public UserImageProfile()
        {
            CreateMap<UserImage, UserImageDTO>()
                .ForMember(u => u.ImageData, opt => opt.MapFrom(src => Convert.ToBase64String(src.ImageData)))
                .ForMember(u => u.UserImageType,
                    opt => opt.MapFrom(src => Enum.GetName(typeof(UserImageType), src.UserImageType)))
                .ForMember(u => u.UserImageId, opt => opt.MapFrom(src => src.Id.ToString()));
            //.ForMember(u => u.UserId, opt => opt.MapFrom(src => src.UserId.ToString()));

        }

    }
}
