﻿namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public interface IToDto<out T>
    {
        T ToDto();
    }
}