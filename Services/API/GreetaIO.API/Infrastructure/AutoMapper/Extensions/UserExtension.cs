﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class UserExtension
    {

        public static UserDTO ToDtoImage(this User user,IMapper mapper)
        {
            return new UserDTO()
            {
                UserId = user.Id.ToString(),
                AliasName = user.AliasName,
                CurrentRewards = user.CurrentRewards,
                ContactDTO = user.Contact.ToDto(),
                TargetId = user.TargetId ?? "" ,
                UserImages = user.UserImages.Select(x=>  mapper.Map<UserImageDTO>(x)).ToList(),
                UserType = Enum.GetName(typeof(UserType), user.UserType)
            };
        }

        public static IEnumerable<UserDTO> ToDtos(this IEnumerable<User> users)
        {

            foreach (var item in users)
            {
                yield return item.ToDto();
            }

        }


        public static IEnumerable<UserDTO> ToDtoImages(this IEnumerable<User> users,IMapper mapper)
        {

            foreach (var item in users)
            {
                yield return item.ToDtoImage(mapper);
            }

        }


        public static UserDTO ToDto(this User user)
        {
            return new UserDTO()
            {
                UserId = user.Id.ToString(),
                AliasName = user.AliasName,
                CurrentRewards = user.CurrentRewards,
                ContactDTO = user.Contact.ToDto(),
                TargetId = user.TargetId ?? "" ,
                UserImages = new List<UserImageDTO>(),
                UserType = Enum.GetName(typeof(UserType), user.UserType),
                UserImageCount = user.UserImageCount
                
            };
        }

    }
}
