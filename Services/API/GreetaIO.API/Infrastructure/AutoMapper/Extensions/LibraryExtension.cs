﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.Utility.Helper;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class LibraryExtension
    {

        public static IEnumerable<LibraryDTO> ToDtos(this IEnumerable<Library> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }


        public static LibraryDTO ToDto(this Library item)
        {
            return new LibraryDTO()
            {
                LibraryId = item.Id,
                LibraryNumber = item.LibraryNumber,
                Name = item.Name,
                Description = item.Description,
                LibraryType = item.LibraryType.GetEnumName(),
                LocationId = item.Location.Id,
                LibraryUsers = new List<UserDTO>()

            };
        }

        public static LibraryDTO ToDtoLibraryUsers(this Library item)
        {
            return new LibraryDTO()
            {
                LibraryId = item.Id,
                LibraryNumber = item.LibraryNumber,
                Name = item.Name,
                Description = item.Description,
                LibraryType = item.LibraryType.GetEnumName(),
                LocationId = item.Location.Id,
                LibraryUsers = item.LibraryUsers.ToDtos()
            };
        }

        

    }
}
