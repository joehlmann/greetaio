﻿using System.Collections.Generic;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class ExtensionHelper
    {

        public static IEnumerable<TResult> ToDtos<TSource,TResult>(IEnumerable<TSource> sourceItems) where TSource : IToDto<TResult>
        {
            foreach (var item in sourceItems)
            {
                yield return item.ToDto();
            }
        }

    }
}
