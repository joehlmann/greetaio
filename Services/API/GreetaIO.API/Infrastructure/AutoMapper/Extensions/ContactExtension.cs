﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.ValueObjects;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class ContactExtension
    {

        public static IEnumerable<ContactDTO> ToDtos(this IEnumerable<Contact> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }


        public static ContactDTO ToDto(this Contact item)
        {
            var result = item != null
                ? new ContactDTO()
                {
                    ContactName = item.ContactName,
                    Email = item.Email,
                    Phone = item.Phone
                }
                : ContactDTO.Empty();
            return result;
        }

        public static Contact ToValueObject(this ContactDTO item)
        {
            return Contact.Create(item.ContactName, item.Phone, item.Email);
        }

    }
}
