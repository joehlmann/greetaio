﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class LibraryUserExtension
    {
        public static LibraryUserDTO ToDto(this LibraryUser item)
        {
            return new LibraryUserDTO()
            {
                LibraryId = item.LibraryId,
                UserId = item.UserId

            };
        }

        public static IEnumerable<LibraryUserDTO> ToDtos(this IEnumerable<LibraryUser> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }

    }
}
