﻿using System;
using System.Collections.Generic;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class TransactionExtension
    {

       
            public static IEnumerable<TransactionDTO> ToDtos(this IEnumerable<Transaction> items,IMapper mapper)
            {
                foreach (var item in items)
                {
                    yield return item.ToDto(mapper);
                }
            }


            public static IEnumerable<string> ToDtosString(this IEnumerable<Transaction> items)
            {
                foreach (var item in items)
                {
                    yield return item.ToDtoString();
                }
            }


            public static string ToDtoString(this Transaction item)
            {
                return item.ToString();
            }

            public static TransactionDTO ToDto(this Transaction item,IMapper mapper)
            {
                return new TransactionDTO()
                {
                    TransactionId = item.Id.ToString(),
                    Item = mapper.Map<ItemDTO>(item.Item),
                    Location = item.Location.ToDto(),
                    PointsReward = item.PointsReward,
                    TransactionType = Enum.GetName(typeof(TransactionType), item.TransactionType),
                    User = item.User.ToDto()

                };
            }

    }
}
