﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.Utility.Helper;

namespace GreetaIO.API.Infrastructure.AutoMapper.Extensions
{
    public static class LocationExtension
    {

        public static IEnumerable<LocationDTO> ToDtos(this IEnumerable<Location> items)
        {
            foreach (var item in items)
            {
                yield return item.ToDto();
            }
        }


        public static LocationDTO ToDto(this Location item)
        {
            return new LocationDTO()
            {
                LocationId = item.Id,
                Address = item.Address.ToDto(),
                Contacts = item.Contacts.ToDtos(),
                CustomerId = item.Customer.Id,
                CustomerName = item.Customer.CompanyName,
                LocationName = item.LocationName,
                LocationType =  item.LocationType.GetEnumName()

            };
        }

        public static LocationDTO ToDtoTransactions(this Location item)
        {
            return new LocationDTO()
            {
                LocationId = item.Id,
                Address = item.Address.ToDto(),
                Contacts = item.Contacts.ToDtos(),
                CustomerId = item.Customer.Id,
                CustomerName = item.Customer.CompanyName,
                LocationName = item.LocationName,
                TransactionStrings = item.Transactions.ToDtosString()
            };
        }

    }
}
