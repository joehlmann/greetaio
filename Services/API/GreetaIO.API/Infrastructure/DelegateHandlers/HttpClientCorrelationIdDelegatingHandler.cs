﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CorrelationId;

namespace GreetaIO.API.Infrastructure.DelegateHandlers
{
    public class HttpClientCorrelationIdDelegatingHandler : DelegatingHandler
    {
        private readonly ICorrelationContextAccessor _correlationContext;


        public HttpClientCorrelationIdDelegatingHandler(ICorrelationContextAccessor correlationContext)
        {
            _correlationContext = correlationContext;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var correlationId = _correlationContext?.CorrelationContext?.CorrelationId;
            var correlationHeader = _correlationContext?.CorrelationContext?.Header;


            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            if (!string.IsNullOrEmpty(correlationHeader))
            {
                request.Headers.Add(correlationHeader, correlationId);
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
