﻿using Microsoft.AspNetCore.Authorization;

namespace GreetaIO.API.Infrastructure.Authorization
{
    public interface IAssertions
    {
        bool CanReadPickups(AuthorizationHandlerContext context);
        bool CanWritePickups(AuthorizationHandlerContext context);
        bool CanDeletePickups(AuthorizationHandlerContext context);
    }
}