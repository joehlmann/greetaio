﻿using GreetaIO.Infrastructure.TokenContext;

namespace GreetaIO.API.Infrastructure.Authorization
{
    public class TokenContextService : ITokenContextService
    {
        
        private string AccessToken { get; set; }
        public void SetAccessToken(string token)
        {
            AccessToken = token;
        }

        public string GetAccessToken()
        {
            return AccessToken;
        }

        
    }
}
