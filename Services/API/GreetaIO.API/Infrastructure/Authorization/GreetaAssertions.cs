﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace GreetaIO.API.Infrastructure.Authorization
{
    public class GreetaAssertions : IAssertions
    {
        public bool CanDeletePickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == GreetaIdentity.Claims.Scope.Type && c.Issuer == GreetaIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == GreetaIdentity.Scopes.DeletePickups.Name && s.Issuer == GreetaIdentity.Scopes.DeletePickups.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanReadPickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == GreetaIdentity.Claims.Scope.Type && c.Issuer == GreetaIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == GreetaIdentity.Scopes.ReadPickups.Name && s.Issuer == GreetaIdentity.Scopes.ReadPickups.Issuer
            ));

            return hasRequiredScopes;
        }

        public bool CanWritePickups(AuthorizationHandlerContext context)
        {
            // Split the scopes string into an array
            var scopes = context.User.Claims.Where(c => c.Type == GreetaIdentity.Claims.Scope.Type && c.Issuer == GreetaIdentity.Claims.Scope.Issuer).ToList();

            // Succeed if the scope array contains the required scope
            var hasRequiredScopes = scopes.Any(s => (
                // Check if the access token has the read only scope
                s.Value == GreetaIdentity.Scopes.WritePickups.Name && s.Issuer == GreetaIdentity.Scopes.WritePickups.Issuer
            ));

            return hasRequiredScopes;
        }
    }
}
