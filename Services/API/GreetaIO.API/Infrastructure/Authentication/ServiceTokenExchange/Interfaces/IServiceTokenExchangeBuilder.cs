﻿using Microsoft.Extensions.DependencyInjection;

namespace GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface IServiceTokenExchangeBuilder
    {
        IServiceCollection Services { get; }
    }
}