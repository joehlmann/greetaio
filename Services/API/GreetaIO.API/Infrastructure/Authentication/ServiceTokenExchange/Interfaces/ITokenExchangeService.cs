﻿using System.Threading.Tasks;
using GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeService
    {
        Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options);
    }
}