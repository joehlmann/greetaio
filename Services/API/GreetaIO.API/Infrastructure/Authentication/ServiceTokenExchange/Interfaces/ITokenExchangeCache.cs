﻿using GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Models;

namespace GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces
{
    public interface ITokenExchangeCache
    {
        TokenExchangeResult FindToken(string referenceToken);

        void AddToken(TokenExchangeResult tokenResult);

        void RemoveToken(TokenExchangeResult tokenResult);
    }
}