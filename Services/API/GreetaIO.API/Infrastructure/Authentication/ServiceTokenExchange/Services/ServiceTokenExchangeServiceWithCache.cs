﻿using System;
using System.Threading.Tasks;
using GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Interfaces;
using GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Models;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange.Services
{
    public class ServiceTokenExchangeServiceWithCache : ICachingTokenExchangeService
    {
        private readonly ITokenExchangeCache _cache;
        private readonly ILogger<ServiceTokenExchangeServiceWithCache> _logger;
        private readonly ITokenExchangeService _tokenExchangeService;
        public ServiceTokenExchangeServiceWithCache(ITokenExchangeService tokenExchangeService ,ITokenExchangeCache cache, ILogger<ServiceTokenExchangeServiceWithCache> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));;
            _tokenExchangeService = tokenExchangeService ?? throw new ArgumentNullException(nameof(tokenExchangeService));;
        }
        public async Task<TokenExchangeResult> ExchangeTokenAsync(string accessToken, ServiceTokenExchangeOptions options)
        {
            _logger.LogInformation("Checking cache for the presence of token");
            var cachedToken = _cache.FindToken(accessToken);

            if (cachedToken == null)
            {
                _logger.LogInformation("Token not found in cache");
                var tokenResult = await PerformOnlineTokenExchange(accessToken, options);

                AddTokenToCache(tokenResult);
                return tokenResult;
            }

            if (cachedToken.AccessTokenExpiryTime < DateTimeOffset.UtcNow)
            {
                _logger.LogInformation("Expired token found in cache");
                _cache.RemoveToken(cachedToken);

                var tokenResult = await PerformOnlineTokenExchange(accessToken, options);

                AddTokenToCache(tokenResult);
                return tokenResult;
            }

            _logger.LogInformation("Token found in cache, returning token");
            return cachedToken;
            
        }
        private async Task<TokenExchangeResult> PerformOnlineTokenExchange(string referenceToken, ServiceTokenExchangeOptions options)
        {
            return await _tokenExchangeService.ExchangeTokenAsync(referenceToken, options);
        }
        private void AddTokenToCache(TokenExchangeResult tokenExchangeResult)
        {
            if (tokenExchangeResult.AccessToken == null)
            {
                return;
            }

            _cache.AddToken(tokenExchangeResult);
        }
    }
}
