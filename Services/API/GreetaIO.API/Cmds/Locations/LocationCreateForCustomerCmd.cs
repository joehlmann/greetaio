﻿using System;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Enums;
using MediatR;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationCreateForCustomerCmd : IRequest<LocationDTO>
    {
        public string CustomerId { get; private set; }

        public string LocationName { get; private set; }

        public int LocationNumber { get; private set; }

        public LocationType LocationType { get; private set; }


        public AddressDTO Address { get; private set; }


        public ContactDTO Contact { get; private set; }

        public LocationCreateForCustomerCmd(string customerId, string locationName,int locationNumber, LocationType locationType, AddressDTO address, ContactDTO contact)
        {
            CustomerId = customerId;
            LocationName = locationName;
            LocationNumber = locationNumber;
            LocationType = locationType;
            Address = address;
            Contact = contact;
        }


        //public LocationCreateForCustomerCommand(string customerId, string locationName, string address1,
        //    string address2,
        //    string state,
        //    string suburb,
        //    string postCode,
        //    ContactDTO contact)
        //{


        //    Address = new AddressDTO()
        //    {
        //        Address1 = address1,
        //        Address2 = address2,
        //        PostCode = postCode,
        //        State = state,
        //        Suburb = suburb
        //    };
        //    CustomerId = customerId;
        //    LocationName = locationName;
        //    Contact = contact;
        //}

        public LocationCreateForCustomerCmd(string customerId, string locationName, string address1,
            string address2,
            string state,
            string suburb,
            string postCode,
            string contactName,
            string contactPhone,
            string email)
        {
            CustomerId = customerId;
            LocationName = locationName;


            Address = new AddressDTO()
            {
                Address1 = address1,
                Address2 = address2,
                PostCode = postCode,
                State = state,
                Suburb = suburb
            };
            Contact = new ContactDTO()
            {
                ContactName = contactName,
                Phone = contactPhone,
                Email = email
            };
        }

        public static LocationCreateForCustomerCmd CreateNew(string customerId, LocationCreateForCustomerCmd cmd)
        {
            if (cmd == null) throw new ArgumentNullException(nameof(cmd));
            return new LocationCreateForCustomerCmd(customerId, cmd.LocationName, cmd.LocationNumber,cmd.LocationType, cmd.Address, cmd.Contact);
        }
    }
}
