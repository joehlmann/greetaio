﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using EventBusMsgBrokerSvc;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Libraries;
using GreetaIO.API.Cmds.UserImages;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationUserImageAtLocationHandler : IRequestHandler<LocationUserImageAtLocationCmd,UserDTO>
    {

        
        private readonly ICrudRepository<Library, int> _libraryRepository;
        private readonly ILogger<LibraryFindUserForCreateImageHandler> _logger;
        //private readonly IIdentityService _identityService;
        private readonly IGreetaQueries _greetaQueries;
        private readonly IRPCReplyService<UserImageSearchCompletedIntEvent> _rpcReplyService;
        
        private readonly IMapper _mapper;


        public LocationUserImageAtLocationHandler(ILogger<LibraryFindUserForCreateImageHandler> logger,
            IMapper mapper, ICrudRepository<Library, int> libraryRepository, IGreetaQueries greetaQueries,
            IRPCReplyService<UserImageSearchCompletedIntEvent> rpcReplyService)
        {   
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper;
            _libraryRepository = libraryRepository;
            _greetaQueries = greetaQueries;
            _rpcReplyService = rpcReplyService;
        }

        public async Task<UserDTO> Handle(LocationUserImageAtLocationCmd cmd, CancellationToken cancellationToken)
        {
            _logger.LogInformation("LocationUserImageAtLocationCmd at LocationId:{LocationId}  TimeId: {TimeId})",
                cmd.LocationId, cmd.TimeId);

            var libDto = await  _greetaQueries.GetLibraryForLocationAsync(cmd.LocationId);

            var library =  await _libraryRepository.GetAsync(libDto.LibraryId);

            var userImage = UserImage.Create(cmd.ImageData.ToByte(),"",cmd.TimeId,cmd.RequestId);


            library.FindUnKnownUser(userImage);

            _libraryRepository.Update(library);

            var committed =  await _libraryRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);


            var result = await _rpcReplyService.GetMessageAsync(userImage.SessionId);

            var userDto = await _greetaQueries.GetUserAsync(result.UserId);


            // Failure Returns Empty Dto
            var response = committed ? userDto : UserDTO.Empty();

            return response;
            
        }
    }


    /// <summary>
    /// Use for Idempotency
    /// </summary>
    public class LocationUserImageAtLocationCmdIdentifiedHandler : IdentifiedCommandHandler<LocationUserImageAtLocationCmd, UserDTO>
    {
        public LocationUserImageAtLocationCmdIdentifiedHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<LocationUserImageAtLocationCmd, UserDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override UserDTO CreateResultForDuplicateRequest()
        {
            return UserDTO.Empty();
        }
    }
}
