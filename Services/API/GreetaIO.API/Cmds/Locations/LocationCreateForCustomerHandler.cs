﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationCreateForCustomerHandler : IRequestHandler<LocationCreateForCustomerCmd, LocationDTO>
    {

        private readonly ICrudRepository<Location,int> _repository;
        private readonly ICrudRepository<Customer, int> _customerRepository;
        private readonly ILogger<CustomerCreateHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;


        public LocationCreateForCustomerHandler(ICrudRepository<Location, int> repository,IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<CustomerCreateHandler> logger, ICrudRepository<Customer, int> customerRepository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _customerRepository = customerRepository;
        }

        public async Task<LocationDTO> Handle(LocationCreateForCustomerCmd request, CancellationToken cancellationToken)
        {


            var customer = await  _customerRepository.GetAsync(request.CustomerId.ToInt()).ConfigureAwait(false);

            var address = Address.Create(request.Address.State, request.Address.Suburb, request.Address.Address1,
                request.Address.Address2, request.Address.PostCode);

            var contact = Contact.Create(request.Contact.ContactName, request.Contact.Phone, request.Contact.Email);

            var location = Location.Create(request.LocationNumber,request.LocationName,request.LocationType, customer,address,contact);

            _logger.LogInformation("----- Creating Location - Customer: {@location}", location);

            _repository.Add(location);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);

            var response = result ? location.ToDto() : LocationDTO.Empty();

            return response;

        }
    }


    public class LocationCreateForCustomerIdentifiedCommandHandler : IdentifiedCommandHandler<LocationCreateForCustomerCmd, LocationDTO>
    {
        public LocationCreateForCustomerIdentifiedCommandHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<LocationCreateForCustomerCmd, LocationDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override LocationDTO CreateResultForDuplicateRequest()
        {
            return LocationDTO.Empty();
        }
    }
}