﻿using System;
using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationUserImageAtLocationCmd :IRequest<UserDTO>
    {
        public Guid RequestId { get; private set; }
        public int LocationId { get; private set; }

        public string ImageData { get; private set; }

        public long TimeId { get; private set; }

        public LocationUserImageAtLocationCmd(string imageData,int locationId, long timeId,Guid requestId)
        {
            ImageData = imageData;
            LocationId = locationId;
            TimeId = timeId;
            RequestId = requestId;
        }
    }

    


}
