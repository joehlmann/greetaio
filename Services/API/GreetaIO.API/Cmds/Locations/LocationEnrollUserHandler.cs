﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationEnrollUserHandler : IRequestHandler<LocationEnrollUserCmd,UserDTO>
    {
        private readonly ICrudRepository<User, Guid> _repository;
        private readonly ICrudRepository<Location, Guid> _locationRepository;
        private readonly ICrudRepository<Library, Guid> _libraryRepository;
        private readonly ApiContext _context;
        private readonly ILogger<CustomerCreateHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IVrServices _vrServices;


        public LocationEnrollUserHandler(ICrudRepository<User, Guid> repository, ICrudRepository<Location, Guid> locationRepository,
            ILogger<CustomerCreateHandler> logger, IIdentityService identityService, IIntegrationEventSvc integrationEventSvc,
            IMediator mediator, ICrudRepository<Library, Guid> libraryRepository, ApiContext context ,IVrServices vrServices)
        {

            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _locationRepository = locationRepository ?? throw new ArgumentNullException(nameof(locationRepository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _libraryRepository = libraryRepository;
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _vrServices = vrServices ?? throw new ArgumentException(nameof(vrServices));
        }


        public async  Task<UserDTO> Handle(LocationEnrollUserCmd message, CancellationToken cancellationToken)
        {
            

            _logger.LogInformation("----- Enroll User: {@message}", message.ToString());

            var library = await _context.Libraries.Where(x => x.Location.Id == message.LocationId)
                .FirstAsync(cancellationToken).ConfigureAwait(false);

            var user = User.Enroll(message.ImageData);


            var result = await _vrServices.EnrollUser(library, user).ConfigureAwait(false);


            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            // Add Integration event to EventBus
            var @event = new UserEnrolledIntEvent(result.Id,library.Id,message.LocationId);
            await _integrationEventSvc.AddAndSaveEventAsync(@event).ConfigureAwait(false);
            _logger.LogInformation("-----User Enrolled: {@event}", @event.ToString());


            return result.ToDto();


        }
    }


    
}
