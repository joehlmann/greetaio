﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.Locations
{
    public class LocationEnrollUserCmd : IRequest<UserDTO>
    {

        

        public int LocationId { get; private set; }

        public IEnumerable<byte[]> ImageData { get; private set; }


        public LocationEnrollUserCmd(IEnumerable<byte[]> imageData,int locationId)
        {
            ImageData = imageData;
            LocationId = locationId;
            
        }

    }

   
}
