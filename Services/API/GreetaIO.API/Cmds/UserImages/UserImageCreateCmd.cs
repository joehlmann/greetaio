﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.UserImages
{
    public class UserImageCreateCmd :IRequest<UserImageDTO>
    {
        public string  UserId { get; }

        public string ImageData  { get; }

        
        public UserImageCreateCmd(string userId, string imageData)
        {
            UserId = userId;
            ImageData = imageData;
        }
    }
}
