﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.UserImages
{
    public class UserImageCreateHandler : IRequestHandler<UserImageCreateCmd,UserImageDTO>
    {

        private readonly ICrudRepository<User,int> _repository;
        private readonly ILogger<UserImageCreateHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public UserImageCreateHandler(ICrudRepository<User, int> repository,
            IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<UserImageCreateHandler> logger,
            IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper;
        }

        public async Task<UserImageDTO> Handle(UserImageCreateCmd cmd, CancellationToken cancellationToken)
        {
            _logger.LogInformation("----- Creating UserImage - User: {@UserId}", cmd.UserId);

            var user = await  _repository.GetAsync(cmd.UserId.ToInt());

            var userImage = UserImage.Create(cmd.ImageData.ToByte());

            user.AddUserImage(userImage);

            _repository.Update(user);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);


            var response = result ? _mapper.Map<UserImageDTO>(userImage) : UserImageDTO.Empty();

            return response;


        }
    }


    /// <summary>
    /// Use for Idempotency
    /// </summary>
    public class UserImageCreateIdentifiedCommandHandler : IdentifiedCommandHandler<UserImageCreateCmd, UserImageDTO>
    {
        public UserImageCreateIdentifiedCommandHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<UserImageCreateCmd, UserImageDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override UserImageDTO CreateResultForDuplicateRequest()
        {
            return UserImageDTO.Empty();
        }
    }
}
