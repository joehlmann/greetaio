﻿using System.Collections.Generic;

namespace GreetaIO.API.Cmds.Pickups
{
    public class AddPickupItemDTO
    {
        public int Quantity { get; set; }
        public string Description { get; set; }
        public int WeightKg { get; set; }
        public bool FoodStuff { get; set; }
        public List<CreateDangerousGoodItemDTO> DangerousGoods { get;set; }
    }
}
