﻿using System;
using System.Collections.Generic;
using MediatR;

namespace GreetaIO.API.Cmds.Pickups
{
    public class CreatePickupCommand: IRequest<CreatePickupResponse>
    {
        public Guid Id { get;set; }
        public string Number { get; set; }
        public DateTime PickupDate { get; set; }
        public DateTime? BookedDateTime { get;set; }
        public int? BookedByUserId { get;set; }
        public DateTime ReadyTime { get;set; }
        public DateTime CloseTime { get; set; }
        public string ChargeAccountCode { get; set; }
        public string SpecialInstructions { get; set; }
        public string SenderCode { get;set; }
        public string SenderName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 {get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }

        public string ContactName { get;set; }
        public string ContactPhone { get; set; }
        
        
        public string ChepPalletTransferDockerNumber { get;set; }
        public int ExchangeChepPalletsIn { get; set; }
        public int ExchangeChepPalletsOut { get; set; }
        
        public int TransferChepPalletsToBex { get; set; }
        public int TransferChepPalletsToReceiver { get; set; }

        public string LoscamPalletTransferDockerNumber { get;set; }
        public int ExchangeLoscamPalletsIn { get; set; }
        public int ExchangeLoscamPalletsOut { get; set; }
        
        public int TransferLoscamPalletsToBex { get; set; }
        public int TransferLoscamPalletsToReceiver { get; set; }

        public List<AddPickupItemDTO> Items { get; set; }

        public string UserId { get; set; }

    }
}
