﻿using System.Collections.Generic;

namespace GreetaIO.API.Cmds.Pickups
{
    public class PickupItemsDTO
    {
        public List<PickupItemDTO> Items { get;set; }
        
    }
}
