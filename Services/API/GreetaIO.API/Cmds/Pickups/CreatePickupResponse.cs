﻿using System;

namespace GreetaIO.API.Cmds.Pickups
{
    public class CreatePickupResponse 
    {
       
        public Guid Id { get; set; }
        public int Number { get; set; }
    }
}
