﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Pickups
{
    public class CreatePickupCommandHandler : IRequestHandler<CreatePickupCommand, CreatePickupResponse>
    {
        //private readonly IMapper _mapper;
        //private readonly ILibraryRepository _pickupRepository;

        //private readonly ISuburbRepository _suburbRepository;
        //private readonly ICustomerRepository _customerRepository;
        //private readonly IPickupNumberService _pickupService;

        private readonly ILogger<CreatePickupCommandHandler> _logger;

        //private readonly IUserRepository _userRepository;
        //private readonly IDangerousGoodRepository _dangerousGoodRepository;
        private readonly int _maximumNumberOfPickupRenumberAttempts;

        public CreatePickupCommandHandler(
            //ILibraryRepository pickupRepository,
            //IMapper mapper,
            //ISuburbRepository suburbRepository,
            //ICustomerRepository customerRepository,
          //  IPickupNumberService pickupService,
            //IUserRepository userRepository,
            ILogger<CreatePickupCommandHandler> logger,
            //IDangerousGoodRepository dangerousGoodRepository,
            IConfiguration config)
        {
            //_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            //_pickupRepository = pickupRepository ?? throw new ArgumentNullException(nameof(pickupRepository));
            //_suburbRepository = suburbRepository ?? throw new ArgumentNullException(nameof(suburbRepository));
            //_customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            //_pickupService = pickupService ?? throw new ArgumentNullException(nameof(pickupService));
            //_userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            //_dangerousGoodRepository = dangerousGoodRepository ?? throw new ArgumentNullException(nameof(dangerousGoodRepository));


            if (!int.TryParse(config.GetSection("Application")["MaximumNumberOfPickupRenumberAttempts"],
                out _maximumNumberOfPickupRenumberAttempts))
            {
                _maximumNumberOfPickupRenumberAttempts = 5; // Default the retry attempts to 5
            }

            ;


        }

        public async Task<CreatePickupResponse> Handle(
            CreatePickupCommand request,
            CancellationToken cancellationToken)
        {
            // Convert the request into a domain object
            //  var pickup = new Location();
            //  var pickupItems = ConstructPickupItems(request.Items);

            //  var errors = new List<Error>();
            //  var autoGeneratePickupNumber = false;
            ////  var senderAccountTask = Task.FromResult(new CustomerAccount(request.SenderCode,request.SenderName));
            //  var bookedByUserTask = Task.FromResult<Domain.Entities.User>(null);

            //  // Populate the required data for validations and business logic
            //  var chargeAccountTask = FindCustomerAccountAsync(request.ChargeAccountCode);

            //  if (!string.Equals(request.ChargeAccountCode,request.SenderCode,StringComparison.OrdinalIgnoreCase))
            //  {
            //      if (!string.IsNullOrEmpty(request.SenderCode))
            //      {
            //  //        senderAccountTask = FindCustomerAccountAsync(request.SenderCode);
            //      }                
            //  }
            //  else
            //  {
            //    //  senderAccountTask = chargeAccountTask;
            //  }

            // var suburbTask = FindSuburbAsync(request.Suburb, request.Postcode);

            // Allow the number generation to run async
            //var numberTask = Task.FromResult(request.Number);

            //if (string.IsNullOrEmpty(request.Number))
            //{
            //   var  anumberTask = _pickupService.GenerateNextAvailablePickupNumberAsync();
            //    // Let the handler know that it's ok to regenerate a new number if a number conflict occurs
            //    autoGeneratePickupNumber = true;
            //}

            //// Check the booked by user id
            //if (request.BookedByUserId != null && request.BookedByUserId  > 0)
            //{
            //    //bookedByUserTask = FindUser(request.BookedByUserId.Value);
            //}

            // Wait for any of the lookups to complete
            //await Task.WhenAll(chargeAccountTask, suburbTask, numberTask, senderAccountTask, bookedByUserTask);

            //if (suburbTask.Result.Id == 0 &&
            //    (!string.IsNullOrEmpty(request.Suburb) && !string.IsNullOrEmpty(request.Postcode)))
            //{
            //    errors.Add(ValidationMessages.PickupInvalidSuburbDetails);
            //}

            //if (bookedByUserTask?.Result != null && bookedByUserTask.Result.Id == 0 && request.BookedByUserId != null && request.BookedByUserId > 0)
            //{
            //    errors.Add(ValidationMessages.PickupBookingUserIsInvalid);
            //}

            //var sender = senderAccountTask.Result;

            // Check the sender account
            // TODO: Add validation here
            //if(sender != null)
            //{
            //    pickup.SetSender(new CustomerAccount(sender.Id, sender.CustomerCode, sender.CustomerAccountName));
            //}

            //pickup.SetInstructions(request.SpecialInstructions);

            //// Perform the business logic
            //pickup.SetNumber(int.Parse(numberTask.Result));
            //pickup.SetAddress(request.AddressLine1, request.AddressLine2,suburbTask.Result);
            //pickup.SetPayingAccount(chargeAccountTask.Result);
            //pickup.SetPickupDate(request.PickupDate);
            //pickup.SetPickupWindow(request.ReadyTime, request.CloseTime);
            //pickup.SetContact(request.ContactName, request.ContactPhone);
            //pickup.SetBookerDetails(bookedByUserTask.Result,request.BookedDateTime);

            ////TODO: Store the context of token user calling the endpoint (application/user), booking application?
            //pickup.AddItems(await pickupItems);

            //pickup.AddPalletDetails(new PickupPallets(request.ExchangeChepPalletsOut,request.TransferChepPalletsToBex,request.TransferChepPalletsToReceiver,request.ChepPalletTransferDockerNumber,request.ExchangeLoscamPalletsOut,request.TransferLoscamPalletsToBex,request.TransferLoscamPalletsToReceiver,request.LoscamPalletTransferDockerNumber));

            //// Book the pickup
            //pickup.Book();

            // Any item errors?
            //    var itemErrors = pickup.Items.Where(x => x.HasErrors).Select(x => x.HasErrors).Any();

            //    if (errors.Any() || pickup.HasErrors || itemErrors)
            //    {
            //        if (pickup.Errors != null)
            //        {
            //            errors.AddRange(pickup.Errors);
            //        }

            //        if (itemErrors)
            //        {
            //            errors.AddRange(pickup.Items.Where(x => x.HasErrors).SelectMany(x => x.Errors));
            //        }
            //        return new CreatePickupResponse {Errors = errors};
            //    }

            //    // Push the data to the database context
            //    _pickupRepository.Add(pickup);

            //    // The number was originally auto generated so allow the system to save itself by regenerating numbers if a unique constraint failure occurs
            //    if (autoGeneratePickupNumber)
            //    {
            //        await SavePickupRepositoryWithRenumberSupportAsync(pickup, cancellationToken);
            //    }
            //    else
            //    {
            //        // Commit the changes
            //        errors.AddRange(await SavePickupRepositoryWithErrorsAsync(pickup, cancellationToken));
            //    }

            //    if (errors.Any())
            //    {
            //        return new CreatePickupResponse {Errors = errors};
            //    }

            //    // Refresh the data from storage cache (required to get the database ids)
            //    pickup = _pickupRepository.GetCacheOnly(pickup.Id);

            //    return new CreatePickupResponse {Id = pickup.Id, Number = pickup.PickupNumber};
            //}

            /// <summary>
            /// Find more details about the customer
            /// </summary>
            /// <param name="code">Customer account code</param>
            /// <returns></returns>
            //private Task<CustomerAccount> FindCustomerAccountAsync(string code)
            //{
            //    return !string.IsNullOrEmpty(code)
            //        ? _customerRepository.FindByCodeAsync(code)
            //        : Task.FromResult<CustomerAccount>(null);
            //}

            ///// <summary>
            ///// Find more details about the suburb
            ///// </summary>
            ///// <param name="name">Name of the suburb</param>
            ///// <param name="postcode">Postcode of the suburb</param>
            ///// <returns></returns>
            //private  Task<Suburb> FindSuburbAsync(string name, string postcode)
            //{
            //    return !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(postcode)
            //        ? _suburbRepository.FindByNameAndPostCodeAsync(name, postcode)
            //        : null;
            //}

            ///// <summary>
            ///// Find more details about the user
            ///// </summary>
            ///// <param name="userId">The id of the user</param>
            ///// <returns></returns>
            //private Task<User> FindUser(int userId)
            //{
            //    return userId > 0 ? _userRepository.FindById(userId) : null;
            //}

            /// <summary>
            /// Try and save any pickup changes renumbering the pickup on failure
            /// </summary>
            /// <param name="pickup">The pickup that's being saved</param>
            /// <param name="cancellationToken">Allows the user to cancel the save</param>
            //private async Task SavePickupRepositoryWithRenumberSupportAsync(PickupRoot pickup,
            //    CancellationToken cancellationToken)
            //{
            //    // Number of retry attempts
            //    var success = false;
            //    for (var currentRenumberAttempt = 0;
            //        currentRenumberAttempt < _maximumNumberOfPickupRenumberAttempts && !success;
            //        currentRenumberAttempt++)
            //    {
            //        try
            //        {
            //            // Commit the changes
            //            await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            //            success = true;
            //        }
            //        catch (DbUpdateException e) when (e.IsUniqueSqlConstraintError())
            //        {
            //            if (currentRenumberAttempt < _maximumNumberOfPickupRenumberAttempts)
            //            {
            //                success = false;
            //                // Generate a new number
            //                var newNumber = await _pickupService.GenerateNextAvailablePickupNumberAsync();

            //                // Log warning
            //                _logger.LogWarning(
            //                    "Regenerating pickup number due to unique conflict from {duplicatePickupNumber} to {newPickupNumber}.",
            //                    pickup.PickupNumber, newNumber);

            //                // Set the new number
            //                pickup.SetNumber(newNumber);
            //                _pickupRepository.UpdateCache(pickup);
            //            }
            //            else
            //            {
            //                throw;
            //            }
            //        }
            //    }
            //}

            /// <summary>
            /// Try and save any pickup changes while handling duplicate number errors
            /// </summary>
            /// <param name="pickup">The pickup that's being saved</param>
            /// <param name="cancellationToken">Allows the user to cancel the save</param>
            //private async Task<List<Error>> SavePickupRepositoryWithErrorsAsync(PickupRoot pickup,
            //    CancellationToken cancellationToken)
            //{
            //    var errors = new List<Error>();
            //    try
            //    {
            //        // Commit the changes
            //        await _pickupRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            //    }
            //    catch (DbUpdateException e) when (e.IsUniqueSqlConstraintError())

            //    {
            //        errors.Add(ValidationMessages.PickupCustomerProvidedDuplicatePickupNumber);

            //        // Log some debug information
            //        _logger.LogDebug("Duplicate pickup number provided {duplicatePickupNumber}.", pickup.PickupNumber);
            //    }

            //    return errors;
            //}

            //private async Task<List<PickupItem>> ConstructPickupItems(List<AddPickupItemDTO> items)
            //{
            //    var pickupItems = new List<PickupItem>();
            //    var itemsDg = items.Where(d => d.DangerousGoods != null).SelectMany(i => i.DangerousGoods).ToList();
            //    var dgDict = new Dictionary<int, DangerousGood>();

            //    if (itemsDg.Any())
            //    {
            //        dgDict = await LoadDGDetail(itemsDg);
            //    }

            //    foreach (var item in items)
            //    {
            //        var dgList = item.DangerousGoods?.Select(x => dgDict[x.UNNo] ?? new DangerousGood(x.UNNo)).ToList();
            //        pickupItems.Add(new PickupItem(item.Quantity, item.Description, item.WeightKg, item.FoodStuff, dgList));
            //    }

            //    return pickupItems;
            //}

            //public async Task<Dictionary<int, DangerousGood>> LoadDGDetail(List<CreateDangerousGoodItemDTO> dangerousGoods)
            //{
            //    // Lookup DG info
            //    var dgTasks = dangerousGoods.Select(x => x.UNNo).Distinct().Select(dgUN => _dangerousGoodRepository.GetByUNNumberAsync(dgUN)).ToArray();

            //    await Task.WhenAll(dgTasks);

            //    var dgDetails = new Dictionary<int, DangerousGood>();
            //    foreach (var dgTask in dgTasks)
            //    {
            //        if (dgTask.Result.UNNo > 0)
            //        {
            //            dgDetails[dgTask.Result.UNNo] = dgTask.Result;
            //        }
            //    }

            //    return dgDetails;
            //}

            return new CreatePickupResponse();
        }
    }
}
