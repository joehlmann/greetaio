﻿using System;

namespace GreetaIO.API.Cmds.Pickups
{
    public class PickupItemDTO
    {
        public Guid Id { get;set; }
        public string Description { get;set; }
        public int WeightKg { get;set; }
        public int Quantity {get; set; }


    }
}
