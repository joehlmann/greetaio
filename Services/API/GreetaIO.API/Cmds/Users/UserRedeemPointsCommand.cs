﻿using System;
using MediatR;

namespace GreetaIO.API.Cmds.Users
{
    public class UserRedeemPointsCmd : IRequest<bool>
    {
        public Guid UserId { get; private set; }

        public int PointsToRedeem { get; private set; }

        public UserRedeemPointsCmd(int pointsToRedeem, Guid userId)
        {
            PointsToRedeem = pointsToRedeem;
            UserId = userId;
        }

    }

    
}
