﻿using MediatR;

namespace GreetaIO.API.Cmds.Users
{
    public class UserPersonliseCmd : IRequest<int>
    {
        public  int LocationId { get; }

        public int UserId { get; }

        public string Name { get; }

        public UserPersonliseCmd(int locationId, int userId, string name)
        {
            LocationId = locationId;
            UserId = userId;
            Name = name;
        }
        

    }
}
