﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;
using MoreLinq;

namespace GreetaIO.API.Cmds.Users
{
    public class UserCreateCmdHandler : IRequestHandler<UserCreateCmd,UserDTO>
    {

        private readonly ICrudRepository<User,int> _repository;
        private readonly ILogger<UserCreateCmdHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public UserCreateCmdHandler(ICrudRepository<User, int> repository,
            IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<UserCreateCmdHandler> logger,
            IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper;
        }

        public async Task<UserDTO> Handle(UserCreateCmd cmd, CancellationToken cancellationToken)
        {
            _logger.LogInformation("----- Creating User at Location: {@LocationId}", cmd.LocationId);

            
            var user = User.Create(cmd.AliasName,cmd.ContactDTO.ToValueObject(),UserType.New,0);

            var userImages = cmd.UserImages.Select(ui => UserImage.Create(ui.ImageData.ToByte(),DateTime.Now.Ticks , ui.Url));

            userImages.ForEach(x=> user.AddUserImage(x));

            //user.AddUserImage(userImage);

            _repository.Add(user);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);


            var response = result ? user.ToDtoImage(_mapper) : UserDTO.Empty();

            return response;


        }
    }

    public class UserCreateIdentifiedCmdHandler : IdentifiedCommandHandler<UserCreateCmd, UserDTO>
    {
        public UserCreateIdentifiedCmdHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<UserCreateCmd, UserDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override UserDTO CreateResultForDuplicateRequest()
        {
            return UserDTO.Empty();
        }
    }
}