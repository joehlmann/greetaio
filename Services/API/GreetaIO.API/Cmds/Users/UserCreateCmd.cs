﻿using System.Collections.Generic;
using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.Users
{
    public class UserCreateCmd : IRequest<UserDTO>
    {


        public string LocationId { get;  }

        public string AliasName { get; }

        public ContactDTO ContactDTO { get; private set; }
        

        public IEnumerable<UserImageDTO> UserImages { get;  }

        public UserCreateCmd(string locationId, string aliasName, ContactDTO contactDTO, IEnumerable<UserImageDTO> userImages)
        {
            LocationId = locationId;
            AliasName = aliasName;
            ContactDTO = contactDTO;
            UserImages = userImages;
        }

    }
}
