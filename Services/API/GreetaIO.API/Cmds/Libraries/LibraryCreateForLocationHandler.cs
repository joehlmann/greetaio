﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Libraries
{
    public class LibraryCreateForLocationHandler : IRequestHandler<LibraryCreateForLocationCmd, LibraryDTO>
    {

        private readonly ICrudRepository<Location,int> _repository;
        private readonly ICrudRepository<Library, int> _libraryRepository;
        private readonly ILogger<LibraryCreateForLocationHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;


        public LibraryCreateForLocationHandler(ICrudRepository<Location, int> repository,IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<LibraryCreateForLocationHandler> logger, ICrudRepository<Library, int> libraryRepository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _libraryRepository = libraryRepository;
        }

        public async Task<LibraryDTO> Handle(LibraryCreateForLocationCmd request, CancellationToken cancellationToken)
        {


            var location = await  _repository.GetAsync(StringHelper.ToInt(request.LocationId));

            var library = Library.Create(request.LibraryNumber, request.Name, request.Description,
                EnumHelper<LibraryType>.Parse(request.LibraryType), location);

            _logger.LogInformation("----- Creating Library - Location: {@library}", library);

            _libraryRepository.Add(library);

            var result =  await _libraryRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);

            var response = result ? library.ToDto() : LibraryDTO.Empty();

            return response;

        }
    }


    public class LibraryCreateForLocationIdentifiedCommandHandler : IdentifiedCommandHandler<LibraryCreateForLocationCmd, LibraryDTO>
    {
        public LibraryCreateForLocationIdentifiedCommandHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<LibraryCreateForLocationCmd, LibraryDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override LibraryDTO CreateResultForDuplicateRequest()
        {
            return LibraryDTO.Empty();
        }
    }
}