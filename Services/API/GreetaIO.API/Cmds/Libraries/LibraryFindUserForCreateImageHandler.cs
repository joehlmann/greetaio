﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.UserImages;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Libraries
{
    public class LibraryFindUserForCreateImageHandler : IRequestHandler<LibraryFindUserForCreateImageCmd,UserImageDTO>
    {

        private readonly ICrudRepository<UserImage,int> _repository;
        private readonly ICrudRepository<Library, int> _libraryRepository;
        private readonly ILogger<LibraryFindUserForCreateImageHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public LibraryFindUserForCreateImageHandler(ICrudRepository<UserImage, int> repository,
            IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<LibraryFindUserForCreateImageHandler> logger,
            IMapper mapper, ICrudRepository<Library, int> libraryRepository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper;
            _libraryRepository = libraryRepository;
        }

        public async Task<UserImageDTO> Handle(LibraryFindUserForCreateImageCmd cmd, CancellationToken cancellationToken)
        {
            _logger.LogInformation("UnknownSub Image tickId:{TimeId}  for LibraryId:{LibraryId} )",
                  cmd.TimeId, cmd.LibraryId);

            var userImage = UserImage.Create(cmd.ImageData.ToByte(),cmd.TimeId);

            var library =  await _libraryRepository.GetAsync(cmd.LibraryId);

            library.FindUnKnownUser(userImage);

            _repository.Add(userImage);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);


            var response = result ? _mapper.Map<UserImageDTO>(userImage) : UserImageDTO.Empty();

            return response;


        }
    }


    /// <summary>
    /// Use for Idempotency
    /// </summary>
    public class LibraryFindUserForCreateImageIdentifiedCmdHandler : IdentifiedCommandHandler<LibraryFindUserForCreateImageCmd, UserImageDTO>
    {
        public LibraryFindUserForCreateImageIdentifiedCmdHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<LibraryFindUserForCreateImageCmd, UserImageDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override UserImageDTO CreateResultForDuplicateRequest()
        {
            return UserImageDTO.Empty();
        }
    }
}
