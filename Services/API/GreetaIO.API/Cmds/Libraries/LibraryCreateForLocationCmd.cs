﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.Libraries
{
    public class LibraryCreateForLocationCmd :IRequest<LibraryDTO>
    {
        public string LocationId { get;  }

        public int LibraryNumber { get;  }

        public string Name { get;  }

        public string Description { get;  }

        public string LibraryType { get;  }

        


        public LibraryCreateForLocationCmd(string locationId, string name, string description, string libraryType,  int libraryNumber)
        {
            LocationId = locationId;
            Name = name;
            Description = description;
            LibraryType = libraryType;
            LibraryNumber = libraryNumber;
            
        }
    }

    
}
