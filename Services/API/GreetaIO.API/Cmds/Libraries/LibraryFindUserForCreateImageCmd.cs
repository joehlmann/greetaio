﻿using GreetaIO.API.Application.Behaviors.Interfaces;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.Infrastructure.Idempotency;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Libraries
{
    public class LibraryFindUserForCreateImageCmd :IRequest<UserImageDTO> ,ICommand
    {
        public int LibraryId { get; private set; }

        public string ImageData { get; private set; }

        public long TimeId { get; private set; }

        public LibraryFindUserForCreateImageCmd(string imageData,int libraryid, long timeId)
        {
            ImageData = imageData;
            LibraryId = libraryid;
            TimeId = timeId;
        }
    }

    


}
