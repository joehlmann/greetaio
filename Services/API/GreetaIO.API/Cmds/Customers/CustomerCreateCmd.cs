﻿using GreetaIO.API.Application.DTOs;
using GreetaIO.Infrastructure.Idempotency;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Customers
{
    public class CustomerCreateCmd :IRequest<CustomerDTO>
    {
        public string  CustomerNumber { get; private set; }

        public string CompanyName { get; private set; }


        public AddressDTO Address { get; private set; }


        public ContactDTO Contact { get; private set; }

        public CustomerCreateCmd(string clientNumber, string companyName,AddressDTO address,ContactDTO contact)
        {
            CustomerNumber = clientNumber;
            CompanyName = companyName;
            Address = address;
            Contact = contact;
        }


        public CustomerCreateCmd(string clientNumber, string companyName, string address1,
            string address2,
            string state,
            string suburb,
            string postCode,
            ContactDTO contact)
        {

            CustomerNumber = clientNumber;
            CompanyName = companyName;
            Address = new AddressDTO()
            {
                Address1 = address1,
                Address2 = address2,
                PostCode = postCode,
                State = state,
                Suburb = suburb
            };
            Contact = contact;
        }

        public CustomerCreateCmd(string clientNumber, string companyName, string address1,
            string address2,
            string state,
            string suburb,
            string postCode,
            string contactName,
            string contactPhone,
            string email)
        {

            CustomerNumber = clientNumber;
            CompanyName = companyName;
            Address = new AddressDTO()
            {
                Address1 = address1,
                Address2 = address2,
                PostCode = postCode,
                State = state,
                Suburb = suburb
            };
            Contact = new ContactDTO()
            {
                ContactName = contactName,
                Phone = contactPhone,
                Email = email
            };
        }
    }

    public class CustomerCreateIdentifiedCmdHandler : IdentifiedCommandHandler<CustomerCreateCmd, CustomerDTO>
    {
        public CustomerCreateIdentifiedCmdHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<CustomerCreateCmd, CustomerDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override CustomerDTO CreateResultForDuplicateRequest()
        {
            return new CustomerDTO();
        }
    }
}
