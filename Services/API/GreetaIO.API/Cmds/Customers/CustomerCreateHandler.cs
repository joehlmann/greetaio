﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Customers
{
    public class CustomerCreateHandler : IRequestHandler<CustomerCreateCmd,CustomerDTO>
    {

        private readonly ICrudRepository<Customer,int> _repository;
        private readonly ILogger<CustomerCreateHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public CustomerCreateHandler(ICrudRepository<Customer, int> repository,IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<CustomerCreateHandler> logger,
            IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<CustomerDTO> Handle(CustomerCreateCmd request, CancellationToken cancellationToken)
        {

            var customer = Customer.Create(request.CustomerNumber.ToInt(),
                request.CompanyName,
                request.Address.State, request.Address.Suburb, request.Address.Address1, request.Address.Address2,
                request.Address.PostCode,
                request.Contact.ContactName, request.Contact.Phone, request.Contact.Email);

            _logger.LogInformation("----- Creating Customer - Customer: {@customer}", customer);

            _repository.Add(customer);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);


            var response = result ? _mapper.Map<CustomerDTO>(customer) : new CustomerDTO();

            return response;

        }
    }
}