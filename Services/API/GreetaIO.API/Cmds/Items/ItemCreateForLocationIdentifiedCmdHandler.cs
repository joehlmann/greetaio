﻿using GreetaIO.API.Application.DTOs;
using GreetaIO.Infrastructure.Idempotency;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Items
{
    /// <summary>
    /// Use for Idempotency
    /// </summary>
    public class ItemCreateForLocationIdentifiedCmdHandler : IdentifiedCommandHandler<ItemCreateForLocationCmd, ItemDTO>
    {
        public ItemCreateForLocationIdentifiedCmdHandler(
            IMediator mediator,
            IRequestManager requestManager,
            ILogger<IdentifiedCommandHandler<ItemCreateForLocationCmd, ItemDTO>> logger)
            : base(mediator, requestManager, logger)
        {
        }

        protected override ItemDTO CreateResultForDuplicateRequest()
        {
            return new ItemDTO();
        }
    }
}
