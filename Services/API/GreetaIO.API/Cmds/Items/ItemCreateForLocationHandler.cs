﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Cmds.Items
{
    public class ItemCreateForLocationHandler : IRequestHandler<ItemCreateForLocationCmd,ItemDTO>
    {

        private readonly ICrudRepository<Location,int> _repository;
        private readonly ILogger<ItemCreateForLocationHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;


        public ItemCreateForLocationHandler(ICrudRepository<Location, int> repository,IMediator mediator,
            IIntegrationEventSvc integrationEventSvc,
            IIdentityService identityService,
            ILogger<ItemCreateForLocationHandler> logger, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper;
        }

        public async Task<ItemDTO> Handle(ItemCreateForLocationCmd cmd, CancellationToken cancellationToken)
        {
            _logger.LogInformation("----- Creating Item - Location: {@locationId}", cmd.LocationId);

            var location = await  _repository.GetAsync(cmd.LocationId.ToInt())
                .ConfigureAwait(false);


            var rewardPoints = RewardPoints.Create(cmd.RewardsType.ToEnum<RewardsType>(),cmd.PointsCost, cmd.PointsReward);

            var item = Item.Create(cmd.ItemNumber,cmd.ItemName,cmd.ItemDescription,cmd.ItemType.ToEnum<ItemType>(),rewardPoints,cmd.IsProduct);

            location.AddItem(item);

            _repository.Update(location);

            var result =  await _repository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken).ConfigureAwait(false);

            return _mapper.Map<ItemDTO>(item);

        }
    }
}