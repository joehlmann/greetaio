﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Cmds.Items
{
    public class ItemCreateForLocationCmd :IRequest<ItemDTO>
    {
        public string LocationId { get;  }

        public int ItemNumber { get;  }

        public string ItemName { get;  }

        public string ItemDescription { get;  }

        public string ItemType { get;  }

        public string RewardsType { get; }


        public int PointsCost { get; }
        public int PointsReward { get;  }

        public bool IsProduct { get;  }


        public ItemCreateForLocationCmd(string locationId, int itemNumber, string itemName, string itemDescription, string itemType, string rewardsType, int pointsCost, int pointsReward, bool isProduct)
        {
            ItemNumber = itemNumber;
            ItemName = itemName;
            ItemDescription = itemDescription;
            ItemType = itemType;
            RewardsType = rewardsType;
            PointsCost = pointsCost;
            PointsReward = pointsReward;
            IsProduct = isProduct;
            LocationId = locationId;
        }
    }
}
