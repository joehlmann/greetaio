﻿using MediatR;

namespace GreetaIO.API.Cmds
{
    public class IdentifiedIntCommand<T, R> : IRequest<R>
        where T : IRequest<R>
    {
        public T Command { get; }
        public int Id { get; }
        public IdentifiedIntCommand(T command, int id)
        {
            Command = command;
            Id = id;
        }
    }
}
