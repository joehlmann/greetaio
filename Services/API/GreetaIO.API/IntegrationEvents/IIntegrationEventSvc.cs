﻿using System;
using System.Threading.Tasks;
using EventBusAbstract.Events;

namespace GreetaIO.API.IntegrationEvents
{
    public interface IIntegrationEventSvc
    {
        Task PublishEventsThroughEventBusAsync(Guid transactionId);
        Task AddAndSaveEventAsync(IntEvent evt);
    }
}