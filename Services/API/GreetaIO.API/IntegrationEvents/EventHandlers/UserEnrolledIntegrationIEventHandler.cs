﻿using System;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.TokenContext;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.IntegrationEvents.EventHandlers
{
    public class UserEnrolledIntegrationIEventHandler : IIntegrationEventHandler<UserEnrolledIntEvent>, IDisposable
    {
        public ITokenContextService TokenContextService { get; }
        public ICorrelationContextAccessor CorrelationContextAccessor { get; }
        private readonly IMediator _mediator;
        private readonly ILogger<UserEnrolledIntegrationIEventHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;

        public UserEnrolledIntegrationIEventHandler(IMediator mediator,
            ILogger<UserEnrolledIntegrationIEventHandler> logger,
            ITokenContextService tokenContextService,
            ICorrelationContextAccessor correlationContextAccessor)
        {
            TokenContextService = tokenContextService;
            CorrelationContextAccessor = correlationContextAccessor;
            _mediator = mediator;
            _logger = logger;
        }


        public Task Handle(UserEnrolledIntEvent @event)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
