﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Events;
using GreetaIO.API.Infrastructure;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.TokenContext;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.IntegrationEvents.EventHandlers.PickupBooked
{
    public class
        PickupBookedIEventHandler : IIntegrationEventHandler<PickupBookedIntEvent>
            
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScope _scopeInstance;

        public PickupBookedIEventHandler(
            ILogger<PickupBookedIEventHandler> logger,
            ICorrelationContextAccessor correlationContext, IServiceProvider services)
        {
            _serviceProvider = services ?? throw new ArgumentNullException(nameof(services));
            _scopeInstance = _serviceProvider.CreateScope();

            _tokenContext = _scopeInstance.ServiceProvider.GetRequiredService<ITokenContextService>();
            _mediator = _scopeInstance.ServiceProvider.GetRequiredService<IMediator>();


            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _correlationContext = correlationContext ?? throw new ArgumentNullException(nameof(correlationContext));


        }

        public async Task Handle(PickupBookedIntEvent eventMsg)
        {


            _tokenContext.SetAccessToken(eventMsg.AuthToken);

            using (_logger.BeginScope(eventMsg.GenerateLoggingMetaData()))
            {
                var pickupBranchRequest = GenerateUpdatePickupBranchRequest(eventMsg);

                await ProcessUpdatePickupBranchRequest(pickupBranchRequest);

                _logger.LogInformation($"PickupBooked integration event has been handled ", eventMsg);
            }


        }

        public Task<UpdatePickupBranchResponse> GenerateUpdatePickupBranchRequest(PickupBookedIntEvent eventMsg)
        {
            return _mediator.Send(new UpdatePickupBranchCommand
            {
                Id = eventMsg.PickupId, Number = eventMsg.PickupNumber, Suburb = eventMsg.PickupSuburb,
                Postcode = eventMsg.PickupPostcode
            });
        }

        public async Task ProcessUpdatePickupBranchRequest(Task<UpdatePickupBranchResponse> pickupBranchResponseTask)
        {
            var pickupBranchResponse = await pickupBranchResponseTask;
            //if (pickupBranchResponse.PickupId)
            //{
            //    using (_logger.BeginScope(new Dictionary<string, object>
            //        {{"Errors", JsonConvert.SerializeObject(pickupBranchResponse.Errors)}}))
            //    {
            //        // TODO: Dispatch Update pickup branch failure event
            //        _logger.LogWarning(
            //            $"Pickup branch failed to be set for pickup number {pickupBranchResponse.PickupNumber}");
            //    }

            //}
            //else
            //{
            //    _logger.LogInformation(
            //        $"Pickup branch successfully set for pickup number {pickupBranchResponse.PickupNumber}");
            //}
        }

        public void Dispose()
        {
            _scopeInstance.Dispose();
        }

        public Dictionary<string, object> GenerateLoggingMetaData(IntEvent eventMsg)
        {
            var correlationId = eventMsg.CorrelationId;
            var correlationName = eventMsg.CorrelationName;

            //if (string.IsNullOrEmpty(correlationId))
            //{
            //    correlationId = Guid.NewGuid().ToString();
            //}

            correlationName = string.IsNullOrEmpty(correlationName) ? "RequestId" : correlationName;

            var loggingMetaData = new Dictionary<string, object>()
            {
                {correlationName, correlationId}
            };

            // Set the correlation data
            new CorrelationContextFactory().Create(correlationId.ToString(), correlationName);
            eventMsg.SetCorrelationDetails(correlationId, correlationName);
            return loggingMetaData;
        }

    }

    public class UpdatePickupBranchResponse 
    {
        public Guid PickupId { get; set; }
        public int PickupNumber { get; set; }
    }

    public class UpdatePickupBranchCommand : IRequest<UpdatePickupBranchResponse>
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }

    }
}
