﻿using System;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using GreetaIO.API.Infrastructure;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.TokenContext;
using MediatR;
using Microsoft.Extensions.Logging;
using Serilog.Context;

namespace GreetaIO.API.IntegrationEvents.EventHandlers
{
    public class UserImageAtLocationIntEventHandler : IIntegrationEventHandler<UserImageAtLocationIntEvent>
    {
        public ITokenContextService TokenContextService { get; }
        public ICorrelationContextAccessor CorrelationContextAccessor { get; }
        private readonly IMediator _mediator;
        private readonly ILogger<UserImageAtLocationIntEventHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;

        public UserImageAtLocationIntEventHandler(IMediator mediator,
            ILogger<UserImageAtLocationIntEventHandler> logger,
            ITokenContextService tokenContextService,
            ICorrelationContextAccessor correlationContextAccessor)
        {
            TokenContextService = tokenContextService;
            CorrelationContextAccessor = correlationContextAccessor;
            _correlationContext = correlationContextAccessor;
            _mediator = mediator;
            _logger = logger;
        }


        public Task Handle(UserImageAtLocationIntEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.GenerateLoggingMetaData()}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent}) ", @event.Id, Program.AppName, @event);
                

                return Task.CompletedTask;
            };
        }

       
    }
}
