﻿using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using GreetaIO.API.Infrastructure;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.TokenContext;
using MediatR;
using Microsoft.Extensions.Logging;
using Serilog.Context;

namespace GreetaIO.API.IntegrationEvents.EventHandlers
{
    public class ImageAddedToUserIntEventHandler : IIntegrationEventHandler<ImageAddedToUserIntEvent>
    {
        public ITokenContextService TokenContextService { get; }
        public ICorrelationContextAccessor CorrelationContextAccessor { get; }
        private readonly IMediator _mediator;
        private readonly ILogger<ImageAddedToUserIntEventHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;

        public ImageAddedToUserIntEventHandler(IMediator mediator,
            ILogger<ImageAddedToUserIntEventHandler> logger,
            ITokenContextService tokenContextService,
            ICorrelationContextAccessor correlationContextAccessor)
        {
            TokenContextService = tokenContextService;
            CorrelationContextAccessor = correlationContextAccessor;
            _mediator = mediator;
            _logger = logger;
        }


        public Task Handle(ImageAddedToUserIntEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.GenerateLoggingMetaData()}-{Program.AppName}"))
            {
                _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                //_logger.LogInformation(_correlationContext.CorrelationContext.CorrelationId);

                return Task.CompletedTask;
            }
            

        }

        
    }
}
