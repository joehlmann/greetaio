﻿using System;
using EventBusAbstract.Events;
using MediatR;
using Newtonsoft.Json;

namespace GreetaIO.API.IntegrationEvents.Events
{
    public class UserImageAtLocationIntEvent : IntEvent
    {
        
        public int LibraryId { get; private set; }

        public int LocationId { get; private set; }

        public string ImageData { get; private set; }


        public override string ToString()
        {
            return $"EventId:{Id} for UnknownImageAtLocation LocationId:{LocationId} Library:{LibraryId} ";
        }


        #region Constructors
        public UserImageAtLocationIntEvent(int libraryId, int locationId, string imageData)
        {
            LibraryId = libraryId;
            LocationId = locationId;
            ImageData = imageData;
        }


        [JsonConstructor]
        public UserImageAtLocationIntEvent(int libraryId, int locationId, string imageData, string correlationId, string clientName, string authToken)
        {
            LibraryId = libraryId;
            LocationId = locationId;
            ImageData = imageData;
            SetCorrelationDetails(correlationId, clientName);
            SetAuthToken(authToken);
        }
        #endregion


    }
}
