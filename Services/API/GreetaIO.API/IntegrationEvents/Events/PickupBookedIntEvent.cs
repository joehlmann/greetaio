﻿using System;
using EventBusAbstract.Events;
using GreetaIO.Utility.Helper;
using Newtonsoft.Json;

namespace GreetaIO.API.IntegrationEvents.Events
{
    public class PickupBookedIntEvent : IntEvent
    {
        public Guid PickupId { get; }
        public int PickupNumber { get; }
        public string PickupStatus { get; }
        public string PickupSuburb { get; }
        public string PickupPostcode { get; }
        public DateTimeOffset? PickupBookedDateTime { get; }
        public DateTime PickupCloseTime { get; }

        public PickupBookedIntEvent(Guid pickupId, int pickupNumber, string pickupStatus, string pickupSuburb, string pickupPostcode, DateTimeOffset? pickupBookedDateTime, DateTime pickupCloseTime)
        {
            PickupId = pickupId;
            PickupStatus = pickupStatus;
            PickupNumber = pickupNumber;
            PickupSuburb = pickupSuburb;
            PickupPostcode = pickupPostcode;
            PickupBookedDateTime = pickupBookedDateTime;
            PickupCloseTime = pickupCloseTime;
        }

        [JsonConstructor]
        public PickupBookedIntEvent(Guid pickupId, int pickupNumber, string pickupStatus, string pickupSuburb, string pickupPostcode, DateTimeOffset? pickupBookedDateTime, DateTime pickupCloseTime, string correlationId, string correlationName,string authToken)
        {
            PickupId = pickupId;
            PickupStatus = pickupStatus;
            PickupNumber = pickupNumber;
            PickupSuburb = pickupSuburb;
            PickupPostcode = pickupPostcode;
            PickupBookedDateTime = pickupBookedDateTime;
            PickupCloseTime = pickupCloseTime;
            SetCorrelationDetails(correlationId,correlationName);
            SetAuthToken(authToken);
        }

    }
}
