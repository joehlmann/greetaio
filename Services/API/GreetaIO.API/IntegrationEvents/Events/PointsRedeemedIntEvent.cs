﻿using System;
using EventBusAbstract.Events;
using GreetaIO.Utility.Helper;
using Newtonsoft.Json;

namespace GreetaIO.API.IntegrationEvents.Events
{
    public class PointsRedeemedIntEvent : IntEvent
    {


        public Guid UserId { get; private set; }

        public Guid LocationId { get; private set; }

        public int RedeemedPoints { get; private set; }


        public PointsRedeemedIntEvent(Guid userId, Guid locationId, int redeemedPoints)
        {
            UserId = userId;
            LocationId = locationId;
            RedeemedPoints = redeemedPoints;
        }


        [JsonConstructor]
        public PointsRedeemedIntEvent(Guid userId, Guid locationId, int redeemedPoints, string correlationId, string correlationName, string authToken)
        {
            UserId = userId;
            LocationId = locationId;
            RedeemedPoints = redeemedPoints;
            SetCorrelationDetails(correlationId, correlationName);
            SetAuthToken(authToken);
        }


    }
}
