﻿using EventBusAbstract.Events;
using MediatR;

namespace GreetaIO.API.IntegrationEvents.Events
{
    public class UserEnrolledIntEvent : IntEvent
    {
        public int LocationId { get; private set; }

        public int LibraryId { get; private set; }

        public int UserId { get; private set; }

        public UserEnrolledIntEvent(int userId, int libraryId, int locationId)
        {
            UserId = userId;
            LibraryId = libraryId;
            LocationId = locationId;
        }

        public override string ToString()
        {
            return $"UserId:{UserId} LibraryId:{LibraryId} LocationId:{LocationId}";
        }


    }
}
