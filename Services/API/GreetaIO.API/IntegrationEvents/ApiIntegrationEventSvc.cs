﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using CorrelationId;
using EventBusAbstract.Abstractions;
using EventBusAbstract.Events;
using GreetaIO.Infrastructure;
using GreetaIO.Infrastructure.TokenContext;
using GreetaIO.Utility.Helper;
using IntegrationEventLogEF;
using IntegrationEventLogEF.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.IntegrationEvents
{
    public class ApiIntegrationEventSvc : IIntegrationEventSvc
    {

        private readonly Func<DbConnection, IIntegrationEventLogService> _integrationEventLogServiceFactory;
        private readonly IEventBus _eventBus;
        private readonly ApiContext _apiContext;
        
        private readonly IIntegrationEventLogService _eventLogService;
        private readonly ILogger<ApiIntegrationEventSvc> _logger;
        private readonly ICorrelationContextAccessor _correlationContext;
        private readonly ITokenContextService _tokenContext;

        public ApiIntegrationEventSvc(IEventBus eventBus,
            ApiContext pickupContext,
            ICorrelationContextAccessor correlationContextAccessor,
            Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory,
            ILogger<ApiIntegrationEventSvc> logger)
        {
            _apiContext = pickupContext ?? throw new ArgumentNullException(nameof(pickupContext));
            _correlationContext = correlationContextAccessor ?? throw new ArgumentNullException(nameof(correlationContextAccessor));
            _integrationEventLogServiceFactory = integrationEventLogServiceFactory ?? throw new ArgumentNullException(nameof(integrationEventLogServiceFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _eventLogService = _integrationEventLogServiceFactory(_apiContext.Database.GetDbConnection());
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task PublishEventsThroughEventBusAsync(Guid transactionId)
        {
            var pendingLogEvents = await _eventLogService.RetrieveEventLogsPendingToPublishAsync(transactionId);

            foreach (var evt in pendingLogEvents)
            {
                //todo Clean
                SetCorrelationDetails(evt.IntegrationEvent);

                _logger.LogInformation("----- Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", evt.EventId, Program.AppName, evt.IntegrationEvent);

                try
                {
                    await _eventLogService.MarkEventAsInProgressAsync(evt.EventId);
                    _eventBus.Publish(evt.IntegrationEvent);
                    await _eventLogService.MarkEventAsPublishedAsync(evt.EventId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "ERROR publishing integration event: {IntegrationEventId} from {AppName}", evt.EventId, Program.AppName);

                    await _eventLogService.MarkEventAsFailedAsync(evt.EventId);
                }
            }
        }

        public async Task AddAndSaveEventAsync(IntEvent evt)
        {
            _logger.LogInformation("----- Enqueuing integration event {IntegrationEventId} to repository ({@IntegrationEvent})", evt.Id, evt);

            await _eventLogService.SaveEventAsync(evt, _apiContext.GetCurrentTransaction());
        }



        private void SetCorrelationDetails(IntEvent evt)
        {
            
            var correlationId = _correlationContext?.CorrelationContext?.CorrelationId;
            var correlationHeader = _correlationContext?.CorrelationContext?.Header;


            if (string.IsNullOrEmpty(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
            }

            correlationHeader = string.IsNullOrEmpty(correlationHeader) ? "request-id" : correlationHeader;
            evt.SetCorrelationDetails(correlationId, correlationHeader);
        }

    }
}
