﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;

namespace GreetaIO.API.Application.ApplicationServices
{
    public class AddressValidService : IAddressValidService
    {
        

        public async Task<Result<bool>> ValidAddressAsync(AddressDTO addressDTO)
        {

            var resultAdd1 = Result.FailureIf(string.IsNullOrEmpty(addressDTO.Address1), ErrorMsg.AddressLine1Invalid);


            var resultPost = Result.FailureIf(string.IsNullOrEmpty(addressDTO.PostCode), ErrorMsg.AddressPostCodeNull)
                .OnSuccessTry(() =>
                    Result.FailureIf(CheckPostCodeIsInvalid(addressDTO.PostCode), ErrorMsg.AddressPostCode));




            var resultState = Result.FailureIf(string.IsNullOrEmpty(addressDTO.State), ErrorMsg.AddressStateInvalidNull)
                .OnSuccessTry(() =>
                    Result.FailureIf(CheckStateIsInvalid(addressDTO.State), ErrorMsg.AddressStateInvalid));


            var resultSub = Result.FailureIf(string.IsNullOrEmpty(addressDTO.Suburb), ErrorMsg.AddressSubNull)
                .OnSuccessTry(() => Result.FailureIf(CheckSubIsInvalid(addressDTO.Suburb), ErrorMsg.AddressSub));
                

            var result =  Result.Combine(resultAdd1, resultPost, resultState, resultSub);

            if (result.IsSuccess)
                return Result.Success(true);

            return await Task.FromResult(Result.Failure<bool>(result.Error)).ConfigureAwait(false);

            
        }


        private bool CheckStateIsInvalid(string postCode)
        {
            StatesEnum res;

            if (EnumHelper<StatesEnum>.TryParse(postCode,out res))
            {
                return false;
            }

            return true;
        }

        private bool CheckSubIsInvalid(string sub)
        {
            return sub.Length <= 3;

        }

        private bool CheckPostCodeIsInvalid(string postCode)
        {
            if (postCode.IsInt())
            {
                return postCode.Length != 4;
            }

            return true;
        }

    }
}
