﻿using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.Infrastructure;
using GreetaIO.Utility.Helper;
using Microsoft.EntityFrameworkCore;

namespace GreetaIO.API.Application.ApplicationServices
{
    public class CustomerAccountValidService : ICustomerAccountValidService
    {
        private readonly ApiContext _context;

        public CustomerAccountValidService(ApiContext context)
        {
            _context = context;
        }

        

        public async Task<Result<bool>> ValidLocationForAccountNumAsync(string accountNum, string locationId)
        {
            if (!accountNum.IsInt() || !locationId.IsInt())
                return Result.Failure<bool>($"AccountNum With Id:{accountNum} Invalid Int Format or Location With Id:{locationId} Invalid Format");
            

            var result = await _context.Locations
                .Join(_context.Customers,
                    l => l.Customer.Id,
                    c => c.Id,
                    (l, c) => new {l, c})
                .AnyAsync(z => z.l.Id == locationId.ToInt() && z.c.CustomerNumber == accountNum.ToInt())
                .ConfigureAwait(false);
            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"AccountNum With Id:{accountNum} For Location With Id:{locationId} does not Exist");

        }

        public async Task<Result<bool>> ValidAccountNumAsync(string accountNum)
        {

            if (!accountNum.IsInt())
                return Result.Failure<bool>($"AccountNum With Id:{accountNum} Invalid Int Format ");

            var result = await _context.Customers
                .AnyAsync(c => c.CustomerNumber == accountNum.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"AccountNum With Id:{accountNum} does not Exist");
        }

      
    }
}
