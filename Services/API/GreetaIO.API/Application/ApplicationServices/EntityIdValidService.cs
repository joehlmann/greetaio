﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.Infrastructure;
using GreetaIO.Utility.Helper;
using Microsoft.EntityFrameworkCore;

namespace GreetaIO.API.Application.ApplicationServices
{
    public class EntityIdValidService : IEntityIdValidService
    {
        private readonly ApiContext _context;

        public EntityIdValidService(ApiContext context)
        {
            _context = context;
        }


        public async Task<Result<bool>> ValidUserIdAsync(string id)
        {

            if (!id.IsInt())
                return Result.Failure<bool>($"User With Id:{id} Invalid Int Format ");

            var result = await _context.Users
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"User With Id:{id} does not Exist");

            
        }

        public async Task<Result<bool>> ValidLocationIdAsync(string id)
        {

            if (!id.IsInt())
                return Result.Failure<bool>($"Location With Id:{id} Invalid Int Format ");

            var result = await _context.Locations
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Location With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidLibraryIdAsync(string id)
        {

            if (!id.IsInt())
                return Result.Failure<bool>($"Library With Id:{id} Invalid Int Format ");

            var result = await _context.Libraries
                .AnyAsync(u => u.Id == id.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Library With Id:{id} does not Exist");
        }

        public async Task<Result<bool>> ValidLibraryIdForLocationIdAsync(string libraryId, string locationId)
        {

            if (!libraryId.IsInt() || !locationId.IsInt())
                return Result.Failure<bool>($"libraryId With Id:{libraryId} Invalid Int Format or Location With Id:{locationId} Invalid Format");

            var result = await _context.Libraries
                .AnyAsync(u => u.Id == libraryId.ToInt() && u.Location.Id == locationId.ToInt())
                .ConfigureAwait(false);

            if (result)
                return Result.Success(true);

            return Result.Failure<bool>($"Location With Id:{locationId} for Library With Id:{libraryId} does not Exist");
        }
    }
}