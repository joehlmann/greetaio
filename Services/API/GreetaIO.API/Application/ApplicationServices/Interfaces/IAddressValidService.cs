﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.ApplicationServices.Interfaces
{
    public interface IAddressValidService
    {
        Task<Result<bool>> ValidAddressAsync(AddressDTO addressDTO);
    }
}