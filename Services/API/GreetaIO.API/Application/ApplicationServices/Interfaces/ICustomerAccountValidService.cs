﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace GreetaIO.API.Application.ApplicationServices.Interfaces
{
    public interface ICustomerAccountValidService
    {
        Task<Result<bool>> ValidAccountNumAsync(string accountNum);

        Task<Result<bool>> ValidLocationForAccountNumAsync(string accountNum, string locationId);
    }


}