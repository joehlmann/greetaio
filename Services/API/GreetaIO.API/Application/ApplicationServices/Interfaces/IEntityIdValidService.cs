﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace GreetaIO.API.Application.ApplicationServices.Interfaces
{
    public interface IEntityIdValidService
    {
       Task<Result<bool>> ValidUserIdAsync(string id);

       Task<Result<bool>> ValidLocationIdAsync(string id);

       Task<Result<bool>> ValidLibraryIdAsync(string id);

       Task<Result<bool>> ValidLibraryIdForLocationIdAsync(string libraryId, string locationId);
    }
}