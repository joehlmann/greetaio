﻿using System.Collections.Generic;

namespace GreetaIO.API.Application.DTOs
{
    public class CustomerDTO
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public AddressDTO Address { get; set; }

        public ContactDTO Contact { get; set; }

        public IEnumerable<LocationDTO> Locations { get; set; }

        
        public string CustomerType { get; set; }

        public static CustomerDTO Empty()
        {
            return new CustomerDTO()
            {
                CustomerType = DtoState.Empty()
            };
        }
    }
}
