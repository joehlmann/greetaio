﻿namespace GreetaIO.API.Application.DTOs
{
    public class UserImageDTO
    {
        public string UserImageId { get; set; }

        //public string UserId { get; set; }

        public string ImageData { get; set; }

        public string TargetId { get; set; }

        public string TransactionId { get; set; }

        public string UserImageType { get; set; }

        

        public string Url { get; set; }


        public static UserImageDTO Empty()
        {
            return new UserImageDTO()
            {
                UserImageType = DtoState.Empty()
            };
        }
    }
}
