﻿namespace GreetaIO.API.Application.DTOs
{
    public class ContactDTO
    {

        public string ContactName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }


        public static ContactDTO Empty()
        {
            return new ContactDTO()
            {
                ContactName = "",
                Email = "",
                Phone = ""
            };
        }

    }
}
