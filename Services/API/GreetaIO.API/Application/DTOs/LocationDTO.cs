﻿using System;
using System.Collections.Generic;

namespace GreetaIO.API.Application.DTOs
{
    public class LocationDTO
    {
        public int LocationId { get; set; }

        public string LocationName { get; set; }

        public AddressDTO Address { get; set; }

        public IEnumerable<ContactDTO> Contacts { get; set; }



        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public IEnumerable<TransactionDTO> Transactions { get; set; }

        public IEnumerable<String> TransactionStrings { get; set; }


        public string LocationType { get; set; }

        public static LocationDTO Empty()
        {
            return new LocationDTO()
            {
                LocationType = DtoState.Empty()
            };
        }


    }
}
