﻿namespace GreetaIO.API.Application.DTOs
{
    public class TransactionDTO
    {
        public string TransactionId { get; set; }

        public ItemDTO Item { get; set; }


        public UserDTO User { get; set; }

        public LocationDTO Location { get; set; }

        public int PointsReward { get; set; }
        public string TransactionType { get;  set; }

        public static TransactionDTO Empty()
        {
            return new TransactionDTO()
            {
                TransactionType = DtoState.Empty()
            };
        }

    }
}
