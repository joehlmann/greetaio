﻿namespace GreetaIO.API.Application.DTOs
{
    public class ItemDTO
    {
        public string ItemId { get; set; }

        public string ItemName { get; set; }


        public string ItemDescription { get; set; }

        public string ItemType { get; set; }

        public string RewardsType { get; set; }

        public int PointsCost { get; set; }

        public string PointsReward { get; set; }

        public bool IsProduct { get; set; }



        


        

        public static ItemDTO Empty()
        {
            return new ItemDTO()
            {
                ItemType = DtoState.Empty()
            };
        }

    }
}
