﻿using System.Collections.Generic;

namespace GreetaIO.API.Application.DTOs
{
    public class UserDTO
    {

        public string UserId { get; set; }

        public string AliasName { get;  set; }

        public ContactDTO ContactDTO { get;  set; }

        public int CurrentRewards { get;  set; }

        public string TargetId { get;  set; }

        public IList<UserImageDTO> UserImages { get; set; }

        public int UserImageCount { get; set; }

        public string UserType { get; set; }


        public static UserDTO Empty()
        {
            return new UserDTO()
            {
                UserType = DtoState.Empty()
            };
        }
    }
}
