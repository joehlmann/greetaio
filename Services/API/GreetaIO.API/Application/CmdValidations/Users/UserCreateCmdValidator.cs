﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.CmdValidations.ValueObjects;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Users;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Users
{
    public class UserCreateCmdValidator : AbstractValidator<UserCreateCmd>
    {
        private readonly IEntityIdValidService _entityIdValidService;
        private readonly IContactValidService _contactValidator;
        private Guid _clientId;
        private object _cmd;
        private string _error;

        public UserCreateCmdValidator(
            ILogger<UserCreateCmdValidator> logger,
            IEntityIdValidService idValidService,
                IContactValidService contactValidator)
        {
            _entityIdValidService = idValidService;
            _contactValidator = contactValidator;

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.LocationId).NotEmpty().WithMessage(ErrorMsg.LocationNull);
            RuleFor(command => command.LocationId).NotEmpty().MustAsync((x, cancellation) => BeValidLocationId(x)).WithMessage(ErrorMsg.LocationDoesNotExist);
            RuleFor(command => command.AliasName).NotEmpty().WithMessage(ErrorMsg.MustSupplyAlias);
            RuleFor(command => command.UserImages).NotEmpty().Must(BeValidByteArray).WithMessage(ErrorMsg.UserImageDoesNotExist);
            RuleFor(command => command.ContactDTO).SetValidator(new ContactValidator(_contactValidator));
            

            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private bool BeValidByteArray(IEnumerable<UserImageDTO> imageData)
        {

            var result = imageData.Select(x=>x.ImageData.IsByteArray());

            return ! result.Any(x=> x ==false);
        }
        

       
        private async Task<bool> BeValidLocationId(string id)
        {
            var result = await _entityIdValidService.ValidLocationIdAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return true;
        }



    }
}
