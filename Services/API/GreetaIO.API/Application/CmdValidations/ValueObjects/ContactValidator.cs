﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.CmdValidations.ValueObjects
{
    public class ContactValidator : AbstractValidator<ContactDTO>
    {
        private readonly IContactValidService _contactValidator;

        private string _contactError = "";


        public ContactValidator(IContactValidService contactValidator)
        {
            _contactValidator = contactValidator;


            RuleFor(command => command).NotEmpty().MustAsync((c, cancel) => BeValidContact(c)).WithMessage(c => string.Format("{0}", _contactError));

        }




        private async Task<bool> BeValidContact(ContactDTO contact)
        {
            var result = await _contactValidator.ValidContactAsync(contact);

            if (result.IsFailure)
            {
                _contactError = result.Error;
                return false;
            }

            return true;
        }
    }
}
