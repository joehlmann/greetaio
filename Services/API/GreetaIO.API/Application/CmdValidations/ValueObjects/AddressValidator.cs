﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.CmdValidations.ValueObjects
{
    public class AddressValidator : AbstractValidator<AddressDTO>
    {
        private IAddressValidService _addressValidService;
        private string _addressError="";

        public AddressValidator(IAddressValidService addressValidService)
        {
            _addressValidService = addressValidService;

            RuleFor(addressDto => addressDto).NotEmpty().MustAsync((a, cancel) => BeValidAddress(a)).WithMessage(c => string.Format("{0}", _addressError));

        }



        private async Task<bool> BeValidAddress(AddressDTO address)
        {
            var result = await _addressValidService.ValidAddressAsync(address);

            if (result.IsFailure)
            {
                _addressError = result.Error;
                return false;
            }

            return true;
        }


    }
}
