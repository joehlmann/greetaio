﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Cmds.UserImages;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.UserImages
{
    public class CreateUserImageCmdValidator : AbstractValidator<UserImageCreateCmd>
    {
        private readonly IEntityIdValidService _entityIdValidService;
        private Guid _clientId;
        private object _cmd;
        private string _error;

        public CreateUserImageCmdValidator(
            ILogger<CreateUserImageCmdValidator> logger,
            IEntityIdValidService idValidService)
        {
            _entityIdValidService = idValidService;

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.UserId).NotEmpty().WithMessage(ErrorMsg.MustSupplyUserId);
            RuleFor(command => command.UserId).NotEmpty().MustAsync((x, cancellation) => BeValidUserId(x)).WithMessage(ErrorMsg.UserDoesNotExist);
            RuleFor(command => command.ImageData).NotEmpty().WithMessage(ErrorMsg.MustSupplyImageData);
            RuleFor(command => command.ImageData).NotEmpty().Must(BeValidByteArray).WithMessage(ErrorMsg.NotValidImage);
            



            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private bool BeValidByteArray(string imageData)
        {

            var result = imageData.IsByteArray();

            return result;
        }
        

       
        private async Task<bool> BeValidUserId(string id)
        {
            var result = await _entityIdValidService.ValidUserIdAsync(id).ConfigureAwait(true);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return true;
        }



    }
}
