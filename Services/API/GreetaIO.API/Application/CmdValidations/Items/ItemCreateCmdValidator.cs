﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Cmds.Items;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Items
{
    public class ItemCreateCmdValidator : AbstractValidator<ItemCreateForLocationCmd>
    {
        private readonly IEntityIdValidService _entityIdValidService;
        private readonly ICustomerAccountValidService _accountService;
        

        private string _customerNumber;
        private object _cmd;
        private string _error="Invalid Location";


        public ItemCreateCmdValidator(ILogger<ItemCreateCmdValidator> logger, IEntityIdValidService entityIdValidService)
        {
            _entityIdValidService = entityIdValidService;
            
        
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.LocationId).NotEmpty().MustAsync((l, cancel) => BeValidLocation(l)).WithMessage(_error);
            RuleFor(command => command.ItemNumber).NotEmpty().GreaterThan(0).WithMessage(ErrorMsg.ItemMustHaveNumber);
            RuleFor(command => command.ItemName).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.ItemMustHaveName);
            RuleFor(command => command.ItemDescription).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.ItemMustHaveDescription);
            RuleFor(command => command.ItemType).NotEmpty().Must(ItemTypeValid).WithMessage(ErrorMsg.ItemTypeInvalid);
            RuleFor(command => command.RewardsType).NotEmpty().Must(RewardsTypeValid).WithMessage(ErrorMsg.RewardsTypeInvalid);




            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private async Task<bool> BeValidLocation(string s)
        {
            var result = await _entityIdValidService.ValidLocationIdAsync(s);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return true;
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }


        private bool RewardsTypeValid(string rewardsType)
        {
            RewardsType parseType;

            if (EnumHelper<RewardsType>.TryParse(rewardsType, out parseType))
                return true;

            return false;
        }

        private bool ItemTypeValid(string itemType)
        {
            ItemType parseType;

            if (EnumHelper<ItemType>.TryParse(itemType, out parseType))
                return true;

            return false;
        }


        private bool CustomerNumberCheck(string clientId)
        {
            _customerNumber = clientId;
            int id;
            return int.TryParse(clientId, out id);
            
        }


       

    }
}
