﻿namespace GreetaIO.API.Application.CmdValidations
{
    public static class ValidationMessages
    {
        public static class Query
        {
            public static class UserMsg
            {
                public static class Detail
                {
                    public static string MissingKeyData => "Search criteria is required";
                    public static string MissingProviderScheme => "A provider scheme must be specified";
                    public static string MissingProviderKey => "A provider key must be specified";
                }

                public static class RolesMsg
                {
                    public static class List
                    {
                        public static string MissingUserId => "The user id is required";
                    }
                }
            }

            public static class RoleMsg
            {
                public static class Detail
                {
                    public static string MissingRoleId => "The role Id is required";

                }
            }



        }
        private static string ConstRoleDoesNotExist => "The role does not exist.";

        public static class CommandMsg
        {

            public static class UserMsg
            {
                private static string ConstUserDoesNotExist => "The user does not exist.";
                public static class Update
                {
                    public static string InvalidPassword = "Please provide a valid password.";
                    public static string UserDoesNotExist = ConstUserDoesNotExist;
                }
                public static class Registration
                {
                    public static string InvalidEmail = "Please provide a valid email address.";
                    public static string InvalidPassword = "Please provide a valid password.";
                }
                public static class ClaimsMsg
                {
                    public static class Add
                    {
                        public static string UserDoesNotExist = ConstUserDoesNotExist;
                        public static string MissingType = "Please provide a Claim type.";
                        public static string MissingValue = "Please provide a value for the claim";
                    }
                    public static class Remove
                    {
                        public static string UserDoesNotExist = ConstUserDoesNotExist;
                        public static string ClaimDoesNotExist = "The claim does not exist";
                    }
                }

                public static class Roles
                {
                    public static class Assign
                    {
                        public static string RoleDoesNotExist = ConstRoleDoesNotExist;
                        public static string UserDoesNotExist = ConstUserDoesNotExist;
                    }
                }
                public static class LoginsMsg
                {
                    public static class Add
                    {
                        public static string UserDoesNotExist = ConstUserDoesNotExist;
                        public static string LoginAlreadyExistForProvider = "A login already exists for this provider.";
                        public static string ProviderKeyMissing = "Please provide a key for the provider";
                        public static string ProviderNameMissing = "Please provide a name for the provider";
                        public static string ProviderDisplayNameMissing = "Please provide a display name for the provider";
                    }
                    public static class Remove
                    {
                        public static string LoginDoesNotExist = "The login does not exist";
                    }
                }

                public static class PrincipalsMsg
                {
                    public static class Create
                    {
                        public static string UserDoesNotExist = ConstUserDoesNotExist;

                    }

                }
            }

            public static class RoleMsg
            {
                private static string ConstInvalidName => "Please provide a role name longer than {0} characters.";
                private static string ConstDuplicateName => "The role name already exists, please enter a different name.";

                public static class Create
                {
                    public static string InvalidName = ConstInvalidName;
                    public static string DuplicateName = ConstDuplicateName;
                }
                public static class Delete
                {
                    public static string MissingId = "The Id of the role is required.";
                }
                public static class Update
                {
                    public static string RoleDoesNotExist = ConstRoleDoesNotExist;
                    public static string InvalidName = ConstInvalidName;
                    public static string DuplicateName = ConstDuplicateName;
                }


            }

            public static class ClientMsg
            {
                private static string ConstInvalidName => "Please provide a client name longer than {0} characters.";
                private static string ConstDuplicateName => "The client name already exists, please enter a different name.";
                private static string ConstClientDoesNotExist => "The client does not exist.";

                public static class Create
                {
                    public static string InvalidName = ConstInvalidName;
                    public static string DuplicateName = ConstDuplicateName;
                    public static string InvalidClientType = "Please provide a valid client type.";
                }
                public static class Delete
                {
                    public static string MissingId = "The id of the client is required.";
                }

                public static class Detail
                {
                    public static string IdOfClientMissing = "Please provide an id of a client.";
                }

                public static class SecretsMsg
                {
                    public static class Generate
                    {
                        public static string MissingIdOfClient => "The id of the client is required";
                        public static string ClientDoesNotExist => ConstClientDoesNotExist;
                    }
                }
            }
        }
    }
}
