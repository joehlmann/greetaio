﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Cmds.Locations;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Locations
{
    public class LocationUserImageAtLocationCmdValidator : AbstractValidator<LocationUserImageAtLocationCmd>
    {
        private readonly ICustomerAccountValidService _accountService;
        private int _customerNum;
        private int _locationId;
        private string _error;
        private object _cmd;

        public LocationUserImageAtLocationCmdValidator(ILogger<LocationUserImageAtLocationCmd> logger, ICustomerAccountValidService accountService)
        {

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.LocationId).NotEmpty().Must(CheckLocationId);
            RuleFor(command => command.ImageData).NotEmpty().Must(ContainImageData)
                .WithMessage($"Invalid Image No Data").Must(SizeImageData).WithMessage($"Image to be > 50mb");
            
            

            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }

        private bool CheckLocationId(int locationId)
        {
            _locationId = locationId;
            return true;
        }
        
        //Todo Fix
        private bool ContainImageData(string imageBytes)
        {

            return imageBytes.Length > 0;
        }

        //todo fix
        private bool SizeImageData(string imageBytes)
        {
            return imageBytes.Length < (1024 * 1024 * 50); // 50 MB);
        }



        private async Task<bool> BeValidCustomerAccountCode(string customerCode)
        {

            var result = await _accountService.ValidAccountNumAsync(customerCode);

            if (result.IsFailure)
            {
                _error = result.Error;
                return result.IsFailure;
            }

            return result.IsSuccess;
        }

    }
}
