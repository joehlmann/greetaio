﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.CmdValidations.ValueObjects;
using GreetaIO.API.Cmds.Locations;
using GreetaIO.TheDomain.Exceptions;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Locations
{
    public class LocationCreateForCustomerCmdValidator : AbstractValidator<LocationCreateForCustomerCmd>
    {
        private readonly ICustomerAccountValidService _accountService;
        private readonly IAddressValidService _addressValidator;
        private readonly IContactValidService _contactValidator;


        private string _customerNumber;
        private string _error="Invalid AccountNum";

        private object _cmd;

        public LocationCreateForCustomerCmdValidator(ILogger<LocationCreateForCustomerCmdValidator> logger,
            ICustomerAccountValidService accountService,
            IAddressValidService addressValidator,
            IContactValidService contactValidator)
        {
            _accountService = accountService;
            _addressValidator = addressValidator;
            _contactValidator = contactValidator;


            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.CustomerId).NotEmpty().Must(CheckCustomerNumber).WithMessage($"{ErrorMsg.CustomerInvalidNumberFormat} {_customerNumber}");
            RuleFor(command => command.CustomerId).NotEmpty().MustAsync((l,cancel)=> BeValidCustomerAccountCode(l)).WithMessage(_error);
            RuleFor(command => command.LocationName).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.LocationNameNotValid);
            RuleFor(command => command.Address).SetValidator(new AddressValidator(_addressValidator));
            RuleFor(command => command.Contact).SetValidator(new ContactValidator(_contactValidator));




            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }


        private bool CheckCustomerNumber(string clientId)
        {
            _customerNumber = clientId;
            int id;
            return int.TryParse(clientId, out id);
            
        }
        

        

        private async  Task<bool> BeValidCustomerAccountCode(string customerCode)
        {
            
            var  result = await _accountService.ValidAccountNumAsync(customerCode);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return true;
        }

    }
}
