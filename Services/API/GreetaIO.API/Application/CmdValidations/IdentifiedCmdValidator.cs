﻿using FluentValidation;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds;
using GreetaIO.API.Cmds.Locations;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations
{
    public class IdentifiedCmdValidator : AbstractValidator<IdentifiedCommand<LocationUserImageAtLocationCmd, UserDTO>>
    {
        private object _cmd;
        public IdentifiedCmdValidator(ILogger<IdentifiedCmdValidator> logger)
        {
            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.Id).NotEmpty();

            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }

        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
    }
}
