﻿using System.Threading.Tasks;
using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Cmds.Libraries;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Librarys
{
    public class LibraryCreateForLocationCmdValidator : AbstractValidator<LibraryCreateForLocationCmd>
    {
        private readonly IEntityIdValidService _entityIdValidService;
        private object _cmd;
        private string _error = "Invalid Location";
        


        public LibraryCreateForLocationCmdValidator(ILogger<LibraryCreateForLocationCmdValidator> logger, IEntityIdValidService entityIdValidService)
        {
            _entityIdValidService = entityIdValidService;

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.LocationId).NotEmpty().MustAsync((l, cancel) => BeValidLocation(l)).WithMessage(_error);
            RuleFor(command => command.LibraryNumber).NotEmpty().GreaterThan(0).WithMessage(ErrorMsg.LibMustHaveNumber);
            RuleFor(command => command.Name).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.LibMustHaveName);
            RuleFor(command => command.Description).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.libMustHaveDescription);
            RuleFor(command => command.LibraryType).NotEmpty().Must(LibraryTypeValid).WithMessage(ErrorMsg.LibTypeInvalid);



            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
        private bool LibraryTypeValid(string rewardsType)
        {
            LibraryType parseType;

            if (EnumHelper<LibraryType>.TryParse(rewardsType, out parseType))
                return true;

            return false;
        }

        private async Task<bool> BeValidLocation(string s)
        {
            var result = await _entityIdValidService.ValidLocationIdAsync(s);

            if (result.IsFailure)
            {
                _error = result.Error;
                return false;
            }

            return true;
        }

        


       

    }
}
