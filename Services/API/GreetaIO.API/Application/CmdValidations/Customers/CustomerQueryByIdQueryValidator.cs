﻿using FluentValidation;
using GreetaIO.API.Application.CmdQueries.Customers;
using GreetaIO.TheDomain.Exceptions;

namespace GreetaIO.API.Application.CmdValidations.Customers
{
    public class CustomerQueryByIdQueryValidator : AbstractValidator<CustomerByIdQuery>
    {


        private object _cmd;

        public CustomerQueryByIdQueryValidator()
        {

            RuleFor(c => c).Must(GetCmd);
            RuleFor(c => c.CustomerId).GreaterThan(0).WithMessage(ErrorMsg.CustomerInvalidNumberFormat);
        }


        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }
        
    }
}
