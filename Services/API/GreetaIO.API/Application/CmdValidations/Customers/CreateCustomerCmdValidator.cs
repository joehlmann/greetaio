﻿using FluentValidation;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.CmdValidations.ValueObjects;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.TheDomain.Exceptions;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.CmdValidations.Customers
{
    public class CreateCustomerCmdValidator : AbstractValidator<CustomerCreateCmd>
    {
        private readonly ICustomerAccountValidService _accountService;
        private readonly IContactValidService _contactValidator;
        private readonly IAddressValidService _addressValidator;

        private string _customerNumber;
        private object _cmd;
        private string _addressError = "";
        private string _contactError = "";


        public CreateCustomerCmdValidator(ILogger<CreateCustomerCmdValidator> logger, ICustomerAccountValidService accountService, IContactValidService contactValidator, IAddressValidService addressValidator)
        {
            _accountService = accountService;
            _contactValidator = contactValidator;
            _addressValidator = addressValidator;

            RuleFor(c => c).Must(GetCmd);
            RuleFor(command => command.CustomerNumber).NotEmpty().Must(CheckCustomerNumber).WithMessage(c=>  string.Format("CustomerNumber Error:{0}", _customerNumber));
            RuleFor(command => command.CompanyName).NotEmpty().MinimumLength(3).WithMessage(ErrorMsg.CustomerCompanyNameNotLongEnough);

            RuleFor(command => command.Address).SetValidator(new AddressValidator(_addressValidator));
            RuleFor(command => command.Contact).SetValidator(new ContactValidator(_contactValidator));

            

            logger.LogInformation("----- INSTANCE CREATED - {ClassName}", GetType().Name);
        }
        private bool GetCmd(object cmd)
        {
            _cmd = cmd;

            return true;
        }


        private bool CheckCustomerNumber(string clientId)
        {
            _customerNumber = clientId;
            int id;
            return int.TryParse(clientId, out id);
            
        }


       

    }
}
