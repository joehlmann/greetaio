﻿using GreetaIO.Infrastructure.Infrastructure.DomainServices;

namespace GreetaIO.API.Application.Settings
{
    public class VrSettingsConfig : IVrSettingsConfig
    {
        public string AuthName { get; set; }
        public int NumMatches { get; set; }
        public float MatchThreshold { get; set; }
    }
}
