﻿namespace GreetaIO.API.Application.Settings
{
    public class ConnectionStringConfig
    {

        public string ConnectionString { get; set; }

        public string ConnectionStringEvents { get; set; }

    }
}
