﻿namespace GreetaIO.API.Application.Settings
{
    public static class ConfigSection
    {
        public static string ConnectionStrings = "ConnectionStrings";
        public static string EventBusConfig = "EventBusConfig";
        public static string CacheConfig = "CacheConfig";
        public static string AzureEventBusConfig = "AzureEventBusConfig";
        public static string VrSettingsConfig = "VrSettingsConfig";
    }
}
