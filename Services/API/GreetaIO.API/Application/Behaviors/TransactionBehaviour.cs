﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using GreetaIO.API.Application.Behaviors.Interfaces;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace GreetaIO.API.Application.Behaviors
{
    public class TransactionBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<TransactionBehaviour<TRequest, TResponse>> _logger;
        private readonly ApiContext _dbContext;
        private readonly IIntegrationEventSvc _greetaIntegrationEventSvc;

        public TransactionBehaviour(ApiContext dbContext,
            IIntegrationEventSvc greetaIntegrationEventSrvc,
            ILogger<TransactionBehaviour<TRequest, TResponse>> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentException(nameof(ApiContext));
            _greetaIntegrationEventSvc = greetaIntegrationEventSrvc ?? throw new ArgumentException(nameof(greetaIntegrationEventSrvc));
            _logger = logger ?? throw new ArgumentException(nameof(ILogger));
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            //exclude queries in Transaction PipeLine
            if (request is IQuery)
            {
                return await next();
            }
            
            var response = default(TResponse);
            var typeName = request.GetGenericTypeName();

            try
            {
                if (_dbContext.HasActiveTransaction)
                {
                    return await next();
                }

                var strategy = _dbContext.Database.CreateExecutionStrategy();

                await strategy.ExecuteAsync(async () =>
                {
                    Guid transactionId;

                    using (var transaction = await _dbContext.BeginTransactionAsync())
                    using (LogContext.PushProperty("TransactionContext", transaction.TransactionId))
                    {
                        transactionId = transaction.TransactionId;
                        _logger.LogInformation("----- Begin transaction {TransactionId} for {CommandName} ({@Command})", transaction.TransactionId, typeName, request);

                        response = await next();

                        _logger.LogInformation("----- Commit transaction {TransactionId} for {CommandName}", transaction.TransactionId, typeName);

                        await _dbContext.CommitTransactionAsync(transaction).ConfigureAwait(false);

                        transactionId = transaction.TransactionId;
                    }

                    await _greetaIntegrationEventSvc.PublishEventsThroughEventBusAsync(transactionId).ConfigureAwait(false);

                    _logger.LogInformation("----- Publish Integration Event from transaction {TransactionId} for {CommandName}", transactionId, typeName);
                }).ConfigureAwait(false);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR Handling transaction for {CommandName} ({@Command})", typeName, request);

                throw;
            }
        }
    }
}
