﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EventBusAbstract.Extensions;
using FluentValidation;
using FluentValidation.Results;
using GreetaIO.TheDomain.Exceptions;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.Application.Behaviors
{
    public class ValidatorBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<ValidatorBehavior<TRequest, TResponse>> _logger;
        private readonly IValidator<TRequest>[] _validators;
        


        public ValidatorBehavior(IValidator<TRequest>[] validators, ILogger<ValidatorBehavior<TRequest, TResponse>> logger)
        {
            _validators = validators;
            _logger = logger;
        }

       

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var typeName = request.GetGenericTypeName();


            _logger.LogInformation("----- Validating command {CommandType}", typeName);

            var failures = _validators
                .Select(v => v.Validate(request))
                .SelectMany(result => result.Errors)
                .Where(error => error != null)
                .ToList();

            var failErrors = failures.Select(x => new {prop = x.PropertyName, msg = x.ErrorMessage.Split(',')})
                .ToList();
                
            var valres = new List<ValidationFailure>();

                foreach (var properror in failErrors)
                {
                    var prop = properror.prop;
                    foreach (var error in properror.msg) valres.Add(new ValidationFailure(prop,error));
                }
            
            var v = new ValidationResult(valres);

            
            

            if (failures.Any())
            {
                _logger.LogWarning("Validation errors - {CommandType} - Command: {@Command} - Errors: {@ValidationErrors}", typeName, request, v);

                throw new GreetaDomainException(
                    $"Command Validation Errors for type {typeof(TRequest).Name}", new ValidationException("Validation exception", valres));
                
            }

            return await next();
        }
    }
}