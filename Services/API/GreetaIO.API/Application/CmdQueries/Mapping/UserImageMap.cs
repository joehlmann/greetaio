﻿using Dapper.FluentMap.Mapping;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.CmdQueries.Mapping
{
    public class UserImageMap : EntityMap<UserImageDTO>
    {

        public UserImageMap()
        {

            Map(p => p.TargetId)
                .Ignore();

        }

    }
}
