﻿using Dapper.FluentMap.Mapping;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.CmdQueries.Mapping
{
    public class UserMap : EntityMap<UserDTO>
    {

        public UserMap()
        {
            Map(p => p.UserId)
                .ToColumn("UserId",false);

            Map(p => p.UserType)
                .ToColumn("UserType", false);

            
        }

    }
}
