﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.Infrastructure;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Customers
{
    public class CustomerByIdQueryHandler : IRequestHandler<CustomerByIdQuery, CustomerDTO>
    {
        private readonly ICrudRepository<Customer, int> _repository;
        private readonly IMapper _mapper;
        private readonly ApiContext _context;

        public CustomerByIdQueryHandler(ICrudRepository<Customer,int> repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentException(nameof(repository));
            _mapper = mapper;
        }


        // TODO use Dapper CQRS
        public async Task<CustomerDTO> Handle(CustomerByIdQuery request, CancellationToken cancellationToken)
        {
            
            var result = await _repository.GetAsync(request.CustomerId).ConfigureAwait(true);

            var dtoResult = _mapper.Map<CustomerDTO>(result);

            return dtoResult;
        }
    }

   
}
