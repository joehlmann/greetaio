﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Customers
{
    public class CustomerByIdQuery : IRequest<CustomerDTO>
    {
        public int CustomerId { get; }


        public CustomerByIdQuery(int customerId)
        {
            CustomerId = customerId;
        }

    }
}
