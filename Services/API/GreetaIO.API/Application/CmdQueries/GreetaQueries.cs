﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Dapper.FluentMap;
using GreetaIO.API.Application.CmdQueries.Mapping;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Pickups;
using GreetaIO.TheDomain.Enums;
using Serilog;

namespace GreetaIO.API.Application.CmdQueries
{
    public class GreetaQueries : IGreetaQueries
    {

        private string _connectionString = string.Empty;



        public GreetaQueries(string connectionString)
        {
            _connectionString = connectionString;
            //FluentMapper.Initialize(config =>
            //{
            //    config.AddMap(new UserMap());
            //    config.AddMap(new UserImageMap());
            //});
        }

        public async Task<LibraryDTO> GetLibraryForLocationAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryFirstOrDefaultAsync(
                    @"select [LibraryId], [RowVersion], [CurrentUserId], [CreateDate], [LastModified], [LibraryNumber], [Name], [Description], [LibraryType], [LocationId]
                          from dbo.Libraries l
                          where l.LocationId = @id "
                    , new { id }
                ).ConfigureAwait(false);


                var resultDto = result != null ? MapLibraryDto(result) : LibraryDTO.Empty();

                return resultDto;
            }
        }

        private LibraryDTO MapLibraryDto(dynamic result)
        {
            var dto = new LibraryDTO()
            {
                LibraryId = result.LibraryId,
                LibraryNumber = result.LibraryNumber,
                Name = result.Name,
                Description = result.Description,
                LibraryType = result.LibraryType,
                LocationId = result.LocationId

            };


            return dto;
        }


        public async Task<UserDTO> GetUserAsync(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var result = await connection.QueryFirstOrDefaultAsync(
                        @"select cast(u.UserId as varchar(50)) as userId,
                            u.AliasName,
                            u.Contact_Phone,
                            u.Contact_Email,
                            u.Contact_ContactName,
                            u.UserType,
                            u.CurrentRewards,
                           cast(u.TargetId as varchar(50)) as TargetId,
                            count(ui.UserImageId) as UserImageCount
                        from dbo.Users u 
                        inner join dbo.UserImages ui on ui.UserId = u.UserId
                        where u.UserId = @id
                        group by u.UserId,
                                 u.AliasName,
                                 u.Contact_Phone,
                                 u.Contact_Email,
                                 u.Contact_ContactName,
                                 u.UserType,
                                 u.CurrentRewards,
                                 u.TargetId"
                        , new { id }
                    ).ConfigureAwait(false);

                    
                    var resultDto = result != null ? MapUserDto(result) : UserDTO.Empty();

                    return resultDto;
                }

            }
            catch (Exception e)
            {
               Log.Information(e.Message);
            }

            return  UserDTO.Empty();
            
        }

        

        public async Task<UserDTO> GetUserWithUserImagesAsync(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    var result = await connection.QueryAsync<dynamic>(
                        @"select cast(u.UserId as varchar(50)) as userId,
                                    u.AliasName,
                                    u.Phone,
                                    cast (u.UserStatus as varchar(50)) as UserStatus,
                                    u.CurrentRewards,
                                    cast(u.TargetId as varchar(50)) as TargetId,
                                    cast(ui.UserImageId as varchar(50)) as UserImageId,	
                                    baze64 as ImageData,	                                
                                    ui.TransactionId,
                                    ui.UserImageType,
                                    ui.Url
                              from dbo.Users u 
                              left join dbo.UserImages ui on ui.UserId = u.UserId
                              cross apply (select ui.ImageData as '*' for xml path('')) T (baze64)
                              where u.UserId= @id
                          "
                        , new { id }
                    ).ConfigureAwait(false);

                    return MapUserImagesDto(result);
                }

            }
            catch (Exception e)
            {
                Log.Information(e.Message);
            }

            return new UserDTO();

        }


        private UserDTO MapUserDto(dynamic result)
        {
            var user = new UserDTO()
            {
                UserId = result.userId,
                AliasName = result.AliasName,
                ContactDTO = new ContactDTO { ContactName = result.Contact_ContactName, Email = result.Contact_Email, Phone = result.Contact_Phone },
                UserType = result.UserType,
                CurrentRewards = result.CurrentRewards,
                TargetId = result.TargetId,
                UserImageCount = (int)result.UserImageCount,
                UserImages = new List<UserImageDTO>()
            };

            
            return user;
        }

        private UserDTO MapUserImagesDto(dynamic result)
        {
            var user = new UserDTO()
            {
                UserId = result[0].userId,
                AliasName = result[0].AliasName,
                ContactDTO = new  ContactDTO {ContactName = result[0].Contact_ContactName,Email = result[0].Contact_Email, Phone = result[0].Contact_Phone},
                UserType = result[0].UserType,
                CurrentRewards = result[0].CurrentRewards,
                TargetId = result[0].TargetId,
                UserImages = new List<UserImageDTO>()
            };

            //user = MapUserDtoEnum(user);

            foreach (dynamic item in result)
            {
                var userImage = new UserImageDTO()
                {
                    UserImageId = item.UserImageId,
                    ImageData = item.ImageData,
                    TransactionId = item.TransactionId,
                    UserImageType = item.UserImageType,
                    Url = item.Url
                };
                user.UserImages.Add(userImage);

            }

            return user;
        }


        public Task<IEnumerable<PickupItemDTO>> GetPickupItems(int pickupNumber)
        {
            throw new NotImplementedException();
        }



        private UserDTO MapUserDtoEnum(UserDTO result)
        {
            //var enumval = (UserType) int.Parse(result.UserType);

            result.UserType = Enum.GetName(typeof(UserType), result.UserType);
            

            return result;
        }

        



        public async Task<IEnumerable<UserTransactionSummaryDTO>> GetTransactionsForUserAsync(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CustomerDTO>> GetCustomersAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<CustomerDTO> GetCustomerByIdAsync(int customerId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<UserImageDTO>> GetUserImagesAsync()
        {

            return await  new Task<IEnumerable<UserImageDTO>>(() => new List<UserImageDTO>());


        }

       
    }
}
