﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Items
{
    public class ItemByIdQuery : IRequest<ItemDTO>
    {
        public ItemByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }

    }
}
