﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Items
{
    public class ItemByIdQueryHandler : IRequestHandler<ItemByIdQuery, ItemDTO>
    {
        private readonly ICrudRepository<Item,int> _repository;
        private readonly IMapper _mapper;
        

        public ItemByIdQueryHandler(ICrudRepository<Item, int> repository, IMapper mapper)
        {
           _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper;
        }


       

        public async Task<ItemDTO> Handle(ItemByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id).ConfigureAwait(true);
                

            var dtoResult = _mapper.Map<ItemDTO>(result);


            return dtoResult;

        }
    }

    

    

    
}
