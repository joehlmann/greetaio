﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Transactions
{
    public class TransactionByIdQueryHandler : IRequestHandler<TransactionByIdQuery, TransactionDTO>
    {
        private readonly ICrudRepository<Transaction, int> _repository;
        private readonly IMapper _mapper;
        

        public TransactionByIdQueryHandler(ICrudRepository<Transaction, int> repository, IMapper mapper)
        {
           _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper;
        }


       

        public async Task<TransactionDTO> Handle(TransactionByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id).ConfigureAwait(true);
                

            var dtoResult = _mapper.Map<TransactionDTO>(result);


            return dtoResult;

        }
    }

    

    

    
}
