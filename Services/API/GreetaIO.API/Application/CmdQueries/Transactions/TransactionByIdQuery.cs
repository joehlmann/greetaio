﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Transactions
{
    public class TransactionByIdQuery :  IRequest<TransactionDTO>
    {
        public TransactionByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
    }
}
