﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Librarys
{
    public class LibraryByIdQuery :  IRequest<LibraryDTO>
    {
        
        public LibraryByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
    }
}
