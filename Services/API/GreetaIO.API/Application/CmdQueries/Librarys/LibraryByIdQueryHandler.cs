﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Librarys
{
    public class LibraryByIdQueryHandler : IRequestHandler<LibraryByIdQuery, LibraryDTO>
    {
        private readonly ICrudRepository<Library, int> _repository;

        public LibraryByIdQueryHandler(ICrudRepository<Library,int> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }


        // TODO use Dapper CQRS
        public async Task<LibraryDTO> Handle(LibraryByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id).ConfigureAwait(true);

            var dtoResult = result.ToDto();

            return dtoResult;
        }
    }

    
}
