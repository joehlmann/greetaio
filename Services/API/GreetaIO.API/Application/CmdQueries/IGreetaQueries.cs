﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;

namespace GreetaIO.API.Application.CmdQueries
{
    public interface IGreetaQueries
    {
        Task<UserDTO> GetUserAsync(int id);

        Task<UserDTO> GetUserWithUserImagesAsync(int id);

        Task<IEnumerable<UserTransactionSummaryDTO>> GetTransactionsForUserAsync(int userId);


        Task<IEnumerable<CustomerDTO>> GetCustomersAsync();



        Task<CustomerDTO> GetCustomerByIdAsync(int customerId);

        Task<IEnumerable<UserImageDTO>> GetUserImagesAsync();

        Task<LibraryDTO> GetLibraryForLocationAsync(int id);


    }

    public class UserTransactionSummaryDTO
    {
    }
}