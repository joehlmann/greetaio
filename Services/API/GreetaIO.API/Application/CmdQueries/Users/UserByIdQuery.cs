﻿using GreetaIO.API.Application.Behaviors.Interfaces;
using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Users
{
    public class UserByIdQuery : IRequest<UserDTO>, IQuery
    {


        public UserByIdQuery()
        {
            
        }

        public UserByIdQuery(int id, bool userImagesIncluded=false)
        {
            Id = id;
            UserImagesIncluded = userImagesIncluded;
        }

        public int Id { get; private set; }

        public bool UserImagesIncluded { get; private set; }
    }
}
