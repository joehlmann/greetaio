﻿using GreetaIO.API.Application.Behaviors.Interfaces;
using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Users
{
    public class UserWithImagesByIdQuery : IRequest<UserDTO> ,IQuery
    {


        public UserWithImagesByIdQuery()
        {
            
        }

        public UserWithImagesByIdQuery(int id, bool userImagesIncluded=false)
        {
            Id = id;
            UserImagesIncluded = userImagesIncluded;
        }

        public int Id { get; private set; }

        public bool UserImagesIncluded { get; private set; }
    }
}
