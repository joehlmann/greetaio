﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.Infrastructure;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Users
{
    public class UserWithImagesByIdQueryHandler : IRequestHandler<UserWithImagesByIdQuery, UserDTO>
    {
        
        private readonly ApiContext _context;
        private readonly IMapper _mapper;
        private readonly IGreetaQueries _greetaQueries;

        public UserWithImagesByIdQueryHandler(ApiContext context,IMapper mapper,IGreetaQueries greetaQueries)
        {
            _mapper = mapper;
            _greetaQueries = greetaQueries;
            _context = context ?? throw new ArgumentNullException(nameof(context));
            
        }


        public async Task<UserDTO> Handle(UserWithImagesByIdQuery request, CancellationToken cancellationToken)
        {
            var userResultImages = await _greetaQueries.GetUserWithUserImagesAsync(request.Id).ConfigureAwait(false);

            return userResultImages;
        }



        //public async Task<ApiResponse<UserDTO>> Handle2(UserByIdQuery request, CancellationToken cancellationToken)
        //{
        //    Domain.Entities.User userResult;
        //    ApiResponse<UserDTO> result;
        //    if (request.UserImagesIncluded)
        //    {
        //        userResult = await _context.Users
        //            .Include(u => u.UserImages)
        //            .Where(u => u.Id == request.Id)
        //            .AsNoTracking()
        //            .FirstOrDefaultAsync(cancellationToken).ConfigureAwait(true);


        //        result = new ApiResponse<UserDTO>
        //        {
        //            Result = userResult.ToDtoImage(_mapper)



        //        };

        //        return result;
        //    }
            
        //    userResult = await _context.Users
        //        .Where(u => u.Id == request.Id)
                
        //        .FirstOrDefaultAsync(cancellationToken).ConfigureAwait(true);


        //    result = new ApiResponse<UserDTO>
        //    {
        //        Result = userResult.ToDto()



        //    };

        //    return result;


        //}
    }

    

    

    
}
