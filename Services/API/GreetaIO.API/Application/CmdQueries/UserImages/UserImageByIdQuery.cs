﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.UserImages
{
    public class UserImageByIdQuery :  IRequest<UserImageDTO>
    {
        public int Id { get; private set; }

        public UserImageByIdQuery(int id)
        {
            Id = id;
        }

    }
}
