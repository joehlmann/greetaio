﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.UserImages
{
    public class UserImageByIdQueryHandler : IRequestHandler<UserImageByIdQuery, UserImageDTO>
    {
        private readonly ICrudRepository<UserImage, int> _repository;
        private readonly IMapper _mapper;


        public UserImageByIdQueryHandler(ICrudRepository<UserImage, int> repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper;
        }




        public async Task<UserImageDTO> Handle(UserImageByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id).ConfigureAwait(true);

            var dtoResult = _mapper.Map<UserImageDTO>(result);

            return dtoResult;
        }
    }






}
