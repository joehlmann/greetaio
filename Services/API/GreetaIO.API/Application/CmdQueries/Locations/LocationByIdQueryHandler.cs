﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Infrastructure.AutoMapper.Extensions;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Locations
{
    public class LocationByIdQueryHandler : IRequestHandler<LocationByIdQuery, LocationDTO>
    {
        private readonly ICrudRepository<Location, int> _repository;
        

        public LocationByIdQueryHandler(ICrudRepository<Location,int> repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            
        }


        // TODO use Dapper CQRS
        public async Task<LocationDTO> Handle(LocationByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id);
                
                
                

            return result.ToDto();
        }
    }

    
}
