﻿using GreetaIO.API.Application.DTOs;
using MediatR;

namespace GreetaIO.API.Application.CmdQueries.Locations
{
    public class LocationByIdQuery :  IRequest<LocationDTO>
    {
        
        public LocationByIdQuery(int locationId)
        {
            Id = locationId;
        }

        public int Id { get; private set; }
    }
}
