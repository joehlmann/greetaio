﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Events;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using GreetaIO.Utility.Infrastructure;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.DomainEventHandlers.UserImageAtLocationEvent
{
    public class UserImageAtLocationDomEventHandler : INotificationHandler<UserImageAtLocationDomEvent>
    {

        private readonly ILogger _logger;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly ICrudRepository<Library, int> _libraryRepository;
        private readonly IDomainEventManagerInMemory _domainEventManager;


        public UserImageAtLocationDomEventHandler(ILogger<UserImageAtLocationDomEventHandler> logger,
            IIntegrationEventSvc integrationEventSvc, ICrudRepository<Library,int> repository, IDomainEventManagerInMemory domainEventManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _libraryRepository = repository ?? throw new ArgumentNullException(nameof(repository));
            _domainEventManager = domainEventManager ?? throw new ArgumentNullException(nameof(domainEventManager));
        }


        public async Task Handle(UserImageAtLocationDomEvent @event, CancellationToken cancellationToken)
        {
            //Check is DomainEvent has Been added to be Published
            if (!_domainEventManager.PublishDomainEvent(@event.DomainEventId))
                return;

            _logger.LogInformation("UserImage at Location with DomainEventId: {DomainEventId} has arrived at LocationId: {Id} LibraryId:{Id} )",
                @event.DomainEventId, @event.Location.Id, @event.Library.Id);

            var userImageAtLocationIntEvent = new UserImageAtLocationIntEvent(@event.Library.Id,@event.Location.Id,@event.UserImage.ImageData.ToByteString(),@event.CorrelationId.ToString(),ClientNames.ApiClient,"");

            await _integrationEventSvc.AddAndSaveEventAsync(userImageAtLocationIntEvent)
                .ConfigureAwait(false);


            
        }
    }
}
