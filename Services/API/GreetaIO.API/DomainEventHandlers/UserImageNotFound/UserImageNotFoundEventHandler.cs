﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Events;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.DomainEventHandlers.UserEnrolled
{
    public class UserImageNotFoundEventHandler : INotificationHandler<UserImageNotFoundDomEvent>
    {

        private readonly ILogger _logger;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly ICrudRepository<Library, Guid> _libraryRepository;



        public UserImageNotFoundEventHandler(ILogger<UserImageNotFoundEventHandler> logger,
            IIntegrationEventSvc integrationEventSvc, ICrudRepository<Library, Guid> repository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _libraryRepository = repository ?? throw new ArgumentNullException(nameof(repository));
        }


       
        public Task Handle(UserImageNotFoundDomEvent @event, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
