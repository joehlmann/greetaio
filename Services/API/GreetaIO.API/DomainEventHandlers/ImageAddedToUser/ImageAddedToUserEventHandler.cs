﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Events;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.DomainEventHandlers.ImageAddedToUser
{
    public class ImageAddedToUserDomainEventHandler : INotificationHandler<UserImageAddedToUserDomEvent>
    {

        private readonly ILogger _logger;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly ICrudRepository<UserImage, int> _repository;
        private readonly IDomainEventManagerInMemory _domainEventManager;



        public ImageAddedToUserDomainEventHandler(ILogger<ImageAddedToUserDomainEventHandler> logger,
            IIntegrationEventSvc integrationEventSvc, ICrudRepository<UserImage, int> repository, IDomainEventManagerInMemory domainEventManager)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _domainEventManager = domainEventManager ?? throw new ArgumentNullException(nameof(domainEventManager));
        }


       
        public async Task Handle(UserImageAddedToUserDomEvent @event, CancellationToken cancellationToken)
        {
            if (!_domainEventManager.PublishDomainEvent(@event.DomainEventId))
                return;

            _logger.LogInformation("User with Id: {UserId} has Added UserImage : {UserImageId} With Url : {Url} )",
                    @event.User.Id,  @event.Image.Id,@event.Image.Url);

            var imageAddedToUserIntegrationEvent = new ImageAddedToUserIntEvent(@event.User.Id,@event.LibraryId,
                @event.Image.ImageData.ToByteString());

            await _integrationEventSvc.AddAndSaveEventAsync(imageAddedToUserIntegrationEvent)
                .ConfigureAwait(false);
        }
    }
}
