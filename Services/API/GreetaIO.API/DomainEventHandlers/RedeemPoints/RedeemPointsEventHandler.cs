﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.Events;
using GreetaIO.TheDomain.RepoInterfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace GreetaIO.API.DomainEventHandlers.RedeemPoints
{
    public class RedeemPointsEventHandler : INotificationHandler<RedeemedPointsDomEvent>
    {

        private readonly ILogger _logger;
        private readonly IIntegrationEventSvc _integrationEventSvc;
        private readonly ICrudRepository<User, Guid> _repository;



        public RedeemPointsEventHandler(ILogger<RedeemPointsEventHandler> logger,
            IIntegrationEventSvc integrationEventSvc, ICrudRepository<User, Guid> repository)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _integrationEventSvc = integrationEventSvc ?? throw new ArgumentNullException(nameof(integrationEventSvc));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }


       
        public async Task Handle(RedeemedPointsDomEvent @event, CancellationToken cancellationToken)
        {

            //_logger.LogInformation("User with Id: {UserId} has Added UserImage : {UserImageId} With Url : {Url} )",
            //        @event.User.Id,  @event.Image.Id,@event.Image.Url);

            //var imageAddedToUserIntegrationEvent = new ImageAddedToUserIntegrationEvent(@event.User.Id,
            //    @event.Image.Id,
            //    "1".ToGuid());

            //await _integrationEventSvc.AddAndSaveEventAsync(imageAddedToUserIntegrationEvent)
            //    .ConfigureAwait(false);
        }
    }
}
