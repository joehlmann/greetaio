﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Net.Http;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using CacheManager.Core;
using CorrelationId;
using EFSecondLevelCache.Core;
using EventBusAbstract;
using EventBusAbstract.Abstractions;
using EventBusMsgBrokerSvc;
using EventBusMsgBrokerSvc.Settings;
using EventBusRabbitMQue;
using EventBusServiceBus;
using GreetaIO.API.Application.ApplicationServices;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Application.Settings;
using GreetaIO.API.Infrastructure.Authentication.ServiceTokenExchange;
using GreetaIO.API.Infrastructure.Authorization;
using GreetaIO.API.Infrastructure.DelegateHandlers;
using GreetaIO.API.Infrastructure.Filters;
using GreetaIO.API.Infrastructure.MiddleWare;
using GreetaIO.API.Infrastructure.MiddleWare.Authentication;
using GreetaIO.API.Infrastructure.MiddleWare.Logging;
using GreetaIO.API.Infrastructure.Modules;

using GreetaIO.API.Infrastructure.Swagger;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.API.IntegrationEvents.EventHandlers;
using GreetaIO.API.IntegrationEvents.EventHandlers.PickupBooked;
using GreetaIO.API.IntegrationEvents.Events;
using GreetaIO.Infrastructure;
using GreetaIO.Infrastructure.HttpClients;
using GreetaIO.Infrastructure.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.Infrastructure.TokenContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using IdentityServer4.AccessTokenValidation;
using IntegrationEventLogEF.Services;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using RabbitMQ.Client;


namespace GreetaIO.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration,IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }
        private IWebHostEnvironment HostingEnvironment { get; }

        public ILifetimeScope AutofacContainer { get; private set; }


        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new MediatorModule());
            builder.RegisterModule(new ApplicationModule(builder, Configuration.GetConnectionString("ConnectionString")));
            //container.RegisterModule(new BioClientModule(container, Configuration.GetValue<string>("BioEndPoint")));
            builder.RegisterModule(new ServicesModule(Configuration.GetConnectionString("ConnectionString")));

        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddCustomMVC(Configuration)
                .AddSwaggerVer()
                .AddCustomOptions(Configuration)
                .AddCustomDbContext(Configuration)
                .AddIntegrationServices(Configuration)
                .AddEventBus(Configuration)
                .AddCorrelationId();

            services.AddAutoMapper(typeof(Startup));

            //Cache
            services.AddEFSecondLevelCache();
            services.AddRedisCacheServiceProvider();

            //Services

            services.AddCustomServices(Configuration);

            services.AddHttpClientServices(Configuration);

            services.AddHttpClients(Configuration, HostingEnvironment);

            services.AddCustomAuthentication(Configuration);

            services.AddIntegrationEventHandlers();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger, IApiVersionDescriptionProvider provider)
        {
            app.UseCorrelationId(new CorrelationIdOptions
            {
                Header = "request-id",
                UseGuidForCorrelationId = true,
                IncludeInResponse = true,
                UpdateTraceIdentifier = true
            });


            if (env.IsDevelopment())
            {
                logger.LogInformation("In Development environment");

                HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var context = (ApiContext)app
                .ApplicationServices.GetService(typeof(ApiContext));

            ApiContextSeed.SeedAsync(context)
                .Wait();

            
            // Authentication
            app.UseAuthentication();
            app.UseMiddleware<SerilogMiddleware>();
            //app.UseMiddleware<AccessTokenContextMiddleware>();
            app.UseMiddleware<CorrelationContextMiddleware>();

            app.UseCors("CorsPolicy");

            


            //app.UseHttpsRedirection();
            app.UseMvcWithDefaultRoute();


            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });

            ConfigureEventBus(app);
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            if (Configuration.GetValue<bool>("LoadTest"))
            {
                app.UseMiddleware<ByPassAuthMiddleware>();
            }

            app.UseAuthentication();
        }

        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            
          eventBus.Subscribe<UserImageAtLocationIntEvent, IIntegrationEventHandler<UserImageAtLocationIntEvent>>();
            
        }

        
    }

    public static class CustomExtensionMethods
    {

        public static IServiceCollection AddIntegrationEventHandlers(this IServiceCollection services)
        {

            //services.AddTransient<PickupBookedIEventHandler>();
            services.AddTransient<UserImageAtLocationIntEventHandler>();
            //services.AddTransient<ImageAddedToUserIntEventHandler>();
            return services;
        }


        public static IServiceCollection AddCustomOptions(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddOptions();
            services.Configure<EventBusConfig>(configuration.GetSection(ConfigSection.EventBusConfig));
            services.Configure<ConnectionStringConfig>(configuration.GetSection(ConfigSection.ConnectionStrings));
            services.Configure<CacheConfig>(configuration.GetSection(ConfigSection.CacheConfig));
            services.Configure<VrSettingsConfig>(configuration.GetSection(ConfigSection.VrSettingsConfig));
            services.Configure<AzureEventBusConfig>(configuration.GetSection(ConfigSection.AzureEventBusConfig));

            return services;
        }

        public static IServiceCollection AddCustomDbContext(this IServiceCollection services,
            IConfiguration configuration)
        {
            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<ConnectionStringConfig>>().Value;

            GreetaIO.Infrastructure.Startup.ConfigureServices(
                services,
                settings.ConnectionString
            );



            return services;
        }

        public static IServiceCollection AddIntegrationServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            
            var settingsAzure = services.BuildServiceProvider().GetRequiredService<IOptions<AzureEventBusConfig>>();
            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<EventBusConfig>>().Value;
            services.AddScoped<ICorrelationContextService, CorrelationContextService>();

            services.AddTransient<Func<DbConnection, IIntegrationEventLogService>>(
                sp => (DbConnection c) => new IntegrationEventLogService(c));

            

            services.AddTransient<IIntegrationEventSvc, ApiIntegrationEventSvc>();

            //Azure || RabbitMQ

            if (settingsAzure.Value.AzureServiceBusEnabled)
            {
                var msgBrokerManClient = new MsgBrokerManager(settingsAzure);

                var resultQueue = msgBrokerManClient.TopicsCheckOrCreateAsync(new List<string>()
                {
                    settingsAzure.Value.AzureTopicName, 
                    settingsAzure.Value.AzureReplyTopicName
                }).Result;


                var resultSub = msgBrokerManClient.SubscriptionCheckOrCreateForTopicAsync(settingsAzure.Value.AzureTopicName, 
                    settingsAzure.Value.AzureSubscriptionClientName).Result;


                services.AddSingleton<IServiceBusPersisterConnection>(sp =>
                {
                    var logger = sp.GetRequiredService<ILogger<DefaultServiceBusPersisterConnection>>();

                    var serviceBusConnectionString = settingsAzure.Value.AzureEventBusConnection;
                    var serviceBusConnection = new ServiceBusConnectionStringBuilder(serviceBusConnectionString);

                    return new DefaultServiceBusPersisterConnection(serviceBusConnection, logger,settingsAzure.Value.AzureTopicName);
                });
            }
            else
            {
                services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
                {
                    var settings = sp.GetRequiredService<IOptions<EventBusConfig>>().Value;
                    var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

                    var factory = new ConnectionFactory()
                    {
                        HostName = settings.EventBusConnection,
                        DispatchConsumersAsync = true
                    };

                    if (!string.IsNullOrEmpty(settings.EventBusUserName))
                    {
                        factory.UserName = settings.EventBusUserName;
                    }

                    if (!string.IsNullOrEmpty(settings.EventBusPassword))
                    {
                        factory.Password = settings.EventBusPassword;
                    }

                    var retryCount = 5;
                    if (!string.IsNullOrEmpty(settings.EventBusRetryCount))
                    {
                        retryCount = int.Parse(settings.EventBusRetryCount);
                    }

                    string amqpPort = "5672";
                    if (!string.IsNullOrEmpty(settings.EventBusEnvVarNodePort))
                    {
                        amqpPort = settings.EventBusEnvVarNodePort;
                        factory.Port = int.Parse(settings.EventBusEnvVarNodePort);
                    }


                    if (!string.IsNullOrEmpty(settings.EventBusConnection))
                    {
                        factory.HostName = settings.EventBusConnection;
                    }

                    string vhost = @"";



                    var rabbitUri = $"amqp://{factory.UserName}:{factory.Password}@{factory.HostName}:{amqpPort}/{vhost}";

                    factory.Uri = new Uri(rabbitUri);


                    return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
                });

            }

            

            return services;
        }



        public static IServiceCollection AddCustomMVC(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                    options.EnableEndpointRouting = false;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddControllersAsServices()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });


            return services;
        }



        public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<EventBusConfig>>().Value;
            var settingsAzure = services.BuildServiceProvider().GetRequiredService<IOptions<AzureEventBusConfig>>().Value;

            var subscriptionClientName = settings.SubscriptionClientName;
            if (settingsAzure.AzureServiceBusEnabled)
            {
                services.AddSingleton<IEventBus, EventBusAzureServiceBus>(sp =>
                {
                    var serviceBusPersisterConnection = sp.GetRequiredService<IServiceBusPersisterConnection>();
                    var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                    var logger = sp.GetRequiredService<ILogger<EventBusAzureServiceBus>>();
                    var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

                    return new EventBusAzureServiceBus(serviceBusPersisterConnection, logger,
                        eventBusSubcriptionsManager, subscriptionClientName, iLifetimeScope);
                });
            }
            else
            {

                services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
                {
                    var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
                    var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                    var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
                    var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
                    var settings = sp.GetRequiredService<IOptions<EventBusConfig>>().Value;

                    var retryCount = 5;
                    if (!string.IsNullOrEmpty(settings.EventBusRetryCount))
                    {
                        retryCount = int.Parse(settings.EventBusRetryCount);
                    }

                    return new EventBusRabbitMQ(rabbitMQPersistentConnection,
                        logger,
                        iLifetimeScope,
                        eventBusSubcriptionsManager,
                        settings.SubscriptionClientName,
                        retryCount);
                });

                //todo AddEventHandlers
                
                services.AddTransient<UserImageAtLocationIntEventHandler>();
            }

            return services;
        }

       


        public static IServiceCollection AddCustomServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddScoped<ITokenContextService, TokenContextService>();

            services.AddScoped<ICustomerAccountValidService, CustomerAccountValidService>();

            services.AddScoped<IEntityIdValidService, EntityIdValidService>();
            services.AddScoped<ICorrelationContextService, CorrelationContextService>();

            // Singleton services

            services.AddScoped<IDomainEventManagerInMemory, DomainEventManagerInMemory>();
            return services;
        }

        #region Cache
        public static IServiceCollection AddRedisCacheServiceProvider(this IServiceCollection services)
        {
            var settings = services.BuildServiceProvider().GetRequiredService<IOptions<CacheConfig>>().Value;

            var jss = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };


            string redisConfigurationKey = settings.RedisConfigurationKey;
            services.AddSingleton(typeof(ICacheManagerConfiguration),
                new CacheManager.Core.ConfigurationBuilder()
                    .WithJsonSerializer(serializationSettings: jss, deserializationSettings: jss)
                    .WithUpdateMode(CacheUpdateMode.Up)
                    .WithRedisConfiguration(redisConfigurationKey, config =>
                    {
                        config.WithAllowAdmin()
                            .WithDatabase(0)
                            .WithEndpoint(settings.RedisConnection, int.Parse(settings.RedisPort));
                    })
                    .WithMaxRetries(100)
                    .WithRetryTimeout(50)
                    .WithRedisCacheHandle(redisConfigurationKey)
                    .WithExpiration(ExpirationMode.Absolute, TimeSpan.FromMinutes(10))
                    .Build());
            services.AddSingleton(typeof(ICacheManager<>), typeof(BaseCacheManager<>));

            return services;
        }
        #endregion

        #region HttpClientAuthorizConfigs


        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services,
            IConfiguration configuration)
        {

            var assertions = services.BuildServiceProvider().GetRequiredService<IAssertions>();

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(
                    options =>
                    {
                        var config = configuration.GetSection("Authentication");

                        options.Authority = config["Authority"];
                        options.ApiName = config["APIName"];
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.RoleClaimType = "role";

                    });
            //services.AddBExAuthorization(assertions);
            services.AddServiceTokenExchange();

            return services;
        }


        public static IServiceCollection AddGreetaAuthorization(this IServiceCollection services,
            IAssertions assertions)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ReadPickups",
                    builder => builder.RequireAssertion(context => assertions.CanReadPickups(context)));
                options.AddPolicy("WritePickups",
                    builder => builder.RequireAssertion(context => assertions.CanWritePickups(context)));
                options.AddPolicy("DeletePickups",
                    builder => builder.RequireAssertion(context => assertions.CanDeletePickups(context)));
            });

            return services;
        }


        public static IServiceCollection AddHttpClientServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //register delegating handlers
            services.AddTransient<HttpClientServiceTokenExchangeDelegatingHandler>();
            services.AddTransient<HttpClientRequestIdDelegatingHandler>();
            services.AddTransient<HttpClientCorrelationIdDelegatingHandler>();
            services.AddSingleton<IAssertions, GreetaAssertions>();


            return services;
        }

        public static IServiceCollection AddHttpClients(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {


            var userAgent = $"GreetaIO.API-{hostingEnvironment.EnvironmentName}";

            var vrServiceUri = configuration.GetValue<string>("BioEndPoint");

            if (!string.IsNullOrEmpty(vrServiceUri))
            {
                var serviceTokenConfig = configuration.GetSection("ServiceTokenExchange");
                bool.TryParse(serviceTokenConfig["EnableCache"], out var enableTokenCache);

                //services.AddTransient<ServicesHttpClient>((s) =>
                //{
                //    var factory = s.GetRequiredService<IHttpMessageHandlerFactory>();
                //    var handler = factory.CreateHandler(nameof(ServicesHttpClient));

                //    var otherHandler = s.GetRequiredService<HttpClientServiceTokenExchangeDelegatingHandler>();
                //    otherHandler.InnerHandler = handler;
                //    return new ServicesHttpClient(new HttpClient(otherHandler, disposeHandler: false));
                //});



                services.AddHttpClient<IServicesHttpClient, ServicesHttpClient>((client) =>
                    {


                        client.BaseAddress = new Uri(vrServiceUri);
                        client.DefaultRequestHeaders.Add("Accept", "application/json");
                        client.DefaultRequestHeaders.Add("User-Agent", userAgent);

                    })
                    .AddHttpMessageHandler<HttpClientCorrelationIdDelegatingHandler>()
                    .AddHttpMessageHandler<HttpClientServiceTokenExchangeDelegatingHandler>()
                    .AddPolicyHandler(GetRetryPolicy())
                    .AddPolicyHandler(GetCircuitBreakerPolicy())
                    .AddPolicyHandler(GetTimeoutPolicy());
            }

            return services;
        }

        #endregion

        #region Policys

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .Or<TimeoutRejectedException>() // TimeoutRejectedException from Polly's TimeoutPolicy
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));


        }

        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
        }

        static IAsyncPolicy<HttpResponseMessage> GetTimeoutPolicy()
        {
            return Policy.TimeoutAsync<HttpResponseMessage>(2);
        }


        #endregion

        #region Swagger

        public static IServiceCollection AddSwaggerVer(this IServiceCollection services)
        {
            services.AddApiVersioning(
                options => { options.ReportApiVersions = true; });
            services.AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(
                options =>
                {

                    options.OperationFilter<SwaggerDefaultValues>();
                    options.IncludeXmlComments(XmlCommentsFilePath);
                });

            services.AddSwaggerGenNewtonsoftSupport();

            return services;
        }

        static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }

        #endregion
    }

    public static class AzureTopics
    {
        public static string ImageInBound = "imginbound";
    }
}
