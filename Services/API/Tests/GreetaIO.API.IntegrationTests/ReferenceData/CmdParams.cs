﻿namespace GreetaIO.API.IntegrationTests.ReferenceData
{
   public static class CmdParams
    {

        public static class Customer
        {
            public static string clientNumber = "clientNumber";
            public static string companyName = "companyName";
         

        }




        public static class Location
        {
            public static string customerId = "customerId";
            public static string locationName = "locationName";
           
           

        }

        public static class Contact
        {
            public static string contactName = "contactName";
            public static string contactPhone = "contactPhone";
            public static string email = "email";
        }

        public static class Address
        {
            public static string address1 = "address1";
            public static string address2 = "address2";
            public static string state = "state";
            public static string suburb = "suburb";
            public static string postCode = "postCode";
        }

        public static class Library
        {
            public static string locationId = "locationId";
            public static string libraryNumber = "libraryNumber";
            public static string name = "name";
            public static string description = "description";
            public static string libraryType = "libraryType";

        }


        public static class Item
        {
            public static string locationId = "locationId";
            public static string itemNumber = "itemNumber";
            public static string itemName = "itemName";
            public static string itemDescription = "itemDescription";
            public static string itemType = "itemType";
            public static string rewardsType = "rewardsType";
            public static string pointsCost = "pointsCost";
            public static string pointsReward = "pointsReward";
            public static string isProduct = "isProduct";
            

        }

        public static class User
        {
            public static string locationId = "locationId";
            public static string aliasName = "aliasName";
            public static string contactDTO = "contactDTO";
            public static string userImages = "userImages";
            public static string userStatus = "userStatus";
            public static string currentRewards = "currentRewards";

        }

        public static class EnrollUser
        {
            public static string imageData = "imageData";
            public static string locationId = "locationId";

        }

        public static class FindUnSub
        {
            public static string imageData = "imageData";
            public static string locationId = "locationId";

        }
        public static class PersonliseUser
        {
            public static string userId = "userId";
            public static string locationId = "locationId";
            public static string name = "name";

        }

        public static class UserImage
        {
            public static string userId = "userId";
            public static string imageData = "imageData";

        }

        public static class Identified
        {
            public static string command = "command";
            public static string id = "id";

        }
    }
}
