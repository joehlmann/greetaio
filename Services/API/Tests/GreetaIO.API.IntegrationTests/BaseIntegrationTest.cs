﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using GreetaIO.API.Infrastructure.Modules;
using GreetaIO.API.IntegrationTests.ArrangeModules;
using GreetaIO.Infrastructure;
using IntegrationEventLogEF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;


namespace GreetaIO.API.IntegrationTests
{
    public class BaseIntegrationTest
    {
        protected IConfiguration _configuration;
        protected IServiceProvider _provider;
        protected ILifetimeScope _containerScope;
        protected IContainer _container;
        protected string _connectionString;

        public BaseIntegrationTest()
        {

            HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();

            _configuration = new  ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            _connectionString = _configuration.GetConnectionString("ConnectionString");

            _provider = Configure();

            _containerScope = _container.BeginLifetimeScope();
        }

        public  IServiceProvider Configure()
        {
            
            var containerBuilder = new ContainerBuilder();


            containerBuilder.RegisterInstance(new LoggerFactory())
                .As<ILoggerFactory>();

            containerBuilder.RegisterGeneric(typeof(Logger<>))
                .As(typeof(ILogger<>));

            containerBuilder.RegisterType<ApiContext>()
                .WithParameter("options", DbContextConfig.Configure(_connectionString))
                .InstancePerLifetimeScope();


            containerBuilder.RegisterType<IntEventLogContext>()
                .WithParameter("options", DbContextConfig.ConfigureEventLog(_connectionString))
                .InstancePerLifetimeScope();

            containerBuilder.RegisterModule(new ApplicationModule(containerBuilder,_connectionString));
            containerBuilder.RegisterModule(new ServicesModuleIntegrationTests(_connectionString));

            containerBuilder.RegisterModule(new MediatorTestModule());


            containerBuilder.AddAutoMapper(typeof(Startup).Assembly);
            
            _container = containerBuilder.Build();

            return new AutofacServiceProvider(_container);
        }

        
        public class DbContextConfig
        {
            public static DbContextOptions<ApiContext> Configure(string connectionString)
            {
                var builder = new DbContextOptionsBuilder<ApiContext>();
                builder.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);

                    });
                builder.EnableSensitiveDataLogging();
                //builder.UseLazyLoadingProxies();
                return builder.Options;
            }


            public static DbContextOptions<IntEventLogContext> ConfigureEventLog(string connectionString)
            {
                var builder = new DbContextOptionsBuilder<IntEventLogContext>();
                builder.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);

                    });
                builder.EnableSensitiveDataLogging();
                builder.UseLazyLoadingProxies();
                return builder.Options;
            }
        }
    }
}
