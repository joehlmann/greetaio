﻿using Autofac;
using EFSecondLevelCache.Core.Contracts;
using EventBusAbstract.Abstractions;
using GreetaIO.API.Application.ApplicationServices;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Infrastructure.Authorization;
using GreetaIO.API.Infrastructure.MiddleWare;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure.Idempotency;
using GreetaIO.Infrastructure.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.Infrastructure.TokenContext;
using IntegrationEventLogEF.Services;
using Microsoft.Data.SqlClient;
using Telerik.JustMock;

namespace GreetaIO.API.IntegrationTests.ArrangeModules
{
    public class ServicesModuleIntegrationTests : Autofac.Module
    {
        protected string _connectionStringEventLog;

        public ServicesModuleIntegrationTests(string connectionStringEventLog)
        {
            _connectionStringEventLog = connectionStringEventLog;
        }


        private void LoadMocks(ContainerBuilder builder)
        {




            builder.RegisterInstance(Mock.Create<IIdentityService>())
                .As<IIdentityService>()
                .SingleInstance();

            builder.RegisterInstance(Mock.Create<IEventBus>())
                .As<IEventBus>()
                .SingleInstance();


            builder.RegisterInstance(Mock.Create<IEFCacheServiceProvider>())
                .As<IEFCacheServiceProvider>()
                .SingleInstance();



        }


        protected override void Load(ContainerBuilder builder)
        {

            LoadMocks(builder);


            //Services
            builder.RegisterType<CustomerAccountValidService>()
                .As<ICustomerAccountValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<CorrelationContextService>()
                .As<ICorrelationContextService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionStringEventLog))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();



            builder.RegisterType<ApiIntegrationEventSvc>()
                .As<IIntegrationEventSvc>()
                .InstancePerLifetimeScope();



            builder.RegisterType<ContactValidService>()
                .As<IContactValidService>()
                .InstancePerLifetimeScope();




            builder.RegisterType<AddressValidService>()
                .As<IAddressValidService>()
                .InstancePerLifetimeScope();



            //Singleton
            builder.RegisterType<DomainEventManagerInMemory>()
                .As<IDomainEventManagerInMemory>()
                .SingleInstance();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .SingleInstance();



        }
    }
}
