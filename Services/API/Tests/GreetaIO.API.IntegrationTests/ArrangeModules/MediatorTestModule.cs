﻿using System.Reflection;
using Autofac;
using FluentValidation;
using GreetaIO.API.Application.Behaviors;
using GreetaIO.API.Application.CmdValidations.Customers;
using GreetaIO.API.Cmds.Customers;
using GreetaIO.API.DomainEventHandlers.ImageAddedToUser;
using MediatR;

namespace GreetaIO.API.IntegrationTests.ArrangeModules
{
    public class MediatorTestModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
                .AsImplementedInterfaces();

            // Register all the Command classes (they implement IRequestHandler) in assembly holding the Commands
            builder.RegisterAssemblyTypes(typeof(CustomerCreateCmd).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>));




           
            // Register the DomainEventHandler classes (they implement INotificationHandler<>) in assembly holding the Domain Events
            builder.RegisterAssemblyTypes(typeof(ImageAddedToUserDomainEventHandler).GetTypeInfo().Assembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>));



            // Register the Command's Validators (Validators based on FluentValidation library)
            builder
                .RegisterAssemblyTypes(typeof(CreateCustomerCmdValidator).GetTypeInfo().Assembly)
                .Where(t => t.IsClosedTypeOf(typeof(IValidator<>)))
                .AsImplementedInterfaces();


            builder.Register<ServiceFactory>(context =>
            {
                var componentContext = context.Resolve<IComponentContext>();
                return t => { object o; return componentContext.TryResolve(t, out o) ? o : null; };
            });

           builder.RegisterGeneric(typeof(ValidatorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
        }
    }



}
