﻿using Autofac;
using GreetaIO.API.Application.CmdQueries;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.Infrastructure.Repositories;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;

namespace GreetaIO.API.IntegrationTests.ArrangeModules
{

    public class ApplicationTestModule
        :Autofac.Module
    {

        public string ConnectionString { get; }

        

        public ApplicationTestModule(string connectionString)
        {
            ConnectionString = connectionString;
        
        }

        protected override void Load(ContainerBuilder builder)
        {
            

            builder.Register(c => new GreetaQueries(ConnectionString))
                .As<IGreetaQueries>()
                .InstancePerLifetimeScope();

          

            builder.RegisterType<CustomerRepository>()
                .As<ICrudRepository<Customer,int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<LibraryRepository>()
                .As<ICrudRepository<Library,int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserImageRepository>()
                .As<ICrudRepository<UserImage, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<ICrudRepository<User, int>>()
                .InstancePerLifetimeScope();
            

            builder.RegisterType<LocationRepository>()
                .As<ICrudRepository<Location, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ItemRepository>()
                .As<ICrudRepository<Item, int>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TransactionRepository>()
                .As<ICrudRepository<Transaction, int>>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IdentityService>()
                .As<IIdentityService>()
                .InstancePerLifetimeScope();

          

    

        }
    }
}
