﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Cmds.UserImages;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using MediatR;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.ValidationsTests
{
    
    public class UserImageCreateCmdValidatorTests : BaseIntegrationTest
    {

      
        private IMediator _mediator;


        public UserImageCreateCmdValidatorTests(){}


        [MemberData(nameof(Data))]
        [Theory]
        public void InvalidImage_createImageCmd_validator_invariant_rule(UserImageCreateCmd command, int numErrors,  string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            
            var errors = new List<string>();

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException) e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException) e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        [MemberData(nameof(Data2))]
        [Theory]
        public void User_Doesnt_Exist_createImageCmd_validator_invariant_rule(UserImageCreateCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            
            var errors = new List<string>();

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        public static List<object[]> Data()
        {
            return new List<object[]>
            {
                new object[] {new UserImageCreateCmd(((int)EntityTypeRange.User).ToString(),"xxx") , 1 , ErrorMsg.NotValidImage },
                

            };
        }
        public static List<object[]> Data2()
        {
            return new List<object[]>
            {
                
                new object[] {new UserImageCreateCmd("99","ddd") , 2 , ErrorMsg.UserDoesNotExist },

            };
        }

    }
}
