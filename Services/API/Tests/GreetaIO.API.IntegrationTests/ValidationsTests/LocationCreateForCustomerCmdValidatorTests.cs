﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Cmds.Locations;
using GreetaIO.API.IntegrationTests.ReferenceData;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Data.SqlClient;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.ValidationsTests
{
    
    public class LocationCreateForCustomerCmdValidatorTests : BaseIntegrationTest
    {
        private IMediator _mediator;






        [MemberData(nameof(Data))]
        [Theory]
        public void Location_CreateForCustomerCmd_validator(LocationCreateForCustomerCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();

            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors.Clear();

                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {
                errors.Clear();

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        //[MemberData(nameof(Data2))]
        //[Theory]
        //public void InvalidCustomer_createCustomerCmd_validator_Address_rule(CustomerCreateCommand command, int numErrors, string errorMsg)
        //{

        //    //Arrange 
        //    _mediator = _containerScope.Resolve<IMediator>();

        //    ClearDatabase();

        //    var errors = new List<string>();

        //    //Act

        //    try
        //    {
        //        var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
        //    }
        //    catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
        //    {
        //        errors = ((ValidationException)e.InnerException.InnerException)?.Errors
        //            .Select(v => v.ErrorMessage).ToList();

        //    }
        //    catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
        //    {

        //        errors.Add(((GreetaDomainException)e.InnerException)?.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine(e.Message);
        //    }



        //    //Assert

        //    errors.Count.Should().Be(numErrors);

        //    errors[0].Should().Be(errorMsg);


        //}

        //[MemberData(nameof(Data3))]
        //[Theory]
        //public void InvalidCustomer_createCustomerCmd_validator_Contact_rule(CustomerCreateCommand command, int numErrors, string errorMsg)
        //{

        //    //Arrange 
        //    _mediator = _containerScope.Resolve<IMediator>();

        //    ClearDatabase();

        //    var errors = new List<string>();


        //    //Act

        //    try
        //    {
        //        var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
        //    }
        //    catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
        //    {
        //        errors = ((ValidationException)e.InnerException.InnerException)?.Errors
        //            .Select(v => v.ErrorMessage).ToList();

        //    }
        //    catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
        //    {

        //        errors.Add(((GreetaDomainException)e.InnerException)?.Message);
        //    }
        //    catch (Exception e)
        //    {
        //        Debug.WriteLine(e.Message);
        //    }



        //    //Assert

        //    errors.Count.Should().Be(numErrors);

        //    errors[0].Should().Be(errorMsg);


        //}



        public static List<object[]> Data()
        {
            return new List<object[]>
            {
                new object[] { 
                    FakeLocationRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Location.customerId]=EntityTypeRange.Customer.ToIntStr(1),
                        [CmdParams.Location.locationName]="TesTLocation",

                    } ) , 1 , ErrorMsg.NoErrors },
             
             


            };
        }

        public static List<object[]> Data2()
        {
            var result =  new List<object[]>
            {
                
                new object[] {
                    FakeLocationRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Location.customerId]=EntityTypeRange.Customer.ToIntStr(),
                        [CmdParams.Location.locationName]="",
                        [CmdParams.Address.address1]="",
                        [CmdParams.Address.address2]="",
                        [CmdParams.Address.state]="",
                        [CmdParams.Address.postCode]="",
                        [CmdParams.Address.suburb]="",
                        [CmdParams.Contact.contactName]="",
                        [CmdParams.Contact.contactPhone]="",
                        [CmdParams.Contact.email]="",


                    } ) , 8 , ErrorMsg.AddressLine1Invalid }
            };
            return result;
        }
        public static List<object[]> Data3()
        {
            var result = new List<object[]>
            {
                new object[] {
                    FakeLocationRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Location.customerId]=EntityTypeRange.Location.ToIntStr(3),
                        [CmdParams.Contact.contactName]="",
                        [CmdParams.Contact.contactPhone]="",
                        [CmdParams.Contact.email]="",

                    } ) , 4 , ErrorMsg.ContactNameEmpty }


            };

            return result;
        }




        private static LocationCreateForCustomerCmd FakeLocationRequest(Dictionary<string, object> args = null)
        {
            return new LocationCreateForCustomerCmd(
                customerId: args != null && args.ContainsKey("customerId") ? (string) args["customerId"] : EntityTypeRange.Location.ToIntStr(),
                locationName: args != null && args.ContainsKey("locationName") ? (string) args["locationName"] : "locationName",
                address1: args != null && args.ContainsKey("address1") ? (string) args["address1"] : "address1",
                address2: args != null && args.ContainsKey("address2") ? (string) args["address2"] : "address2",
                state: args != null && args.ContainsKey("state") ? (string) args["state"] : "VIC",
                suburb: args != null && args.ContainsKey("suburb") ? (string) args["suburb"] : "suburb",
                postCode: args != null && args.ContainsKey("postCode") ? (string) args["postCode"] : "3000",
                contactName: args != null && args.ContainsKey("contactName") ? (string) args["contactName"] : "contactName",
                contactPhone: args != null && args.ContainsKey("contactPhone") ? (string) args["contactPhone"] : "12345678",
                email: args != null && args.ContainsKey("email")
                    ? (string) args["email"]
                    : "1@1.com"
            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.[Locations] where LocationId={(EntityTypeRange.Location.ToInt()+1).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
