﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Items;
using GreetaIO.API.IntegrationTests.ReferenceData;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Data.SqlClient;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.ValidationsTests
{
    
    public class ItemCreateCmdValidatorTests : BaseIntegrationTest
    {

      
        private IMediator _mediator;


        public ItemCreateCmdValidatorTests(){}


        [MemberData(nameof(DataHappy))]
        [Theory]
        public void ValidItem_createItemCmd_validator_invariant_rule(ItemCreateForLocationCmd command, int numErrors,  string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);
            
            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;

                var a = response;

            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException) e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException) e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);

            // CleanUp


        }

        [MemberData(nameof(DataLocationInvalid))]
        [Theory]
        public void Item_InvalidLocation_createItemCmd_validator_invariant_rule(ItemCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataItemNumSad))]
        [Theory]
        public void Item_InvalidItemNum_createItemCmd_validator_invariant_rule(ItemCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataItemTypeSad))]
        [Theory]
        public void ItemType_InvalidType_createItemCmd_validator_invariant_rule(ItemCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataRewardsTypeSad))]
        [Theory]
        public void RewardsType_InvalidType_createItemCmd_validator_invariant_rule(ItemCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }


        public static List<object[]> DataLocationInvalid()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Item.locationId]="1",
                        [CmdParams.Item.itemNumber]=99

                    } ) , 1 , ErrorMsg.InvalidLocation }
            };
            return result;
        }
        public static List<object[]> DataRewardsTypeSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Item.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Item.itemNumber]=99,
                        [CmdParams.Item.rewardsType]="its"

                    } ) , 1 , ErrorMsg.RewardsTypeInvalid }
            };
            return result;
        }
        public static List<object[]> DataItemTypeSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Item.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Item.itemNumber]=99,
                        [CmdParams.Item.itemType]="its"

                    } ) , 1 , ErrorMsg.ItemTypeInvalid }
            };
            return result;
        }


        public static List<object[]> DataHappy()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Item.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Item.itemNumber]=99

                    } ) , 1 , ErrorMsg.NoErrors }
            };
            return result;
        }
        public static List<object[]> DataItemNumSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Item.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Item.itemNumber]=0

                    } ) , 2 , ErrorMsg.ItemMustNotBeEmpty }
            };
            return result;
        }

        public static List<object[]> DataContactSadNull()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeItemRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = null,Email = null,Phone = null},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 3 , ErrorMsg.ContactNameEmpty }
            };
            return result;
        }
        private static IEnumerable<UserImageDTO> GetUserImageDtos()
        {
            //var userImage1 = UserImage.Create(File.ReadAllBytes("Resources/cat1.png"), "Resources/cat1.png");

            

            var userImageDto = new UserImageDTO()
            {
                ImageData = (File.ReadAllBytes("Resources/cat1.png")).ToByteString(),
                TargetId = "",
                TransactionId = "",
                Url = "Resources/cat1.png",
                //UserId = "",
                UserImageId = "",
                UserImageType = UserImageType.New.GetEnumName()
            };

            var result = new List<UserImageDTO>()
            {
                userImageDto
            };

            return result;
        }

        private static ItemCreateForLocationCmd FakeItemRequest(Dictionary<string, object> args = null)
        {
            return new ItemCreateForLocationCmd(
                locationId: args != null && args.ContainsKey("locationId") ? (string)args["locationId"] : EntityTypeRange.Location.ToIntStr(),
                itemNumber: args != null && args.ContainsKey("itemNumber") ? (int)args["itemNumber"] : EntityTypeRange.Item.ToInt(),
                itemName: args != null && args.ContainsKey("itemName") ? (string)args["itemName"] : "itemName",
                itemType: args != null && args.ContainsKey("itemType") ? (string)args["itemType"] :  ItemType.Product.GetEnumName(),
                rewardsType: args != null && args.ContainsKey("rewardsType") ? (string)args["rewardsType"] : RewardsType.Gold.GetEnumName(),
                pointsCost: args != null && args.ContainsKey("pointsCost") ? (int)args["pointsCost"] : 1,
                pointsReward: args != null && args.ContainsKey("pointsReward") ? (int)args["pointsReward"] : 1,
                isProduct: args != null && args.ContainsKey("isProduct") ? (bool)args["isProduct"] : true,
                itemDescription: args != null && args.ContainsKey("itemDescription") ? (string)args["itemDescription"] : "itemDescription"


            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.[items] where itemid > {(EntityTypeRange.Item.ToInt() + 2).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
