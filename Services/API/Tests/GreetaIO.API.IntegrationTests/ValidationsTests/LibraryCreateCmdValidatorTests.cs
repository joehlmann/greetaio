﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Libraries;
using GreetaIO.API.IntegrationTests.ReferenceData;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Data.SqlClient;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.ValidationsTests
{
    
    public class LibraryCreateCmdValidatorTests : BaseIntegrationTest
    {

      
        private IMediator _mediator;


        public LibraryCreateCmdValidatorTests(){}


        [MemberData(nameof(DataHappy))]
        [Theory]
        public void Happy_Library_createLibraryCmd_validator(LibraryCreateForLocationCmd command, int numErrors,  string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(TheDomain.Exceptions.ErrorMsg.NoErrors);
            
            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;

                var a = response;

            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException) e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException) e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                 Debug.WriteLine(e.Message);
                 errors.Clear();
                 errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);

            // CleanUp


        }

        [MemberData(nameof(DataLocationIdSad))]
        [Theory]
        public void LocationId_InvalidId_createLibraryCmd_validator(LibraryCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataLibraryTypeSad))]
        [Theory]
        public void LibraryType_InvalidLibraryType_createLibraryCmd_validator(LibraryCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataNullSad))]
        [Theory]
        public void NullAll_InvalidNull_createLibraryCmd_validator(LibraryCreateForLocationCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }


            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }


        public static List<object[]> DataLocationIdSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeLibraryRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Library.locationId]=EntityTypeRange.Location.ToIntStr(2),
                        [CmdParams.Library.name]="My Lib",
                        [CmdParams.Library.libraryType]=LibraryType.New.GetEnumName(),


                    } ) , 1 , ErrorMsg.InvalidLocation }
            };
            return result;
        }

        public static List<object[]> DataLibraryTypeSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeLibraryRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Library.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Library.name]="My Lib",
                        [CmdParams.Library.libraryType]="someType",


                    } ) , 1 , ErrorMsg.LibTypeInvalid }
            };
            return result;
        }


        public static List<object[]> DataHappy()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeLibraryRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Library.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.Library.name]="My Lib",
                        [CmdParams.Library.libraryType]=LibraryType.New.GetEnumName(),


                    } ) , 1 , ErrorMsg.NoErrors }
            };
            return result;
        }
        public static List<object[]> DataNullSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeLibraryRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.Library.locationId]=null,
                        [CmdParams.Library.name]=null,
                        [CmdParams.Library.libraryType]=null,
                        [CmdParams.Library.libraryNumber]=0,
                        [CmdParams.Library.description]=null,


                    } ) , 8 , "'Location Id' must not be empty." }
            };
            return result;
        }

        public static List<object[]> DataContactSadNull()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeLibraryRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = null,Email = null,Phone = null},
                        

                    } ) , 3 , ErrorMsg.ContactNameEmpty }
            };
            return result;
        }
        

        private static LibraryCreateForLocationCmd FakeLibraryRequest(Dictionary<string, object> args = null)
        {
            return new LibraryCreateForLocationCmd(
                locationId: args != null && args.ContainsKey("locationId") ? (string)args["locationId"] : EntityTypeRange.Location.ToIntStr(),
                name: args != null && args.ContainsKey("name") ? (string)args["name"] : "name",
                description: args != null && args.ContainsKey("description") ? (string)args["description"] : "description",
                libraryType: args != null && args.ContainsKey("libraryType") ? (string)args["libraryType"] : LibraryType.Default.GetEnumName(),
                libraryNumber: args != null && args.ContainsKey("libraryNumber") ? (int)args["libraryNumber"] : EntityTypeRange.Library.ToInt()
            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.Libraries where LibraryId > {(EntityTypeRange.Library.ToInt() + 2).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
