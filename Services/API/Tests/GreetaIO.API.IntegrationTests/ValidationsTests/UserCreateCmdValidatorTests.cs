﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.DTOs;
using GreetaIO.API.Cmds.Users;
using GreetaIO.API.IntegrationTests.ReferenceData;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Microsoft.Data.SqlClient;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.ValidationsTests
{
    
    public class UserCreateCmdValidatorTests : BaseIntegrationTest
    {

      
        private IMediator _mediator;


        public UserCreateCmdValidatorTests(){}


        [MemberData(nameof(DataHappy))]
        [Theory]
        public void ValidUser_createImageCmd_validator_invariant_rule(UserCreateCmd command, int numErrors,  string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);
            
            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;

                var a = response;

            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException) e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException) e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);

            // CleanUp


        }

        [MemberData(nameof(DataContactSad))]
        [Theory]
        public void User_InvalidContact_createImageCmd_validator_invariant_rule(UserCreateCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataContactSadNull))]
        [Theory]
        public void User_InvalidContactNull_createImageCmd_validator_invariant_rule(UserCreateCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }
        [MemberData(nameof(DataLocationNullSad))]
        [Theory]
        public void LocationId_InvalidNull_createImageCmd_validator_invariant_rule(UserCreateCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }

        [MemberData(nameof(DataLocationInvalid))]
        [Theory]
        public void LocationId_Invalid_Id_createImageCmd_validator_invariant_rule(UserCreateCmd command, int numErrors, string errorMsg)
        {

            //Arrange 
            _mediator = _containerScope.Resolve<IMediator>();
            ClearDatabase();

            var errors = new List<string>();

            errors.Add(ErrorMsg.NoErrors);

            //Act

            try
            {
                var response = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);

            errors[0].Should().Be(errorMsg);


        }


        public static List<object[]> DataLocationInvalid()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeUserRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]="1",
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = "bob",Email = "1@1.com",Phone = "12345678"},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 1 , ErrorMsg.LocationDoesNotExist }
            };
            return result;
        }

        public static List<object[]> DataLocationNullSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeUserRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=null,
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = "bob",Email = "1@1.com",Phone = "12345678"},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 3 , ErrorMsg.LocationNull }
            };
            return result;
        }


        public static List<object[]> DataHappy()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeUserRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = "Bob",Email = "1@2.com",Phone = "12345678"},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 1 , ErrorMsg.NoErrors }
            };
            return result;
        }
        public static List<object[]> DataContactSad()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeUserRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = "",Email = "",Phone = ""},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 3 , ErrorMsg.ContactNameEmpty }
            };
            return result;
        }

        public static List<object[]> DataContactSadNull()
        {
            var result = new List<object[]>
            {

                new object[] {
                    FakeUserRequest( new Dictionary<string, object>()
                    {
                        [CmdParams.User.locationId]=EntityTypeRange.Location.ToIntStr(),
                        [CmdParams.User.aliasName]="My Alias",
                        [CmdParams.User.contactDTO]= new ContactDTO(){ContactName = null,Email = null,Phone = null},
                        [CmdParams.User.userImages]= GetUserImageDtos()

                    } ) , 3 , ErrorMsg.ContactNameEmpty }
            };
            return result;
        }
        private static IEnumerable<UserImageDTO> GetUserImageDtos()
        {
            //var userImage1 = UserImage.Create(File.ReadAllBytes("Resources/cat1.png"), "Resources/cat1.png");

            

            var userImageDto = new UserImageDTO()
            {
                ImageData = (File.ReadAllBytes("Resources/cat1.png")).ToByteString(),
                TargetId = "",
                TransactionId = "",
                Url = "Resources/cat1.png",
                //UserId = "",
                UserImageId = "",
                UserImageType = UserImageType.New.GetEnumName()
            };

            var result = new List<UserImageDTO>()
            {
                userImageDto
            };

            return result;
        }

        private static UserCreateCmd FakeUserRequest(Dictionary<string, object> args = null)
        {
            return new UserCreateCmd(
                locationId: args != null && args.ContainsKey("locationId") ? (string)args["locationId"] : EntityTypeRange.Location.ToIntStr(),
                aliasName: args != null && args.ContainsKey("aliasName") ? (string)args["aliasName"] : "aliasName",
                contactDTO: args != null && args.ContainsKey("contactDTO") ? (ContactDTO)args["contactDTO"] : ContactDTO.Empty(),
                userImages: args != null && args.ContainsKey("userImages") ? (IEnumerable<UserImageDTO>)args["userImages"] : new List<UserImageDTO>(){UserImageDTO.Empty()}
                
            );

        }

        private void ClearDatabase()
        {
            string query =
                $"DELETE FROM dbo.[Users] where UserId > {(EntityTypeRange.User.ToInt() + 2).ToString()};";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection)
                {
                    CommandType = CommandType.Text
                };

                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
