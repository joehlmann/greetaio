﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.CmdQueries.Librarys;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.QueryHandlerTests
{

    
    public class LibraryByIdQueryTests : BaseIntegrationTest
    {

        public LibraryByIdQueryTests()
        {

        }



        [InlineData(0, 0, LibraryType.Default)]
        [Theory]
        public void Success_LibraryQueryCmd_ById_Results(int  addToId, int numErrors, LibraryType expectedType)
        {

            //Arrange  and some more 
            var errors = new List<string>();
            var  _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.Library.ToInt(addToId);
            var command = new LibraryByIdQuery(byId);

            var response = LibraryDTO.Empty();


            //Act ssss

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                 response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert that this should work 

            errors.Count.Should().Be(numErrors);
            response.LibraryType.Should().Be(expectedType.GetEnumName());



        }



        
        [InlineData(-2001, 0, LibraryType.Empty)]
        [Theory]
        public void Fail_LibraryQueryCmd_ById(int addToId, int numErrors, LibraryType expectedType)
        {

            //Arrange 
            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.Library.ToInt(addToId);
            var command = new LibraryByIdQuery(byId);

            var response = LibraryDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            response.LibraryType.Should().Be(expectedType.GetEnumName());



        }

    }
}
