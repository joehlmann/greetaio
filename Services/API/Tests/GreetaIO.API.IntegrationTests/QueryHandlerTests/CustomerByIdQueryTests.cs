﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.CmdQueries.Customers;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.QueryHandlerTests
{

    
    public class CustomerByIdQueryTests : BaseIntegrationTest
    {

        public CustomerByIdQueryTests()
        {

        }



        [InlineData(0, 0, CustomerType.Default)]
        [Theory]
        public void Success_CustomerQueryCmd_ById_Results(int  addToId, int numErrors, CustomerType expectedCustomerType)
        {

            //Arrange 
            


            var errors = new List<string>();
            var  _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.Customer.ToInt(addToId);
            var command = new CustomerByIdQuery(byId);

            var response = CustomerDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                 response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            response.CustomerType.Should().Be(expectedCustomerType.GetEnumName());



        }
        
        [InlineData(-200, 0, CustomerType.Empty)]
        [Theory]
        public void Fail_CustomerQueryCmd_ById_Results(int addToId, int numErrors, CustomerType expectedCustomerType)
        {

            //Arrange 
            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.Customer.ToInt(addToId);
            var command = new CustomerByIdQuery(byId);

            var response = CustomerDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            response.CustomerType.Should().Be(expectedCustomerType.GetEnumName());



        }

    }
}
