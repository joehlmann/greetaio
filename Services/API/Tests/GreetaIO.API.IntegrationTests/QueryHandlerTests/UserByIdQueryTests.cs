﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Autofac;
using FluentAssertions;
using FluentValidation;
using GreetaIO.API.Application.CmdQueries.Users;
using GreetaIO.API.Application.DTOs;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Exceptions;
using GreetaIO.Utility.Helper;
using MediatR;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.API.IntegrationTests.QueryHandlerTests
{

    
    public class UserByIdQueryTests : BaseIntegrationTest
    {

        public UserByIdQueryTests()
        {

        }



        [InlineData(0, 0, UserType.New)]
        
        [Theory]
        public void Success_UserQueryCmd_ById_Results(int  addToId, int numErrors, UserType expectedType)
        {

            //Arrange this for me and again 
            var errors = new List<string>();

            var  _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.User.ToInt(addToId);
            var command = new UserByIdQuery(byId);

            var response = UserDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                 response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            response.UserType.Should().Be(expectedType.GetEnumName());



        }

        
        [InlineData(-201, 0, UserType.Empty)]
        [Theory]
        public void Fail_UserQueryCmd_ById_Results(int addToId, int numErrors, UserType expectedType)
        {

            //Arrange 
            var errors = new List<string>();
            var _mediator = _containerScope.Resolve<IMediator>();

            var byId = EntityTypeRange.User.ToInt(addToId);
            var command = new UserByIdQuery(byId);

            var response = UserDTO.Empty();


            //Act

            try
            {
                var result = _mediator.Send(command, Arg.IsAny<CancellationToken>()).Result;
                response = result;
            }
            catch (AggregateException e) when (e.InnerException.InnerException.GetType() == typeof(ValidationException))
            {
                errors = ((ValidationException)e.InnerException.InnerException)?.Errors
                    .Select(v => v.ErrorMessage).ToList();

            }
            catch (AggregateException e) when (e.InnerException.GetType() == typeof(GreetaDomainException))
            {

                errors.Add(((GreetaDomainException)e.InnerException)?.Message);
            }
            catch (Exception e)
            {
                errors.Clear();
                errors.Add(e.Message);
            }



            //Assert

            errors.Count.Should().Be(numErrors);
            response.UserType.Should().Be(expectedType.GetEnumName());



        }

    }
}
