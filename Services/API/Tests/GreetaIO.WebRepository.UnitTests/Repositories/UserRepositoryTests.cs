﻿using System;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using GreetaIO.Infrastructure;
using GreetaIO.TheDomain.Entities;
using GreetaIO.TheDomain.RepoInterfaces;
using GreetaIO.WebRepository.UnitTests.ArrangeModules;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telerik.JustMock;
using Xunit;

namespace GreetaIO.WebRepository.UnitTests.Repositories
{
    
    public class UserRepositoryTests
    {
        private readonly IMediator _mediator;
        private readonly ILifetimeScope _container;
        private readonly ApiContext _context;
        private readonly ICrudRepository<User, Guid> _repository;

        public UserRepositoryTests()
        {
            _context = Mock.Create<ApiContext>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(_context).As<ApiContext>();
            builder.AddAutoMapper(typeof(Startup).Assembly);
            builder.RegisterModule(new MediatorModuleForTesting());
            _container = builder.Build();
            _mediator = _container.Resolve<IMediator>();
        }



        [Fact]
        public void RequestManagerGetById_NewId_Success()
        {


        }

        [Fact]
        public void RequestManagerGetById_RepeatedId_Fail()
        {


        }

    }
}
