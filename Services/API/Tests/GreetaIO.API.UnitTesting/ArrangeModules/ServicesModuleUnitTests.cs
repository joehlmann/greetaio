﻿using Autofac;
using EFSecondLevelCache.Core.Contracts;
using EventBusAbstract.Abstractions;
using GreetaIO.API.Application.ApplicationServices;
using GreetaIO.API.Application.ApplicationServices.Interfaces;
using GreetaIO.API.Infrastructure.Authorization;
using GreetaIO.API.Infrastructure.MiddleWare;
using GreetaIO.API.Infrastructure.Services;
using GreetaIO.API.IntegrationEvents;
using GreetaIO.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure;
using GreetaIO.Infrastructure.Infrastructure.DomainServices;
using GreetaIO.Infrastructure.TokenContext;
using IntegrationEventLogEF.Services;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Telerik.JustMock;

namespace GreetaIO.API.UnitTesting.ArrangeModules
{
    public class ServicesModuleUnitTests : Autofac.Module
    {
        protected string _connectionStringEventLog;
        protected IEventBus _eventBus;
        protected ILogger _logger;
        protected IIdentityService _identityService;



        public ServicesModuleUnitTests(string connectionStringEventLog)
        {
            _connectionStringEventLog = connectionStringEventLog;
            
        }

        private void LoadMocks(ContainerBuilder builder)
        {
            
            builder.RegisterInstance(Mock.Create<IIdentityService>())
                .As<IIdentityService>()
                .SingleInstance();

            builder.RegisterInstance(Mock.Create<IEventBus>())
                .As<IEventBus>()
                .SingleInstance();

            builder.RegisterInstance(Mock.Create<ILogger>())
                .As<ILogger>()
                .SingleInstance();



            builder.RegisterInstance(Mock.Create<IEFCacheServiceProvider>())
                .As<IEFCacheServiceProvider>()
                .SingleInstance();
        }


        protected override void Load(ContainerBuilder builder)
        {
            LoadMocks(builder);

            builder.RegisterType<TokenContextService>()
                .As<ITokenContextService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<CustomerAccountValidService>()
                .As<ICustomerAccountValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EntityIdValidService>()
                .As<IEntityIdValidService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<CorrelationContextService>()
                .As<ICorrelationContextService>()
                .InstancePerLifetimeScope();


            builder.RegisterType<IntegrationEventLogService>()
                .WithParameter("dbConnection", new SqlConnection(_connectionStringEventLog))
                .As<IIntegrationEventLogService>()
                .InstancePerLifetimeScope();


          

            builder.RegisterType<ApiIntegrationEventSvc>()
                .As<IIntegrationEventSvc>()
                .InstancePerLifetimeScope();

            



            //Singleton
            builder.RegisterType<DomainEventManagerInMemory>()
                .As<IDomainEventManagerInMemory>()
                .SingleInstance();






            // Register the Command's Validators (Validators based on FluentValidation library)
            //builder
            //    .RegisterAssemblyTypes(typeof(Startup).GetTypeInfo().Assembly)
            //    .Where(t => t.IsClosedTypeOf(typeof(IValidator<>)))
            //    .AsImplementedInterfaces();



        }
    }
}
