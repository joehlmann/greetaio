﻿using Autofac;
using FluentAssertions;
using GreetaIO.API.Application.ApplicationServices.Interfaces;

using GreetaIO.TheDomain.Enums;
using GreetaIO.Utility.Helper;
using Xunit;

namespace GreetaIO.API.UnitTesting.ServicesApplication
{
    
    public class CustomerAccountServiceTests : BaseUnitTests
    {
        private ICustomerAccountValidService _accountService;

        public CustomerAccountServiceTests()
        {
            
        }


        
        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.Customer, true)]
        public  void Validates_CustomerAccount_Number(int id ,bool expectedResult)
        {
            //Arrange 

            _accountService = _containerScope.Resolve<ICustomerAccountValidService>();

            //Act

            var result =  _accountService.ValidAccountNumAsync(id.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        [Theory]
        [InlineData("1", false)]
        [InlineData("incorrect", false)]
        public void Validates_CustomerAccount_Number_Correct_Syntax(string id, bool expectedResult)
        {
            //Arrange 

            _accountService = _containerScope.Resolve<ICustomerAccountValidService>();

            //Act

            var result = _accountService.ValidAccountNumAsync(id.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        [Theory]
        [InlineData(1, 1, false)]
        [InlineData((int)EntityTypeRange.Customer, (int)EntityTypeRange.Location, true)]
        public void Validates_CustomerAccount_Number_for_Location(int accountNum, int locationId , bool expectedResult)
        {
            //Arrange 

            _accountService = _containerScope.Resolve<ICustomerAccountValidService>();

            //Act

            var result = _accountService.ValidLocationForAccountNumAsync(accountNum.ToString(),locationId.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("1x", "1x", false)]
        [InlineData("incorrect", "incorrect", false)]
        public void Validates_CustomerAccount_Number_for_Location_Correct_syntax(string accountNum, string locationId, bool expectedResult)
        {
            //Arrange 

            _accountService = _containerScope.Resolve<ICustomerAccountValidService>();

            //Act

            var result = _accountService.ValidLocationForAccountNumAsync(accountNum.ToString(), locationId.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }



    }
}
