﻿using Autofac;
using FluentAssertions;
using GreetaIO.API.Application.ApplicationServices.Interfaces;

using GreetaIO.TheDomain.Enums;
using GreetaIO.Utility.Helper;
using Xunit;

namespace GreetaIO.API.UnitTesting.ServicesApplication
{
    
    public class IdValidServiceTests : BaseUnitTests
    {
        private IEntityIdValidService _idValidService;

        public IdValidServiceTests()
        {
            
        }


        
        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.User, true)]
        public void Validates_userId_for_IdValidService(int id ,bool expectedResult)
        {
            //Arrange 

            _idValidService = _containerScope.Resolve<IEntityIdValidService>();

            //Act

            var result = _idValidService.ValidUserIdAsync(id.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }


        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.Location, true)]
        public void Validates_LocationId_for_IdValidService(int id, bool expectedResult)
        {
            //Arrange 

            _idValidService = _containerScope.Resolve<IEntityIdValidService>();

            //Act

            var result = _idValidService.ValidLocationIdAsync(id.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(1, false)]
        [InlineData((int)EntityTypeRange.Library, true)]
        public void Validates_LibraryId_for_IdValidService(int id, bool expectedResult)
        {
            //Arrange 

            _idValidService = _containerScope.Resolve<IEntityIdValidService>();

            //Act

            var result = _idValidService.ValidLibraryIdAsync(id.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(1,1 , false)]
        [InlineData((int)EntityTypeRange.Library,(int)EntityTypeRange.Location, true)]
        public void Validates_LibraryId_for_LocationId_IdValidService(int libId,int locId, bool expectedResult)
        {
            //Arrange 

            _idValidService = _containerScope.Resolve<IEntityIdValidService>();

            //Act

            var result = _idValidService.ValidLibraryIdForLocationIdAsync(libId.ToString(),locId.ToString()).Result;

            //Assert

            result.IsSuccess.Should().Be(expectedResult);
        }

    }
}
