﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using GreetaIO.API.Infrastructure.Modules;
using GreetaIO.API.IntegrationTests.ArrangeModules;
using GreetaIO.API.UnitTesting.ArrangeModules;
using GreetaIO.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GreetaIO.API.UnitTesting
{
    public class BaseUnitTests
    {
        protected IConfiguration _configuration;
        protected IServiceProvider _provider;
        protected ILifetimeScope _containerScope;
        protected IContainer _container;
        protected string _connectionString;

        public BaseUnitTests()
        {
            _configuration = new  ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            _connectionString = _configuration.GetConnectionString("ConnectionString");

            _provider = Configure();

            _containerScope = _provider.GetRequiredService<ILifetimeScope>(); 
        }

        public  IServiceProvider Configure()
        {
            
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new ApplicationModule(containerBuilder, _connectionString));
            containerBuilder.RegisterModule(new ServicesModuleUnitTests(_connectionString));

            containerBuilder.RegisterType<ApiContext>()
                .WithParameter("options", DbContextConfig.Configure(_connectionString))
                .InstancePerLifetimeScope();

          

            containerBuilder.AddAutoMapper(typeof(Startup).Assembly);
            containerBuilder.RegisterModule(new MediatorTestModule());
            _container = containerBuilder.Build();

            return new AutofacServiceProvider(_container);
        }

        
        public class DbContextConfig
        {
            public static DbContextOptions<ApiContext> Configure(string connectionString)
            {
                var builder = new DbContextOptionsBuilder<ApiContext>();
                builder.UseSqlServer(connectionString,
                    sqlServerOptionsAction: sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);

                    });
                builder.UseLazyLoadingProxies();
                return builder.Options;
            }
        }
    }
}
