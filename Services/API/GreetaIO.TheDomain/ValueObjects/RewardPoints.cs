﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;
using GreetaIO.TheDomain.Enums;

namespace GreetaIO.TheDomain.ValueObjects
{
   public class RewardPoints : ValueObject
    {

        public RewardsType RewardsType { get; private set; }


        public int PointsCost { get; private set; }
        public int PointsReward { get; private set; }


        public RewardPoints(RewardsType rewardsType, int pointsCost, int pointsReward)
        {
            RewardsType = rewardsType;
            PointsCost = pointsCost;
            PointsReward = pointsReward;
        }




        // Constructors & Factories

        public static RewardPoints Create(RewardsType rewardsType, int pointsCost, int pointsReward)
        {
            return new RewardPoints(rewardsType, pointsCost, pointsReward);
        }


        public static  RewardPoints Empty()
        {
            return new RewardPoints(default,default,default);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return RewardsType;
            yield return PointsReward;
            yield return PointsCost;
        }
    }
}
