﻿namespace GreetaIO.TheDomain.Enums
{
    public enum TransactionType
    {

        Unknown,
        Default,
        Known,
        Multiple,
        Fail,
        Empty

    }
}
