﻿namespace GreetaIO.TheDomain.Enums
{
    public enum EntityTypeRange
    {
        User =1000,
        Customer = 2000,
        Library = 3000,
        Item = 4000,
        Location = 5000,
        Transaction = 8000,
        UserImage = 9000
    }
}