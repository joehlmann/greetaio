﻿namespace GreetaIO.TheDomain.Enums
{
    public enum UserType
    {
        New,
        Default,
        PreviousVisitor,
        RegisteredVisitor,
        WasRegistered,
        Empty
    }
}
