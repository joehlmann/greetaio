﻿namespace GreetaIO.TheDomain.Enums
{
    public enum StatesEnum
    {
        NT=1000,
        NSW=2000,
        ACT=2900,
        VIC=3000,
        QLD=4000,
        SA = 5000,
        WA = 6000,
        TAS = 7000
    }
}