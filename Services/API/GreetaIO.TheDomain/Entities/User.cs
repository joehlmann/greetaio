﻿using System;
using System.Collections.Generic;
using System.Linq;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Events;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class User : Entity<int> , IAggregateRoot
    {
        
        //State

        public string AliasName { get; private set; }

        public virtual Contact Contact { get; private set; }

        public UserType UserType { get; private set; }

        public int CurrentRewards { get; private set; }

        public int UserImageCount => _userImages.Count;



        public string TargetId { get; private set; }


        private  List<UserImage> _userImages = new List<UserImage>();


        public virtual IReadOnlyList<UserImage> UserImages => _userImages.ToList();


        


        public bool IsEmpty { get; private set; }


        public override string ToString()
        {
            return $"UserId:{Id}" + ":" + AliasName + ":" + Contact.ToString() + ":" + Enum.GetName(typeof(UserType), UserType) +
                   ":CurrentRewards" + CurrentRewards + ":TargetId" +
                   TargetId + $":UserImageCount{_userImages.Count}";
        }


        public void AddUserImage(UserImage userImage)
        {
            AddDomainEvent(new UserImageAddedToUserDomEvent(this, userImage));

            _userImages.Add(userImage);
        }


        // Constructors & Static factories
        // HiLo 1000

        public User(int id,string aliasName, Contact contact, UserType userStatus, int currentRewards)
            :this(aliasName, contact, userStatus, currentRewards)
        {
            Id = id;

        }

        public User(string aliasName, Contact contact, UserType userStatus, int currentRewards)
        {
            
            AliasName = aliasName;
            Contact = contact;
            UserType = userStatus;
            CurrentRewards = currentRewards;


        }
        private User( IEnumerable<byte[]> images)
        {
            //Id = id;
            AliasName = "AliasName";
            Contact.Empty();
            UserType = UserType.New;
            _userImages.AddRange(images.Select(i=> UserImage.Create(i)));

        }

        protected User() { }

        private User(bool isEmpty)
        {
            Id = default;
            UserType = UserType.Empty;
            IsEmpty = true;
        }

        public static User Empty(bool isEmpty=true)
        {
            return new User(isEmpty);
            
        }

        public static User Seed(int id, string aliasName, Contact contact, UserType userStatus, int currentRewards)
        {
            return new User(id,  aliasName, contact, userStatus, currentRewards);
        }


        public static User Create(string aliasName, Contact contact, UserType userStatus, int currentRewards)
        {
            return new User(aliasName, contact, userStatus,currentRewards);
        }


        public static User Enroll(IEnumerable<byte[]> images)
        {
            return new User( images);
        }


    }
}
