﻿using System;
using GreetaIO.TheDomain.Enums;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public  class Transaction : Entity<int>
    {
        public virtual  Item Item { get; set; }

        public TransactionType TransactionType { get; set; }

        public virtual User User { get; set; }

        public virtual  Location Location { get; set; }

        public int PointsReward { get; set; }

        public override string ToString()
        {
            return $"TransactionId:{Id}" + ":" + Item.ToString() + ":" + Enum.GetName(typeof(TransactionType), TransactionType) +
                   User.ToString() + ":Location" + Location.LocationName + ":Points" + PointsReward;
                   
        }


        // Factories & Constructors
        public Transaction()
        {
            Id = default;
            Item = Item.Empty();
            TransactionType = TransactionType.Empty;
            User = User.Empty();
            Location = Location.Empty();
            PointsReward = default;

        }

        public static Transaction Empty()
        {
            return new Transaction();
        }

    }

}

