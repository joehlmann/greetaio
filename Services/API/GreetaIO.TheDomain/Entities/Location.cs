﻿using System.Collections.Generic;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class Location : Entity<int>
    {
        
        public int LocationNumber { get; private set; }
        public string LocationName { get; set; }

        public LocationType LocationType { get; set; }

        public virtual Address Address { get; set; }



        private readonly List<Contact> _contacts = new List<Contact>();
        public virtual IReadOnlyList<Contact> Contacts => _contacts.AsReadOnly();

        
        public virtual  Customer Customer { get; set; }


        private readonly List<Transaction> _transactions = new List<Transaction>();
        public virtual IReadOnlyList<Transaction> Transactions => _transactions.AsReadOnly();

        private readonly List<Item> _items= new List<Item>();
        public virtual  IReadOnlyList<Item> Items => _items.AsReadOnly();



        public void AddItem(Item item)
        {
            //AddDomainEvent(new ImageAddedToUserEvent(this, userImage));

            _items.Add(item);
        }

        // Factories & Constructors
        // HiLo 5000

        protected Location() { }

        protected Location(bool isEmpty)
        {
            Id = default;
            LocationNumber = default;
            LocationName = "";
            Address = Address.Empty();

            Customer = Customer.Empty();
            LocationType = LocationType.Empty;
        }

        private Location(  int locationNumber,string locationName, LocationType locationType, Customer customer,
            Address address,Contact contact)
        {
            LocationNumber = locationNumber;
            LocationName = locationName;
            Customer = customer;
            Address = address;
            _contacts.Add(contact);
            LocationType = locationType;

        }


        
        private Location(int locationId, 
            int locationNumber , string locationName,LocationType locationType,
            Customer customer,
            Address address,
            Contact contact)
            :this(locationNumber,locationName,locationType,customer,address,contact)
        {
            Id = locationId;
            
            
        }



        private Location(int num)
        {
            Id = num + (int)EntityTypeRange.Location;
            LocationNumber = num;
            Address = Address.Empty();

            _contacts.Add(Contact.Empty());
            
            LocationName = "LocationName";
            

        }

        //public static Location Create( Customer customer, string locationName,
        //    string state, string suburb, string address1, string address2, string postCode,
        //    string contactName, string phone, string email)
        //{
        //    return new Location(customer,   locationName,
        //        state, suburb, address1, address2, postCode,
        //        contactName, phone, email);

        //}

        public static Location Create(int locationNumber,
            string locationName,
            LocationType locationType,
            Customer customer,
            Address address,
            Contact contact
            )
        {
            return new Location( locationNumber, locationName,locationType,
                customer,address,contact);

        }

        public static Location Default(int num)
        {
            return new Location(num);
        }
        public static Location Empty(bool isEmpty=true)
        {
            return new Location(isEmpty);
        }

    }
}
