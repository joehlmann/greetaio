﻿namespace GreetaIO.TheDomain.Entities
{
    
    public class LibraryUser 
    {
        public int UserId { get; set; }
        public virtual  User User { get; set; }

        public int LibraryId { get; set; }
        public virtual Library Library { get; set; }

        // Factories & Constructors

        

        public LibraryUser()
        {
            UserId = default;
            User = User.Empty();
            LibraryId = default;
            Library = Library.Empty();
        }

        public static LibraryUser Empty()
        {
            return new LibraryUser();
        }
    }
}
