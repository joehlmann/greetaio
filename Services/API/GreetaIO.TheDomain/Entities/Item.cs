﻿using System;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class Item : Entity<int>
    {

        public int ItemNumber { get; set; }

        public string ItemName { get; set; }

        public string ItemDescription { get; set; }

        public ItemType ItemType { get; set; }

        public virtual RewardPoints RewardPoints { get; set; }

        public bool IsProduct { get; set; }
        public int GetPoints() => (int) RewardPoints.RewardsType * RewardPoints.PointsReward;


        public override string ToString()
        {
            return $"ItemId:{Id}" + ":" +ItemName + ":" + ItemDescription + ":" + Enum.GetName(typeof(ItemType), ItemType) +
                   Enum.GetName(typeof(RewardsType), RewardPoints.RewardsType) + ":PointsCost" + RewardPoints.PointsCost + ":PointsReward" +
                   RewardPoints.PointsReward;
        }


        // Constructors  & factories
        // HiLo 4000

        protected Item() { }

        protected Item(bool isEmpty)
        {
            Id = default;
            ItemNumber = default;
            ItemName = default;
            ItemDescription = default;
            RewardPoints = RewardPoints.Empty();
            IsProduct = default;
            ItemType = ItemType.Empty;
        }

        public Item(int itemNumber, string itemName, string itemDescription, ItemType itemType,
            RewardPoints rewardPoints,
            bool isProduct)
        {
            ItemNumber = itemNumber;
            ItemName = itemName;
            ItemDescription = itemDescription;
            ItemType = itemType;
            RewardPoints = rewardPoints;
            IsProduct = isProduct;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemNumber"></param>
        /// <param name="itemName"></param>
        /// <param name="itemDescription"></param>
        /// <param name="itemType"></param>
        /// <param name="rewardsType"></param>
        /// <param name="pointsCost"></param>
        /// <param name="pointsReward"></param>
        /// <param name="isProduct"></param>
        public Item(int id, int itemNumber, string itemName, string itemDescription, ItemType itemType,
            RewardPoints rewardPoints,
            bool isProduct)
            : this(itemNumber, itemName, itemDescription, itemType, rewardPoints, isProduct)
        {
            Id = id;
        }

        public static Item Create(int itemNumber, string itemName, string itemDescription, ItemType itemType,
            RewardPoints rewardPoints,
            bool isProduct)
        {
            return new Item(itemNumber,itemName,itemDescription,itemType,rewardPoints,isProduct);
        }

        public static Item Empty(bool isEmpty = true)
        {
            return new Item(isEmpty);
        }
    }

}

