﻿using System;
using GreetaIO.TheDomain.Enums;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class UserImage : Entity<int>
    {
        public Guid RequestId { get; private set; }

        public byte[] ImageData { get; private set; }

        public string TargetId { get; private set; }

        public string TransactionId { get; private set; }

        public UserImageType UserImageType { get; private set; }


        //public int UserId { get; private set; }


        public string Url { get; private set; }

        public long SessionId { get; private set; }

        #region constructors
        private UserImage()
        {
            RequestId = Guid.Empty;
            Id = default;
            ImageData = new byte[0];
            TargetId = "";
            TransactionId = "";
            UserImageType = UserImageType.Empty;
            //UserId = default;
            Url = "";
            SessionId = default;

        }
        private UserImage(byte[] imageData, long timeId)
        {
            ImageData = imageData;
            TargetId = "";
            TransactionId = "";
            UserImageType = UserImageType.New;
            CreateDate = DateTime.Now;
            SessionId = timeId;
        }

        private UserImage(byte[] imageData, long timeId, string url,Guid requestId)
            : this(imageData, timeId)
        {
            RequestId = requestId;
            Url = url;
        }
        #endregion

        #region diconnected-constructors
        private UserImage(UserImageType userImageType, string transactionId, string targetId, byte[] imageData)
        {
            UserImageType = userImageType;
            TransactionId = transactionId;
            TargetId = targetId;
            ImageData = imageData;
            UserImageType = UserImageType.Library;
        }

        private UserImage(int id,UserImageType userImageType, string transactionId, string targetId, byte[] imageData, string url)
            : this(userImageType, transactionId, targetId, imageData)
        {
            Id = id ;
            Url = url;
        }
        #endregion

        #region Factories     
        public static UserImage Create(byte[] imageData,long timeId=default, string url="")
        {
            return new UserImage(imageData, timeId, url,Guid.Empty);
        }
        
        
        public static UserImage Create(byte[] imageData, string url ,  long timeId,Guid requestId)
        {
            return new UserImage(imageData,  timeId, url,requestId);
        }
        public static UserImage Empty()
        {
            return new UserImage();
        }

        #endregion
    }
}
