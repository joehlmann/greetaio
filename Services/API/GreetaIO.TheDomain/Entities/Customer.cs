﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.ValueObjects;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class Customer : Entity<int>
    {
        
        public string CompanyName { get; set; }

        public int CustomerNumber { get; private set; }


        public virtual  Contact Contact { get; set; }


        private readonly List<Location> _locations = new List<Location>();
        public virtual List<Location> Locations => _locations;


        public CustomerType CustomerType { get; set; }

        // Methods


        public void Addlocation(Location location)
        {
            _locations.Add(location);
        }



        // Constructors & Factories

        protected Customer()
        {
            
        }

        protected Customer(bool isEmpty)
        {
            Id = default;
            CompanyName = "";
            CustomerNumber = default;
            Contact = Contact.Empty();
            CustomerType = CustomerType.Empty;
        }

        // HiLo 2000
        public Customer(int customerNumber,
            string companyName,
            string state, string suburb, string address1, string address2, string postCode,
            string contactName, string phone, string email)
        {
            CustomerNumber = customerNumber;
            CompanyName = companyName;
                Locations.Add(Location.Default(0));
                Contact = Contact.Create(contactName, phone, email);
                
                CustomerType = CustomerType.Default;
        }


        /// <summary>
        /// Detached Entity Creation
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="customerNumber"></param>
        /// <param name="companyName"></param>
        /// <param name="state"></param>
        /// <param name="suburb"></param>
        /// <param name="address1"></param>
        /// <param name="address2"></param>
        /// <param name="postCode"></param>
        /// <param name="contactName"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        public Customer(int customerId, int customerNumber,  string companyName,
            Address address,Contact contact
            )
        {
            Id = customerId;
            CustomerNumber = customerNumber;
            CompanyName = companyName;
            
            Contact = contact;
            
        }


        
        private Customer(int num)
        {
            Id = num + (int) EntityTypeRange.Customer;
            CustomerNumber = num;
            
            Contact = Contact.Empty();
            CompanyName = "CompanyName";
            
            CustomerType = CustomerType.Default;

        }

        public static Customer Create(int customerNumber,
            string companyName,
            string state, string suburb, string address1, string address2, string postCode,
            string contactName, string phone, string email)
        {
            return new Customer(customerNumber,
                companyName,
                state, suburb, address1, address2, postCode,
                contactName, phone, email);
            
        }

        public static Customer Create(int customerNumber,string companyName,
            Address address,
            Contact contact,
            int customerId=0)
        {
            return new Customer(customerId,customerNumber,companyName
                ,address
                ,contact);

        }

        public static Customer Seed(int num)
        {
            return new Customer(num);
        }

        public static Customer Empty(bool isEmpty=true)
        {
            return new Customer(isEmpty);
        }

    }
}
