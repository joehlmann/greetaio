﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using GreetaIO.TheDomain.Enums;
using GreetaIO.TheDomain.Events;
using GreetaIO.Utility;

namespace GreetaIO.TheDomain.Entities
{
    public class Library : Entity<int>
    {
        

        

        public int LibraryNumber { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }


        public LibraryType LibraryType { get; private set; }

        public virtual  Location Location { get; private set; }



        private readonly List<User> _libraryUsers = new List<User>();
        public virtual IReadOnlyList<User> LibraryUsers => _libraryUsers.AsReadOnly();


        private readonly List<UserImage> _userImages = new List<UserImage>();
        public virtual IReadOnlyList<UserImage> UserImages => _userImages.AsReadOnly();


        // Methods

        public void AddLibUser(User libraryUser)
        {
            _libraryUsers.Add(libraryUser);
        }


        public void FindUnKnownUser(UserImage userImage)
        {
            _userImages.Add(userImage);
            AddDomainEvent(new UserImageAtLocationDomEvent(userImage,Location,this) );
        }


        // HiLo 3000


        //[NotMapped]
        //public virtual List<User> Users => _libraryUsers.Select(e => e.User).ToList();

        [NotMapped]
        public string GalleryId => Id.ToString();



        // Factories & Constructors

        private Library(int libraryNumber, string name, string description, LibraryType libraryType, Location location)
        {
            LibraryNumber = libraryNumber;
            Name = name;
            Description = description;
            LibraryType = libraryType;
            Location = location;
        }



        public static Library Create(int libraryNumber,
        string name, string description, LibraryType libraryType,
         Location location)
        {
            return new Library(libraryNumber, name, description, libraryType, location);

        }

        protected Library()
        {
            Id = default;
            LibraryNumber = default;
            Name = default;
            Description = default;
            LibraryType = LibraryType.Empty;
            Location = Location.Empty();
        }

        public static Library Empty()
        {
            return new Library();
        }

    }
}
