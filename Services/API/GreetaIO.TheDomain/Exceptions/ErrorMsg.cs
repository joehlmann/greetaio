﻿namespace GreetaIO.TheDomain.Exceptions
{
    public static class ErrorMsg
    {

        public static string NoErrors = "NoErrors";


        //item 
        public static string ItemMustHaveNumber = "ItemMustHaveNumber";
        public static string ItemMustNotBeEmpty = "'Item Number' must not be empty.";
        public static string ItemMustHaveName = "ItemMustHaveName";
        public static string ItemMustHaveDescription = "ItemMustHaveDescription";
        public static string ItemTypeInvalid = "ItemTypeInvalid";
        public static string RewardsTypeInvalid = "RewardsTypeInvalid";
        public static string InvalidLocation = "Invalid Location";



        //item 
        public static string LibMustHaveNumber = "LibMustHaveNumber";
        public static string LibMustHaveName = "LibMustHaveName";
        public static string libMustHaveDescription = "libMustHaveDescription";
        public static string LibTypeInvalid = "LibTypeInvalid";



        // Value Objects
        public static string AddressLine1Invalid = "Address:Invalid Address Line1";
        public static string AddressStateInvalid = "Address:Invalid State";
        public static string AddressStateInvalidNull = "Address:Invalid State cant be null";
        public static string AddressPostCode = "Address:Invalid PostCode";
        public static string AddressPostCodeNull = "Address:PostCode cant be null";
        public static string AddressSub = "Address:Invalid Suburb Name Length";
        public static string AddressSubNull = "Address:Invalid Suburb Name Null";
        public static string ContactInvalid = "Contact:Invalid Contact";


        //UserImage-User
        public static string NotValidImage = "Not a valid Image";
        public static string MustSupplyUserId = "Must Supply UserId";
        public static string MustSupplyImageData = "Must Supply ImageData";
        public static string UserDoesNotExist = "User doesn't Exist";
        public static string UserImageDoesNotExist = "UserImage doesn't Exist";


        //User
        
        public static string MustSupplyAlias = "Must Supply AliasName";
        
        public static string LocationForUserDoesNotExist = "Location for User doesn't Exist";
        


        //Customer
        public static string CustomerCreateFailed = "Create Customer Failed";
        public static string CustomerMustSupplyId = "Must Supply Id";
        public static string CustomerDoesNotExist = "Customer doesn't Exist";
        public static string CustomerCompanyNameNotLongEnough = "CompanyName Not Long Enough";
        public static string CustomerInvalidNumberFormat = "Invalid Number Format";
        public static string CustomerNumberInvalid = "Customer Number Invalid";



        //Library
        public static string LibraryNotValid = "Not a valid Image";
        public static string LibraryMustSupplyId = "Must Supply Id";
        
        public static string LibraryDoesNotExist = "Library doesn't Exist";
        

        //Location
        public static string LocationNull = "Location null";
        public static string LocationNameNotValid = "Loc Name must be more than 2 chars";
        public static string LocationMustSupplyId = "Must Supply UserId";
        
        public static string LocationDoesNotExist = "Location doesn't Exist";


        public static string ContactNameEmpty = "Contact: Name Cant Be Empty";
        public static string ContactNameToLong = "Contact: Name To Long";
        public static string ContactPhoneEmpty = "Contact: Phone Empty";
        public static string ContactPhoneInvalidLength = "Contact: Phone Invalid length";
        public static string ContactEmailEmpty { get; } = "Contact:E-mail can't be empty";
        public static string ContactEmailToLong { get; } = "Contact:E-mail is too long";

       
    }
}
