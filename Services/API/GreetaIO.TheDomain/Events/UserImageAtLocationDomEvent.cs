﻿using GreetaIO.TheDomain.Entities;
using MediatR;

namespace GreetaIO.TheDomain.Events
{
    public class UserImageAtLocationDomEvent : DomainEvent, INotification
    {

        public Location Location { get; private set; }
        public UserImage UserImage { get; private set; }

        public Library Library { get; private set; }


        public  UserImageAtLocationDomEvent(UserImage unknownUser, Location location,Library library)
        {
            UserImage = unknownUser;
            Location = location;
            Library = library;
        }
    }
}
