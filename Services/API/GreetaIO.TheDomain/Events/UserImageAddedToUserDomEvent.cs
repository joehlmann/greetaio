﻿using GreetaIO.TheDomain.Entities;
using MediatR;

namespace GreetaIO.TheDomain.Events
{
    public class UserImageAddedToUserDomEvent  : DomainEvent, INotification
    {
        public User User { get; private set; }
        public UserImage Image { get; private set; }

        public int LibraryId { get; private set; }

        public UserImageAddedToUserDomEvent(User user, UserImage image,int libraryId=0)
        {
            User = user;
            Image = image;
            LibraryId = libraryId;
        }
    }
}
