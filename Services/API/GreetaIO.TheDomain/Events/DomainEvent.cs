﻿using System;

namespace GreetaIO.TheDomain.Events
{
    public abstract class DomainEvent
    {

        public Guid DomainEventId { get; private set; }

        public Guid CorrelationId { get; private set; }

        protected DomainEvent()
        {
            DomainEventId = Guid.NewGuid();
        }

    }
}
