﻿namespace GreetaIO.TheDomain.RepoInterfaces
{
    public interface IRepository
    {

        IUnitOfWork UnitOfWork { get; }
    }
}
