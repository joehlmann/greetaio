﻿using System.Threading.Tasks;

namespace GreetaIO.TheDomain.RepoInterfaces
{
    public interface ICrudRepository<T,TId> 
        where T : class
        //where TId : struct
    {
        IUnitOfWork UnitOfWork { get; }
        T Add(T entity);

        T Update( T entity);

        Task<T> GetAsync(TId id);

    }
}